<?php

class Lf_Utility_Family_Tree
{
    public static function get_instance()
    {
        return new self();
    }

    public static function init()
    {
        return self::get_instance();
    }

    /**
     * Returns the array of children posts
     *
     * @param int $post_id the parent post's id
     * @param string $post_type post type to find
     *
     * @return array children posts
     */
    static private function get_childs(int $post_id = 0, string $post_type = 'cpt-family')
    {
        if (!$post_id) {
            $post_status        = 'publish';
            $num_of_posts        = -1;

            $args = array(
                'post_parent'    => $post_id,
                'post_type'        => $post_type,
                'numberposts'    => $num_of_posts,
                'post_status'    => $post_status,
                'orderby'        => 'post_title',
                'order'            => 'ASC'
            );
            $results = get_children($args);
        } else {
            global $wpdb;

            $query = 'SELECT p.ID, post_title FROM ' . $wpdb->posts . ' p LEFT JOIN '
                . DB_POSTS_FAMILIES . ' ON p.ID = post_id WHERE family_id = ' . $post_id
                . ' AND post_status = "publish" AND post_type = "' . $post_type . '"';

            $results = $wpdb->get_results($query);
        }

        return $results;
    }

    /**
     * Generate Tree of Posts Recursively
     *
     * @param array &$tree reference to the tree
     */
    static private function get_tree(array &$tree)
    {
        foreach ($tree as $node) {
            $node->childs = self::get_childs($node->ID, 'cpt-sub-family');
            self::get_tree($node->childs);
        }
    }

    /**
     * @param int $family ID of the Family
     */
    static function create_tree(bool $complete_tree = true, int $family = null)
    {
        // Get Families or Family
        if (!$complete_tree)
            $tree[] = get_post($family);
        else
            $tree = self::get_childs();

        self::get_tree($tree);

        return $tree;
    }

    static function order_group_families_tree(array &$group_families)
    {
        $ordered_families = [];

        foreach ($group_families as $key => $value) {
            $family_group_name = get_the_title($key);

            usort($value, function ($a, $b) {
                $a = $a->post_title ?? get_the_title($a);
                $b = $b->post_title ?? get_the_title($b);

                return $a > $b;
            });

            $ordered_families[$family_group_name] = $value;
        }
        ksort($ordered_families);

        $group_families = $ordered_families;
    }

	static function order_tree_subfamilies(array &$tree)
	{
		usort($tree, function ($a, $b) {
			$a = $a->post_title ?? get_the_title($a);
			$b = $b->post_title ?? get_the_title($b);

			return $a > $b;
		});
		ksort($tree);
	}
}
