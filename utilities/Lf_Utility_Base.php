<?php

class Lf_Utility_Base {

	public static function get_instance() {
		return new self();
	}

	public static function init() {
		return self::get_instance();
	}

	static function get_total($args = []) {
		$args['total'] = true;
		return self::get_list($args);
	}

	static function get_list($args = []) {
		$debug = $args['debug'] ?? null;
		$post_type = $args['post_type'] ?? null;
		$post_not_in = $args['post__not_in'] ?? null;
		$user_families = $family_id = $instance = null;
		$all_types = [LF_CPT_QUESTION, LF_CPT_VIDEO, LF_CPT_MANUAL];
		$is_feed = $args['is_feed'] ?? false;

		if ((!$post_type || self::get_filtered_type_content($post_type)) && !$is_feed) {
			$user_families = self::get_families_id_by_user();
			if (!$user_families && !current_user_can('edit_posts'))
				return [];
			$family_id = $args['family_id'] ?? null;
			$family_id = $family_id ? self::check_valid_family_to_search($family_id, $user_families) : null;
		} else if ($is_feed)
			$family_id = $args['family_id'] ?? null;

		$metas = $args['metas'] ?? [];
		$total = $args['total'] ?? false;
		$q = $args['q'] ?? null;
		$title = $args['title'] ?? null;
		$orderby = $args['orderby'] ?? 'post_date';
		$dates = $args['dates'] ?? [];

		if ($q)
			$title = $q;

		$page = $args['page'] ?? 0;
		$limit = $args['limit'] ?? -1;

		$params = [
			'post_status' => 'publish',
			'orderby' => $orderby,
			'order' => $post_type == LF_CPT_MANUAL ? 'ASC' : 'DESC'
		];

		if ($dates)
			$params['date_query'] = $dates;

		if ($title)
			$params['s'] = $title;

		if ($post_type)
			$params['post_type'] = $post_type;

		if ($post_not_in)
			$params['post__not_in'] = $post_not_in;

		if ($metas) {
			foreach ($metas as $meta_key => $meta_value)
				$params['meta_query'][$meta_key . '_clause'] = [
					'key' => $meta_key,
					'value' => $meta_value
				];
		}

		if (self::get_filtered_type_content($post_type)) {
			if ($family_id || !$is_feed) {
				if (in_array($post_type, $all_types)) {
					$questions = self::get_filtered_posts_id_by_post_type($family_id);
					if (!$questions && (!current_user_can('edit_posts') || $is_feed))
						return [];
					$params['post__in'] = $post_not_in ? array_diff($questions, $post_not_in) : $questions;
				} else {
					$key = 'familia';
					$meta_query = [
						'key' => $key,
						'value' => $family_id ? $family_id : $user_families,
						'compare' => 'IN'
					];

					$params['meta_query'][$key . '_clause'] = $meta_query;
				}
			}
		}		

		if ($total) {
			$query = new WP_Query($params);
			return $query->found_posts;
		}

		if ($limit) {
			$params['posts_per_page'] = $limit;
			$params['offset'] = $page * $limit + ($post_type == LF_CPT_VIDEO && $page ? 1 : 0);
		}

		$query = new WP_Query($params);

		$results = $query->get_posts();

		$items = [];
		foreach ($results as $result) {
			$id = $result->ID;

			$item = wp_cache_get("$result->post_type\_$id");
			if ($item === false) {
				$instance = self::get_instance_by_post_type($result->post_type);

				$item = new $instance;
				$item->get($id);
				wp_cache_set("$result->post_type\_$id", $item);
			}
			$items[] = $item;
		}

		if ($limit === 1)
			return reset($items);

		return $items;
	}

	static function has_filter($args) {

		$keys_filter = [];

		foreach ($args as $key => $value) {
			if (in_array($key, $keys_filter) && $value) {
				return true;
			}
		}
		return false;
	}

	static function get_instance_by_post_type($post_type) {
		$instance = null;

		switch ($post_type) {
			case LF_CPT_COURSE:
				$instance = Lf_Model_Course::class;
				break;
			case LF_CPT_FAMILY:
				$instance = Lf_Model_Family::class;
				break;
			case LF_CPT_VIDEO:
				$instance = Lf_Model_Video_Clip::class;
				break;
			case LF_CPT_QUESTION:
				$instance = Lf_Model_Question::class;
				break;
			case LF_CPT_MANUAL:
				$instance = Lf_Model_Manual::class;
				break;
		}

		if (!$instance)
			die('Hay que definir modelo de:' . $post_type);

		return $instance;
	}

	static function update_post_metas($post_id, $metas) {
		foreach ($metas as $meta_key => $meta_value) {
			if ($meta_value) {
				update_post_meta($post_id, $meta_key, $meta_value);
			}
		}
	}

	static function get_families_by_user() {
		$args = [
			'post_type' => LF_CPT_FAMILY,
		];
		$families = self::get_list($args);
		$user = Lf_Login::get_lf_user();

		if (!$user)
			return $families;

		$user_ei = $user->get_entry_id();


		$user_families = wp_cache_get('user_families', $user_ei);
		if ($user_families === false) {
			$user_families = $user->get_families_ids();
			wp_cache_add('user_families', $user_families, $user_ei);
		}

		foreach ($families as $key => $family) {
			$code = $family->code;
			if (!in_array($code, $user_families))
				unset($families[$key]);
		}

		return $families;
	}

	static function get_wp_families_ids($families) {
		$args = [
			'post_type' => LF_CPT_FAMILY,
		];
		$wp_families = self::get_list($args);

		foreach ($wp_families as $key => $family) {
			$code = $family->code;
			if (!in_array($code, $families))
				unset($wp_families[$key]);
		}

		return array_column($wp_families, 'ID');
	}

	static function get_families_id_by_user($return_field = 'ID') {
		$families = self::get_families_by_user();

		return array_column($families, $return_field);
	}

	static function check_valid_family_to_search($family_id, $user_families) {
		if ($user_families) {
			if (!is_array($family_id)) {
				foreach ($user_families as $user_family) {
					if ($user_family == $family_id) {
						return $family_id;
					}
				}
			} else {
				foreach ($family_id as $key => $family) {
					if (!$family)
						continue;
					if ($family instanceof WP_Post)
						$family = $family->ID;
					if (!in_array($family, $user_families)) {
						unset($family_id[$key]);
					}
				}
				return $family_id;
			}
		}

		return null;
	}

	static function get_filtered_type_content($post_type = null) {
		$valid_post_types = [LF_CPT_FAMILY];

		if (!$post_type || !in_array($post_type, $valid_post_types))
			return true;

		return false;
	}

	static function get_filtered_posts_id_by_post_type($family_id = null) {
		global $wpdb;
		$user_families = self::get_families_id_by_user();
		if ($family_id && !is_array($family_id))
			$family_id = [$family_id];
		else if (!$family_id)
			$family_id = $user_families;
		else {
			foreach ($family_id as $key => $family) {
				if (!in_array($family, $user_families))
					unset($family_id[$key]);
			}
		}

		if (!sizeof($family_id))
			return [];

		$query = 'SELECT DISTINCT post_id FROM ' . DB_POSTS_FAMILIES . ' WHERE family_id IN (' . implode(',', $family_id) . ')';
		$result = $wpdb->get_col($query);

		return $result;
	}

	static function get_featured_post($family = null, $post_type = 'video') {
		global $wpdb;
		if (!$family)
			$family = self::get_families_id_by_user();
		else if (!is_array($family))
			$family = [$family];

		$query = 'SELECT post_id AS family_id, meta_value AS featured_id FROM ';
		if ($post_type != 'curso')
			$query .= $wpdb->posts . ' p LEFT JOIN ' . $wpdb->postmeta . ' ON p.ID = meta_value';
		else
			$query .= $wpdb->postmeta;
		$query .= ' WHERE 1';
		if ($family)
			$query .= ' AND post_id IN (' . implode(',', $family) . ')';
		$query .= ' AND meta_key = "' . $post_type . '_destacado" AND meta_value NOT LIKE ""';
		if ($post_type != 'curso')
			$query .= ' AND post_status = "publish" ORDER BY post_date DESC';

		$result = $wpdb->get_results($query);

		return $result;
	}

	/**
	 * Obtenemos todos los campos de los grupos de campos
	 * de ACF a través del ID del post
	 *
	 * @param int $post_id
	 * @return array
	 */
	static function get_acf_fields_by_post_id($post_id) {
		$acf_fields = [];
		$groups = acf_get_field_groups(array('post_id' => $post_id));
		foreach ($groups as $group) {
			$acf_fields = array_merge($acf_fields, acf_get_fields($group['key']));
		}
		return $acf_fields;
	}

	/**
	 * Obtenemos la key del campo a través
	 * del nombre de éste
	 *
	 * @param string $name
	 * @return string
	 */
	static function get_acf_field_key_by_name($acf_fields, $name) {
		foreach ($acf_fields as $acf_field) {
			if ($acf_field['name'] == $name) {
				return $acf_field['key'];
			}
		}
		die('ACF key no encontrada ' . $name);
		//return null;
	}

	/**
	 * Obtenemos la key del campo a través
	 * del nombre de éste
	 *
	 * @param string $name
	 * @return string
	 */
	static function get_acf_subfield_key_by_name($acf_fields, $name) {
		foreach ($acf_fields as $acf_field) {
			if (isset($acf_field['sub_fields'])) {
				foreach ($acf_field['sub_fields'] as $sub_field)
					if ($sub_field['name'] == $name) {
						return $sub_field['key'];
					}
			}
		}
		die('ACF key no encontrada ' . $name);
		//return null;
	}

	static function get_post_type_url($post_type) {
		$url = '';

		switch ($post_type) {
			case LF_CPT_VIDEO:
				$url = LF_CPT_VIDEO_URL;
				break;
			case LF_CPT_MANUAL:
				$url = LF_CPT_MANUAL_URL;
				break;
			case LF_CPT_COURSE:
				$url = LF_CPT_COURSE_URL;
				break;
			case LF_CPT_QUESTION:
				$url = LF_CPT_QUESTION_URL;
				break;
		}

		return get_home_url(null, $url);
	}

	/**
	 * prepare_dependant_posts
	 *
	 * @param  mixed $removed_families
	 * @return array $elements
	 */
	static function prepare_dependant_posts(array $removed_families) {
		$post_types = [LF_CPT_COURSE, LF_CPT_VIDEO, LF_CPT_MANUAL, LF_CPT_QUESTION];
		$elements = [];

		foreach ($removed_families as $family) {
			$family_title = get_the_title($family);
			$args['family_id'] = $family;
			$elements[$family_title] = [];
			foreach ($post_types as $post_type) {
				if ($post_type == LF_CPT_QUESTION)
					$posts = Lf_Model_Question::get_exclusive_family_questions($family);
				else {
					$args['post_type'] = $post_type;
					$posts = self::get_list($args);
				}
				if (sizeof($posts))
					$elements[$family_title][$post_type] = [];
				foreach ($posts as $item) {
					$elements[$family_title][$post_type][] = [
						'title' => $item->title,
						'url' => get_edit_post_link($item->ID)
					];
				}
				foreach ($posts as $post)
					wp_update_post(['ID' => $post->ID, 'post_status' => 'draft']);
			}
			wp_update_post(['ID' => $family, 'post_status' => 'draft']);
		}
		return $elements;
	}

	/**
	 * send_removed_family_mail
	 *
	 * @param  mixed $elements
	 * @return void
	 */
	static function send_removed_family_mail(array $elements) {
		$email = MAIL_TO;
		$from = MAIL_FROM;
		$subject = 'Centro de Ayuda | Existen Familias eliminadas';
		$body = 'Prueba';
		$route = LF_PLUGIN_DIR . '/templates/email.html.php';
		ob_start();
		require $route;
		$body = Html::minify(ob_get_clean());
		$id = Lf_WS_Automatic_Mail::send_automatic_mail($email, $from, $subject, $body, ID_MAIL_GROUP);
	}

	/**
	 * send_bad_rating_mail
	 *
	 * @param  mixed $elements
	 * @return void
	 */
	static function send_bad_rating_mail(int $element) {
		$email = RATING_MAIL_TO;
		$from = MAIL_FROM;
		$subject = 'Centro de Ayuda | Se ha registrado una valoración baja';
		$body = 'Prueba';
		$route = LF_PLUGIN_DIR . '/templates/bad_rating_email.html.php';
		ob_start();
		require $route;
		$body = Html::minify(ob_get_clean());
		$id = Lf_WS_Automatic_Mail::send_automatic_mail($email, $from, $subject, $body, ID_MAIL_GROUP);
	}

	static function get_post_params() {
		$term = $_POST['term'] ?? '';
		$family = $_POST['family'] ?? '';

		return [
			'search' => $term,
			'family' => $family
		];
	}

	static function has_access(array $families_ids) {
		$user_families = self::get_families_id_by_user('code');
		foreach ($families_ids as $family_id) {
			if (in_array($family_id, $user_families)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * https://jira.lefebvre.es/browse/WEB-44966
	 * @return boolean
	 */
	public static function limited_user_access() {
		$is_limited_user = false;
		$lf_user = Lf_Login::get_lf_user() ?? null;
		$wp_user = wp_get_current_user() ?? null;

		if (!$lf_user) {
			$is_limited_user = true;
		}

		if ($wp_user) {
			$valid_roles = ['administrator', 'editor'];
			$roles = $wp_user->roles ?? [];
			foreach ($roles as $rol) {
				if (in_array($rol, $valid_roles)) {
					$is_limited_user = true;
					break;
				}
			}
		}

		if ($lf_user) {
			$limited_users_entry = ['E1653014'];
			$entry = $lf_user->get_entry_id() ?? null;

			if (in_array($entry, $limited_users_entry)) {
				$is_limited_user = true;
			}
		}

		return $is_limited_user;
	}

}
