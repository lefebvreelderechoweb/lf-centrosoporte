<?php

class Lf_Utility_Course
{
	public static function get_courses_for_user($args = []): array
	{
		$lf_user = Lf_Login::get_lf_user();
		if ($lf_user)
			$args['id_user_pro'] = $lf_user->get_entry_id();

		if (!isset($args['families_ids']) || (isset($args['families_ids']) && !$args['families_ids'])) {
			$args['families_ids'] = implode(',', Lf_Utility_Base::get_families_id_by_user('code'));
		}	
		
		if(Lf_Utility_Base::limited_user_access()){
			return [];
		}

		$courses = Lf_Api_Rest_Formation_Product::get_courses_for_user($args);
		
		$lf_courses = [];
		foreach ($courses->result ?? [] as $course) {
			$lf_courses[] = new Lf_Model_Course($course);
		}

		return $lf_courses;
	}

	public static function get_all_courses($args = []): array
	{
		$courses = Lf_Api_Rest_Formation_Product::get_all_courses($args);

		$lf_courses = [];
		foreach ($courses->result ?? [] as $course) {
			$lf_courses[] = new Lf_Model_Course($course);
		}

		return $lf_courses;
	}

	private static function _get_courses_families(array $courses): array
	{
		$courses_families = array_column($courses, 'families');
		$families = [];

		foreach ($courses_families as $courses_family) {
			foreach ($courses_family as $course_family) {
				if ($course_family)
					$families[$course_family->idFamilia] = $course_family->descFamilia ?? '';
			}
		}
		return $families;
	}

	public static function get_courses_group_by_families(array $courses = []): array
	{
		$courses = $courses ?: self::get_all_courses();

		$families = self::_get_courses_families($courses);

		$group_courses = [];
		$current_post = get_post();
		$family_code = 0;
		if ($current_post && $current_post->post_type === LF_CPT_FAMILY)
			$family_code = get_field('codigo', $current_post->ID);

		foreach ($families as $family_id => $value) {
			foreach ($courses as $key => $course) {
				foreach ($course->families as $family) {
					if ($family->descFamilia === $value && ($family_id == $family_code || $family_code == 0)) {
						$group_courses[$value][] = $course;
						unset($courses[$key]);
					}
				}
			}
		}

		return $group_courses;
	}

	public static function get_courses_for_user_group_by_families(): array
	{
		$courses = self::get_courses_for_user();
		return self::get_courses_group_by_families($courses);
	}

	public static function get_course_by_id($id): ?Lf_Model_Course
	{
		$courses = self::get_courses_for_user();
		foreach ($courses as $course) {
			if ($course->ID == $id)
				return $course;
		}
		return null;
	}

	public static function get_courses_by_family($family_ids): array
	{
		if (sizeof($family_ids) == 1)
			return self::get_courses_for_user([
				'id_family' => $family_ids[0]
			]);

		$courses = self::get_all_courses();

		if (sizeof($family_ids) == 0)
			return $courses;

		$courses = array_filter($courses, function ($course) use ($family_ids) {
			foreach ($course->families as $family) {
				if (in_array($family->idFamilia, $family_ids))
					return true;
			}
			return false;
		});

		return $courses;
	}

	public static function get_related_elements($all = false): array
	{
		global $wpdb;

		$query			= 'SELECT * FROM ' . DB_COURSES_RELATIONS;
		if (!$all) {
			$url		= $_SERVER['REQUEST_URI'] ?? '';
			$url		= esc_url($url);
			$url		= explode('/', $url);
			$course_id	= (int) $url[count($url) - 1] ?? 0;

			$query		.= ' WHERE post_id = ' . $course_id;
		}

		$related		= $wpdb->get_results($query);

		return $related;
	}
}
