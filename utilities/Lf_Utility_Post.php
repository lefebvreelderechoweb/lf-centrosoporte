<?php
class Lf_Utility_Post extends Lf_Utility_Base
{
    private static function _save_post($post_id, $col_name = 'family_id')
    {
        global $wpdb;
        $table = DB_POSTS_FAMILIES;
        $acf_fields = Lf_Utility_Base::get_acf_fields_by_post_id($post_id);
        $key = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, 'familias');
        $value = $_POST['acf'][$key];
        $selected_families = [];

        update_post_meta($post_id, 'familias', $value);
        $families = is_array($value) ? $value : json_decode($value);

        if (!$families)
            $families = [];

        $query = 'SELECT ' . $col_name . ' FROM ' . $table . ' WHERE post_id = ' . $post_id;
        $prev_families = $wpdb->get_col($query);

        // Eliminate unused families
        foreach ($prev_families as $family) {
            if (!in_array($family, $families))
                $wpdb->delete($table, ['post_id' => $post_id, $col_name => $family]);
        }

        // Insert new families
        foreach ($families as $family) {
            $node_id = wp_get_post_parent_id($family);
            if (!$node_id)
                $node_id = $family;

            $arr = ['post_id' => $post_id, $col_name => $node_id];
            if ($col_name !== 'family_id') {
                $str = stripslashes($_POST['familiesSelected']);
                $selected_families = json_decode($str);

                foreach ($selected_families as $key => $selected_family) {
                    $exists_row = $wpdb->get_var("SELECT COUNT(*) FROM " . DB_POSTS_FAMILIES . " WHERE node_id = $arr[$col_name] AND post_id = $post_id AND family_id = $key");
                    if (!$exists_row) {
                        $arr['family_id'] = $key;
                        $wpdb->insert($table, $arr);
                    }
                }
            } else {
                if (!in_array($family, $prev_families))
                    $wpdb->insert($table, $arr);
            }
            foreach ($selected_families as $key => $selected_family) {
                $count = $wpdb->get_var("SELECT COUNT(*) FROM " . DB_POSTS_FAMILIES . " WHERE node_id = $key AND post_id = $post_id AND family_id = $key");
                if (!$selected_family && !$count) {
                    $arr['node_id'] = $key;
                    $arr['family_id'] = $key;
                    $wpdb->insert($table, $arr);
                }
            }
        }
    }

    private static function _relate_content($post_id)
    {
        //$families           = json_decode((string)get_field('familias', $post_id));
        $families           = explode(',', str_replace(array(']', '['), '', get_field('familias', $post_id)));
        $acf_fields         = Lf_Utility_Base::get_acf_fields_by_post_id($post_id);
        $key                = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, 'contenido_relacionado');
        $related_content    = &$_POST['acf'][$key];

        if (!$related_content)
            $_POST['acf'][$key] = [];

        if (sizeof($related_content) >= 5)
            return;

        $faqs = get_posts([
            'post_type'     => LF_CPT_QUESTION,
            'post_status'   => 'publish',
            'post__not_in'  => array($post_id),
            'numberposts'   => -1
        ]);
        $faqs = wp_list_pluck($faqs, 'ID');

        foreach ($faqs as $faq) {
            $fam = explode(',', str_replace(array(']', '['), '', get_field('familias', $faq)));
            foreach ($fam as $family_ID) {
                foreach ($families as $thefam) {
                    if ($family_ID == $thefam)
                        $related_content[] = $faq;
                }
            }
            if (sizeof($related_content) >= 5)
                break;
        }

        //if we don't find any then select at least 5
        /*if (sizeof($related_content) < 5){
            $faqs = get_posts([
                'post_type'     => LF_CPT_QUESTION,
                'post_status'   => 'publish',
                'post__not_in'  => array($post_id),
                'numberposts'   => 5 - count($related_content)
            ]);
            foreach($faqs as $faq)
                $related_content[] = $faq->ID;
        }*/
    }

    static function save_question($post_id)
    {
        if (!empty($_POST)) {
            self::_save_post($post_id, 'node_id');
            self::_relate_content($post_id);
        }
    }

    static function save_manual($post_id)
    {
        $field            = 'versiones';
        $sub_field        = 'fecha_caducidad';

        if (!empty($_POST)) {
            $date        = new DateTime('now');
            $date->add(new DateInterval('P30D'));
            $date        = $date->format('Ymd');

            $acf_fields    = Lf_Utility_Base::get_acf_fields_by_post_id($post_id);
            $key        = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, $field);
            $sub_key    = Lf_Utility_Base::get_acf_subfield_key_by_name($acf_fields, $sub_field);
            $rows        = sizeof($_POST['acf'][$key]);

            foreach ($_POST['acf'][$key] as $index => $acf) {
                if (--$rows) {
                    if (!strlen($acf[$sub_key]))
                        $_POST['acf'][$key][$index][$sub_key] = $date;
                } else
                    $_POST['acf'][$key][$index][$sub_key] = '';
            }

            self::_save_post($post_id);
        }
    }

    static function save_family($family_id)
    {
        if (!empty($_POST)) {
            $types = ['video', 'curso'];
            $acf_fields = Lf_Utility_Base::get_acf_fields_by_post_id($family_id);

            foreach ($types as $type) {
                $key = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, $type . '_destacado');
                $post_id = (int)$_POST['acf'][$key];

                // Eliminate anterior family's featured link
                $prev_featured = get_field('familia', $post_id);
                delete_field($type . '_destacado', $prev_featured);

                update_field('familia', $family_id, $post_id);
            }

            // Change Family Group
            $key = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, 'familia_agrupada');
            $group_id = (int)$_POST['acf'][$key];
            $prev_group_id = get_field('familia_agrupada', $family_id);
            if ($group_id != $prev_group_id->ID) {
                $group = (array)get_field('familias', $group_id);
                $prev_group = get_field('familias', $prev_group_id);
                foreach ($prev_group as $key => $value) {
                    if ($value == $family_id)
                        unset($prev_group[$key]);
                }
                array_push($group, $family_id);
                update_field('familias', $group, $group_id);
                update_field('familias', $prev_group, $prev_group_id);
            }
        }
    }

    static function save_family_group($group_id)
    {
        if (!empty($_POST)) {
            $acf_fields = Lf_Utility_Base::get_acf_fields_by_post_id($group_id);
            $key = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, 'familias');
            $families = $_POST['acf'][$key];
            $prev_families = get_field('familias', $group_id);
            foreach ($prev_families as $family) {
                if (!in_array($family, $families))
                    delete_field('familia_agrupada', $family);
            }
            foreach ($families as $family) {
                update_field('familia_agrupada', $group_id, $family);
            }
        }
    }

    static function save_video_course($post_id, $post)
    {
        if (!empty($_POST) && $post_id != null && $post != null) {
            $acf_fields = Lf_Utility_Base::get_acf_fields_by_post_id($post_id);
            $key = Lf_Utility_Base::get_acf_field_key_by_name($acf_fields, 'familias');

            $family_id = $_POST['acf'][$key];

            $type = $post->post_type === LF_CPT_VIDEO ? 'video' : 'curso';

            $prev_family_id = get_field('familias', $post_id);

            if (gettype($prev_family_id) == 'array' || gettype($prev_family_id) == 'object') {
                foreach ($prev_family_id as $prev_id) {
                    $family_featured = get_field($type . '_destacado', $prev_family_id);
                    if (!in_array($prev_id, $family_id) && $family_featured == $post_id)
                        delete_field($type . '_destacado', $prev_id);
                }
            } else {
                $family_featured = get_field($type . '_destacado', $prev_family_id);
                if (!in_array($prev_family_id, $family_id) && $family_featured == $post_id)
                    delete_field($type . '_destacado', $prev_family_id);
            }

            self::_save_post($post_id);
        }
    }
}
