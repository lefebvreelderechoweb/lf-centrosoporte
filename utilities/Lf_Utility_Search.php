<?php

class Lf_Utility_Search
{
	public static function get_instance()
	{
		return new self();
	}

	public static function init()
	{
		return self::get_instance();
	}

	public static function order_recent(&$items)
	{
		usort($items, function ($a, $b) {
			$t1 = strtotime($a->date);
			$t2 = strtotime($b->date);
			return $t2 - $t1;
		});
	}

	public static function order_recent_reverse(&$items)
	{
		usort($items, function ($a, $b) {
			$t1 = strtotime($a->date);
			$t2 = strtotime($b->date);
			return $t1 - $t2;
		});
	}

	public static function order_alfa(&$items)
	{
		usort($items, function ($a, $b) {
			$t1 = $a->title;
			$t2 = $b->title;
			if ($t1 > $t2) {
				return 1;
			} else if ($t1 < $t2) {
				return -1;
			} else {
				return 0;
			}
		});
	}

	public static function has_posts($elements, $post_type, $courses = [])
	{
		$q = $_GET['term'] ?? '';
		$q = strtolower($q);
		$group_courses = 0;

		if ($post_type == LF_CPT_COURSE || $post_type == 'BUSCADOR') {
			$family = get_field('codigo', $elements);
			foreach ($courses as $course) {
				$course_family_ids = $course->get_families_ids();
				if (in_array($family, $course_family_ids)) {
					if (($q && strpos(strtolower($course->title), $q) !== false) || !$q) {
						$group_courses++;
					}
				}
			}
		}
		if ($post_type != LF_CPT_COURSE) {
			global $wpdb;
			$query = "SELECT COUNT(post_id) as total FROM " . DB_POSTS_FAMILIES .
				" LEFT JOIN {$wpdb->prefix}posts p ON p.ID = post_id
				WHERE post_status = \"publish\" AND family_id = $elements
				AND post_title like \"%$q%\"";

			if ($post_type != 'BUSCADOR')
				$query .= " AND post_type = '$post_type'";

			$result = $wpdb->get_results($query);
			$group_courses = $result[0]->total;
		}


		return $group_courses ?? 0;
	}
}
