<?php
$post_types = [
  LF_CPT_COURSE   => 'Cursos',
  LF_CPT_VIDEO    => 'Videos',
  LF_CPT_MANUAL   => 'Manuales',
  LF_CPT_QUESTION => 'Preguntas Frecuentes'
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Familias Eliminadas</title>
</head>

<body>
  <div>
    <p>Hay familias que se han eliminado en el servicio.
      En Centro de Ayuda se han puesto en borrador tanto las familias como los elementos vinculados del tipo
      Cursos, Videos, Manuales y Preguntas Frequentes.</p>
    <p>Las Familias Eliminadas y sus elementos vinculados son:</p>
    <?php foreach ($elements as $key => $element) : ?>
      <p>
        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?= $key ?>" aria-expanded="false" aria-controls="collapse<?= $key ?>">
          <?= $key ?>
        </button>
      </p>
      <div class="collapse" id="collapse<?= $key ?>">
        <?php foreach ($element as $key => $item) : ?>
          <div class="card card-body">
            <ul class="list-group">
              <li class="list-group-item active" aria-current="true"><?= $post_types[$key] ?></li>
              <ul class="list-group">
                <?php foreach ($item as $post) : ?>
                  <li class="list-group-item"><a href="<?= $post['url'] ?>" target="_blank"><?= $post['title'] ?></a></li>
                <?php endforeach ?>
              </ul>
            </ul>
          </div>
        <?php endforeach ?>
      </div>
    <?php endforeach ?>
  </div>
</body>

</html>
