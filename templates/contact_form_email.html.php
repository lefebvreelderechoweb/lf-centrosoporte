<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Una persona se ha puesto en contacto</title>
</head>

<body>
    <div>
        <h3>Una persona se ha puesto en contacto:</h3>
        <p>Nombre completo: <?= $name ?></p>
        <p>Correo electrónico: <?= $email_user ?></p>
        <p>Motivo de contacto: <?= $cause ?></p>
        <p>Observaciones: <?= $observation ?></p>
    </div>
</body>

</html>
