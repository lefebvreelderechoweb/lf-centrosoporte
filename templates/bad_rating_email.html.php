<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Se ha registrado una valoración baja</title>
</head>

<body>
    <div>
        <p>Se ha registrado una valoración baja para la pagina:</p>
        <a href="<?= esc_url(get_permalink($element)) ?>"><?= get_the_title($element) ?></a>
        <br />
        <p>Para poder ver las valoraciones, acude a la herramienta: <a href="<?= get_site_url() ?>/wp-admin/admin.php?page=rate-content">ACCEDER</a></p>
    </div>
</body>

</html>
