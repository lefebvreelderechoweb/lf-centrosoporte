<?php
/**
 *
 * @link              lefebvre.es
 * @since             1.0.0
 * @package           Lf_Acf_Fields
 *
 * @wordpress-plugin
 * Plugin Name:		  Advanced Custom Fields: Related Courses
 * Plugin URI:        lefebvre.es
 * Description:       Recupera el listado de Cursos
 * Version:           1.0.0
 * Author:            Lefebvre
 * Author URI:        lefebvre.es
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lf-acf-related-courses
 */

// exit if accessed directly
if (!defined('ABSPATH'))
	exit;

class lf_acf_plugin_related_courses {

	/**
	 * Configuración del plugin
	 * @since	1.0.0
	 * @var array
	 */
	var $settings;

	/**
	 * Creación del plugin
	 * @since	1.0.0
	 */
	function __construct() {

		$this->settings = array(
			'version' 		=> '1.0.0',
			'url' 			=> plugin_dir_url(__FILE__),
			'path' 			=> plugin_dir_path(__FILE__),
			'textdomain' 	=> 'lf-acf-related-courses'
		);

		// Registro del campo
		add_action('acf/include_field_types', array($this, 'include_field')); // v5
		//add_action('wp_ajax_get_selected_families', array('Lf_Model_Family_Tree', 'ajax_get_selected_families'));
	}

	/**
	 *
	 *  Registro del campo
	 *
	 *  @since	1.0.0
	 *  @return	void
	 */
	function include_field() {

		// carga de traducciones
		load_plugin_textdomain($this->settings['textdomain'], false, plugin_basename(dirname(__FILE__)) . '/lang');

		// inicialización del campo
		include_once('includes/class-lf-acf-field-related-courses.php');
	}
}

// initialize
new lf_acf_plugin_related_courses();
