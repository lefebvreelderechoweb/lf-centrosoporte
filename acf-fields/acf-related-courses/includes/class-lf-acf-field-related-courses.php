<?php

if (!defined('ABSPATH'))
	exit;

/**
 * Descripción del campo/clase
 */
class Lf_Acf_Field_Related_Courses extends acf_field {
	/**
	 *  Configuración del campo
	 *
	 *  @since	1.0.0
	 *
	 */
	function __construct($plugin_settings) {

		/**
		 *  settings (array) Configuración del plugin
		 */
		$this->settings = $plugin_settings;

		/**
		 *  name (string) Nombre del tipo de campo
		 */
		$this->name = 'related-courses';

		/**
		 *  label (string) Descripción del campo
		 */
		$this->label = __('Cursos Relacionados', $this->settings['textdomain']);

		/**
		 *  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		 */
		$this->category = 'content';

		/**
		 * l10n (array) Array de string usados en JavaScript.
		 * @example js: var msg_error_empty = acf.l10n.input_hidden;
		 */
		$this->l10n = array(
			'error' => __('Error! Please enter a higher value', $this->settings['textdomain']),
		);

		parent::__construct();
	}


	/**
	 *
	 * Configuración/es del campo. Visible cuando se edita el campo en 'campos
	 * personalizados'
	 *
	 * @type	action
	 * @since	1.0
	 *
	 * @param	$field (array)
	 * @return	void
	 */
	function render_field_settings($field) {
	}

	/**
	 *
	 *  Html del campo en el backend
	 *
	 *  @type	action
	 *  @since	1.0
	 *
	 *  @param	$field (array) the $field Campo a renderizar
	 *  @return	void
	 */
	function render_field($field) {

		ob_start();
		extract($field);
		$name = apply_filters( 'lf_field_name_tree', $name );
		include($this->settings['path'] . '/views/acf-related-courses.php');
		echo ob_get_clean();
	}

	/**
	 *  load_field()
	 *
	 *  This filter is applied to the $field after it is loaded from the database
	 *
	 *  @type	filter
	 *  @since	1.0
	 *
	 *  @param	$field (array) the field array holding all the field options
	 *  @return	$field
	 */
	function load_field( $field ) {

		global $post;
		$post_type 					= $field[ 'post_type' ] ?? ($post->post_type ?? null);
		$post_type 					= $post_type ? get_post_type_object( $post_type ) : null;
		$field['post_type_object'] 	= $post_type;
		$field['post_type_name'] 	= $post_type && $post_type->label ? ': ' . $post_type->label : '';

		return $field;
	}

}

// initialize
new Lf_Acf_Field_Related_Courses($this->settings);
