<?php
/**
 *
 * @link              lefebvre.es
 * @since             1.0.0
 * @package           Lf_Acf_Fields
 *
 * @wordpress-plugin
 * Plugin Name:		  Advanced Custom Fields: Family Tree
 * Plugin URI:        lefebvre.es
 * Description:       Recupera el listado de Familias seleccionadas
 * Version:           1.0.0
 * Author:            Lefebvre
 * Author URI:        lefebvre.es
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lf-acf-family-tree
 */

// exit if accessed directly
if (!defined('ABSPATH'))
	exit;

class lf_acf_plugin_family_tree {

	/**
	 * Configuración del plugin
	 * @since	1.0.0
	 * @var array
	 */
	var $settings;

	/**
	 * Creación del plugin
	 * @since	1.0.0
	 */
	function __construct() {

		$this->settings = array(
			'version' 		=> '1.0.0',
			'url' 			=> plugin_dir_url(__FILE__),
			'path' 			=> plugin_dir_path(__FILE__),
			'textdomain' 	=> 'lf-acf-family-tree'
		);

		// Registro del campo
		add_action('acf/include_field_types', array($this, 'include_field')); // v5
		//add_action('wp_ajax_get_selected_families', array('Lf_Model_Family_Tree', 'ajax_get_selected_families'));
	}

	/**
	 *
	 *  Registro del campo
	 *
	 *  @since	1.0.0
	 *  @return	void
	 */
	function include_field() {

		// carga de traducciones
		load_plugin_textdomain($this->settings['textdomain'], false, plugin_basename(dirname(__FILE__)) . '/lang');

		// inicialización del campo
		include_once('includes/class-lf-acf-field-family-tree.php');
	}
}

// initialize
new lf_acf_plugin_family_tree();
?>
