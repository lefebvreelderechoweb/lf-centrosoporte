<?php

if (!defined('ABSPATH'))
	exit;

/**
 * Descripción del campo/clase
 */
class Lf_Acf_Field_Family_Tree extends acf_field {
	/**
	 *  Configuración del campo
	 *
	 *  @since	1.0.0
	 *
	 */
	function __construct($plugin_settings) {

		/**
		 *  settings (array) Configuración del plugin
		 */
		$this->settings = $plugin_settings;

		/**
		 *  name (string) Nombre del tipo de campo
		 */
		$this->name = 'family-tree';

		/**
		 *  label (string) Descripción del campo
		 */
		$this->label = __('Árbol de Familias', $this->settings['textdomain']);

		/**
		 *  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		 */
		$this->category = 'content';

		/**
		 * l10n (array) Array de string usados en JavaScript.
		 * @example js: var msg_error_empty = acf.l10n.input_hidden;
		 */
		$this->l10n = array(
			'error' => __('Error! Please enter a higher value', $this->settings['textdomain']),
		);

		parent::__construct();
	}


	/**
	 *
	 * Configuración/es del campo. Visible cuando se edita el campo en 'campos
	 * personalizados'
	 *
	 * @type	action
	 * @since	1.0
	 *
	 * @param	$field (array)
	 * @return	void
	 */
	function render_field_settings($field) {
		acf_render_field_setting($field, array(
			'label'            => __('Sólo una opción?', $this->settings['textdomain']),
			'instructions'     => __('Si está activado, se permitirá seleccionar solamente un elemento de la lista', $this->settings['textdomain']),
			'type'             => 'true_false',
			'name'             => 'only_radio',
			'ui'               => 1
		));

		acf_render_field_setting($field, array(
			'label'            => __('Árbol completo?', $this->settings['textdomain']),
			'instructions'     => __('Si está activado, aparecerá el árbol con todas las familias.
									En otro caso, aparecerán solamente los nodos de la familia seleccionada', $this->settings['textdomain']),
			'type'             => 'true_false',
			'name'             => 'complete_tree',
			'ui'               => 1
		));
	}

	/**
	 *
	 *  Html del campo en el backend
	 *
	 *  @type	action
	 *  @since	1.0
	 *
	 *  @param	$field (array) the $field Campo a renderizar
	 *  @return	void
	 */
	function render_field($field) {

		ob_start();
		extract($field);
		$name = apply_filters( 'lf_field_name_tree', $name );
		include($this->settings['path'] . '/views/acf-family-tree.php');
		echo ob_get_clean();
	}

	/**
	 *  load_field()
	 *
	 *  This filter is applied to the $field after it is loaded from the database
	 *
	 *  @type	filter
	 *  @since	1.0
	 *
	 *  @param	$field (array) the field array holding all the field options
	 *  @return	$field
	 */
	function load_field( $field ) {

		global $post;
		$post_type 					= $field[ 'post_type' ] ?? ($post->post_type ?? null);
		$post_type 					= $post_type ? get_post_type_object( $post_type ) : null;
		$field['post_type_object'] 	= $post_type;
		$field['post_type_name'] 	= $post_type && $post_type->label ? ': ' . $post_type->label : '';

		return $field;
	}

}

// initialize
new Lf_Acf_Field_Family_Tree($this->settings);
?>
