# Centro de Ayuda

El portal está desarrollado en Wordpress y consiste de 4 tipos de contenido. Todo el contenido está filtrado por familias que tiene contratado el usuario logado.

Para acceder al contenido, hay que estar logado en el portal. En otro caso, siempre se redirige a **{url} / no-autorizado**.

El usuario puede entrar de varias formas:

- **{url} / login**
- **{url} / login** / entrada_encriptada
- **{url} / login** / entrada
- **{url} / login** ? encrypt_entry
- **{url} / login** ? idEntrada
- **{url} / login** ? em & idNewsletter

Los contenidos son:

## Preguntas Frecuentes

Un listado de preguntas frecuentes ordenado por Agrupaciones de Familias, Familias y Subfamilias. El árbol de Subfamilias puede tener una profundidad de más de 3 niveles (no tiene límite).

### Lf_Shortcode_Faq_List

Se recuperan las agrupaciones de familias de siguiente forma:

```php
Array (

    [ID] => Array (                             (FAMILIA_AGRUPADA)

        [0] => WP_Post Object (                 (FAMILIA)
            [ID]
            [post_title]
            [post_type] => cpt-family
            ...
            [childs] => Array (                 (SUBFAMILIAS)

                [0] => stdClass Object (
                    [ID]
                    [post_title]
                    [childs] => Array (         (SUBFAMILIAS)

                        [0] => stdClass Object (
                            [ID]
                            [post_title]
                            [childs] => Array() (SUBFAMILIAS)
                        )

                    )
                )

            )
        )

    )
)
```

Posteriormente, se ordenan las **Agrupaciones**, **Familias** y **Subfamilias** alfabéticamente.

Después, se pinta el HTML de la **Agrupación**, **Familia**, **Subfamilias** *(de todos los niveles)* y las **Preguntas** en este orden en la función `print_faqs_list()`.

## Vídeos

Un listado de Videos con un **elemento destacado**. Si no se filtra por familias, se elige el vídeo **más reciente** de los destacados de las familias.

Si no existe ningún video destacado asignado a una familia, se elige, como destacado, el **video más reciente** de los videos disponibles.

### Lf_Shortcode_Video_List

No tiene mucho misterio. Se recuperan los videos de las familias seleccionadas/contratadas y se pinta el HTML con el listado de Videos.

## Manuales

Un listado de Manuales. Cada manual puede tener **como mucho 2 versiones/ediciones, actual y anterior**.

La versión anterior es accesible desde el enlace que aparecerá por debajo de la portada del manual. En todos los casos, al clicar[^1] en un manual, se abre una **pestaña nueva** con el **visor PDF** con la versión del manual seleccionada.

[^1]: Los manuales no tienen la vista del detalle, a diferencia de los demás tipos de contenido que sí la tienen.

## Cursos

El listado y el detalle de los cursos se alimentan con los datos del servicio:

- [PRE] https://led-pre-servicegwint.lefebvre.es/serviceformacionproductos/
- [PRO] https://led-servicegwinterno.lefebvre.es/serviceformacionproductos/

Los metodos son:

- `Cursos/RecuperarCursosAsistente` *(Cursos a los que puede inscribirse el usuario)*
- `Cursos/RecuperarCursos` *(Todos los cursos de Centro de Ayuda)*

Para la inscripción al curso se utiliza el método

- `Asistentes/InscribirAsistenteACurso`

___

Cada apartado lleva un **sidebar** con el filtrado por familas agrupadas / familias, listado de elementos destacados, contenido relacionado y novedades.

Está alocado en: `theme/lf-centrosoporte-theme/sidebar.php`

---

El buscador general busca por los 4 contenidos y devuelve los resultados resumidos en 4 elementos, con posibilidad de expandirlos.

Está alocado en: `Lf_Shortcode_Search_List`

---

Hay dos tipos de campos ACF creados para este portal.

El primero es el **árbol de subfamilias** para las **preguntas frecuentes** [**acf-family-tree**] que a su vez llama a **Lf_Shortcode_Family_Tree**.

El segundo es para **relacionar** cualquier tipo de contenido con un curso [**acf-related-courses**] que a su vez llama a **Lf_Shortcode_Related_Courses**.

---

Todos los tipos de contenido tienen el **NPS** (valoración de contenido) que se muestra en el **detalle del contenido** (menos los cursos) o en el **visor vídeo del curso**. `shortcode/lf_shortcode_rate_content`

En el Backend hay un apartado con el listado de las valoraciones con filtros. `shortcode/backend/lf_shortcode_rate_content_list`

---

En la home, se muestra un **carousel** con uno de cada tipo de contenido aleatoriamente. `shortcode/lf_shortcode_carousel`

---

Cuando un contenido se asigna a una familia como el contenido destacado, se relaciona en la bbdd bilateralmente. Es decir, el contenido tiene la familia asignada, y la familia tiene el contenido asignado. Si se cambia la familia del contenido, se quita la relación desde la familia también.


En el caso de FAQs y subfamilias, es más complejo. Al estar relacionado con uno o varios nodos del árbol, esta relación se guarda en la tabla **lf_posts_families** de forma:

```
La Familia tiene Nodos

El Nodo tiene Nodos y Preguntas Frecuentes
```

Hay dos tipos de Nodos, **únicos** y **compartidos**. Los Nodos compartidos son aquellos que pueden pertenecer a **varias familias** (el mísmo Nodo con el mismo ID).

En este caso, cuando una Pregunta Frecuente o una Subfamilia se asigna a un Nodo compartido, se guarda en **todos** los nodos[con el mismo ID] de **todas** las Familias a las que pertenece.

Estos procesos se realizan en `Lf_Utility_Post.php`

---

# IMPORTANTE

Cuando el servicio de API se suba a PRO, sustituir en `lf-login/includes/models/class-lf-user-model-service-com-tools.php`:

- línea **279** por **278**
- línea **283** por **282**
- línea **267** por **268**

En `lf-centrosoporte/services/Lf_Service_Url.php` eliminar las líneas **78** y **79**.
