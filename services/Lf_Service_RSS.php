<?php
class Lf_Service_RSS
{
    const ITEMS_PER_PAGE = 10;
    const TYPES = [
        LF_CPT_VIDEO_URL    => LF_CPT_VIDEO,
        LF_CPT_MANUAL_URL   => LF_CPT_MANUAL,
        LF_CPT_QUESTION_URL => LF_CPT_QUESTION
    ];

    static function init($comments)
    {
        $type           = $_GET['type'] ?? '';
        if (!array_key_exists($type, self::TYPES))
            $post_type  = array_values(self::TYPES)[0];
        else
            $post_type  = self::TYPES[$type];

        $limit          = $_GET['limit'] ?? self::ITEMS_PER_PAGE;
        $page           = $_GET['page'] ?? 1;
        if (!is_numeric($page)) {
            $page = 1;
        }

        $title      = $_GET['title'] ?? '';
        $family     = $_GET['family'] ?? '';
        $date_start = $_GET['date_start'] ?? '';
        $date_end   = $_GET['date_end'] ?? '';

        if ($family)
            $family    = Lf_Model_Import::check_exist_by_post_meta_value(LF_CPT_FAMILY, 'codigo', $family);

        $args = [
            'title'             => $title,
            'post_type'         => $post_type,
            'posts_per_page'    => $limit,
            'limit'             => $limit,
            'page'              => $page - 1,
            'family_id'         => $family,
            'is_feed'           => true
        ];

        if ($date_start) {
            if (preg_match('/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/', $date_start))
                $args['dates']['after'] = $date_start . ' 00:00:00';
        }

        if ($date_end) {
            if (preg_match('/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/', $date_end))
                $args['dates']['before'] = $date_end . ' 23:59:59';
        }

        $posts = Lf_Utility_Base::get_list($args);

        if ($post_type == LF_CPT_QUESTION) {
            foreach ($posts as $post) {
                $families = [];
                if ($post->family) {
                    foreach ($post->family as $family)
                        $families[] = get_post($family);
                    $post->family = $families;
                }
            }
        }

        self::render($posts);
    }

    static function render($posts)
    {
        $root = '<rss version="2.0"></rss>';

        $postsXML  = new SimpleXMLElement($root);
        $channel  = $postsXML->addChild('channel');

        $channel->addChild('title', 'Centro de Ayuda');
        $channel->addChild('link',  'https://' . $_SERVER['HTTP_HOST'] . '/feed');
        $channel->addChild('language', 'es');

        foreach ($posts as $post) {
            $title          = $post->title ?? null;
            $families       = $post->family ?? [];
            $date           = $post->date ?? null;
            $description    = $post->description ?? '';
            $image          = null;

            $item           = $channel->addChild('item');

            if ($title) {
                $item->addChild('title', $title);
            }

            if ($date) {
                $item->addChild('pubDate', $date);
            }

            if ($families) {
                $item->addChild('familias', implode(', ', array_column($families, 'post_title')));
            }

            if ($description) {
                $item->addChild('description', $description);
            }

            if ($post->post_type == LF_CPT_VIDEO) {
                $image = $post->image ?? $post->preview ?? null;
            } else if ($post->post_type == LF_CPT_MANUAL) {
                $versions = Lf_Model_Manual::get_versions($post->ID);
                $image = wp_get_attachment_image_src($versions['imagen'], array('840', '600'));
                if (is_array($image))
                    $image = $image[0];
            }
            if ($image) {
                $img = $item->addChild('image');
                $img->addChild('url', $image);
                $item->addChild('urlImage', $image);
            }

            $id_nl  = $_GET['idNewsletter'] ?? 0;
            $url    = get_permalink($post->ID) . "?idNewsletter=$id_nl";
            $item->addChild('url', $url);
        }

        Header("HTTP/1.1 200 OK");
        Header('Content-type: text/xml');
        echo $postsXML->asXML();
    }
}
