<?php
class Lf_Service_Url
{

    /**
     * Comprueba si se debe lanzar un error 410
     * @global type $wpdb
     * @global type $post
     */
    static function check_410()
    {
        global $wpdb;
        global $post;

        if (strlen($_SERVER['REQUEST_URI']) > 1 && trim($_SERVER['REQUEST_URI']) != "") {

            $slug = basename($_SERVER['REQUEST_URI']);
            $slugTrashed = $slug . '__trashed';

            $query = "  SELECT wpp.*
						FROM   " . $wpdb->prefix . "posts wpp
						WHERE   wpp.post_name = '$slug'
						OR      wpp.post_name = '$slugTrashed'
						LIMIT 100";

            $posts = $wpdb->get_results($query);

            if (count($posts) > 0) {
                $is410 = true;
                foreach ($posts as $post) {
                    $is410 = ($post->post_status !== 'publish' && $post->post_status !== 'inherit');
                    if (!$is410) {
                        break;
                    }
                }

                if ($is410) {
                    status_header(410);
                    if (file_exists(STYLESHEETPATH . '/410.php')) {
                        include STYLESHEETPATH . '/410.php';
                        exit();
                    }
                }
            }
        }
    }

    private static function _check_defined_emails(string $email): string
    {
        
        $emails_pre = [
            'r.villarrubia@lefebvre.es'     => 'E1654331',
            'a.perez.mera-ext@lefebvre.es'  => 'E1654744'
        ];
        $emails_pro = [
            'formacion@lefebvre.es'         => 'E1657689',
            'cc.sanchez@lefebvre.es'        => 'E1703824',
            'r.villarrubia@lefebvre.es'     => 'E1661478',
            'a.perez.mera-ext@lefebvre.es'  => 'E1706491'
        ];

        if (getenv('ENVIRONMENT') === 'PROD') {
            return $emails_pro[$email] ?? '';
        } else {
            return $emails_pre[$email] ?? '';
        }
    }

    static function init()
    {
        $url            = $_SERVER['REQUEST_URI'] ? trim($_SERVER['REQUEST_URI']) : null;
       
        $id_newsletter   = $_GET['idNewsletter'] ?? 0;
        $email          = $_GET['em'] ?? 0;
        $entry          = $_GET['encrypt_entry'] ?? $_GET['idEntrada'] ?? 0;
       
        if (($id_newsletter && $email) || $entry) {
            if (!$entry) {
                // TODO: eliminar despues de las pruebas
                $entry = self::_check_defined_emails($email);
                if (!$entry)
                    // TODO: fin de TODO
                    $token_response = Lf_Api_Rest_NewsLetter::get_token_newsletter(NEWSLETTER_USER,NEWSLETTER_PASS);
                    $result = Lf_Api_Rest_NewsLetter::get_user_pro_newsletter($email, $id_newsletter, $token_response);
                    //$result = Lf_Api_Rest_NewsLetter::get_user_pro_newsletter_old($email, $idNewsletter);
                    $result = $result->result ?? null;
                    $entry = $result->idUsuarioPro ?? '';
            }
            $uri = explode('?', $_SERVER['REQUEST_URI']);
            $uri = get_home_url(null, $uri[0]);
            do_action('lf_login_redirect', $entry, $uri);
        }

        if ($url) {
            switch ($url) {

                case (preg_match('/go-to-centinela.*/', $url) ? true : false):

                    $lf_user = Lf_Login::get_lf_user();

                    if (empty($lf_user))
                        return '';

                    $tools = $lf_user ? $lf_user->get_tool_mini_hub() : [];
                    if (empty($tools))
                        return '';

                    // Find idHerramienta == 11 and check if indAcceso == 1 in $tools
                    $tool = array_filter($tools, function ($t) {
                        return $t->idHerramienta == 11 && $t->indAcceso == 1;
                    });

                    if (!empty($tool)) {
                        $tool = array_shift($tool);
                        $response = Lf_Api_Rest_Claves::get_token_sso_with_entry([
                            'id_user_pro' => $lf_user->get_entry_id(),
                            'id_client_nav' => $lf_user->getNavisionClientId()
                        ]);
                        $token = $response->oResultado->strToken ?? '';
                        $tool->url = str_replace('{token}', $token, URL_CENTINELA);
                        header('Location: ' . $tool->url);
                    }

                    die;
                    break;

                case (preg_match('/import-families.*/', $url) ? true : false):
                    Lf_Model_Family::import();
                    exit();
                    break;
                case (preg_match('/feed.*/', $url) ? true : false):
                    break;
                case (preg_match('/cursos\/\d+/', $url) ? true : false):
                    Lf_Shortcode_Course_Detail::call();
                    die();
                    break;
                default:
                    if (!is_user_logged_in() && !preg_match('/no-autorizado.*/', $url) && !preg_match('/login.*/', $url))
                        wp_redirect(home_url('no-autorizado'));
                    self::check_410();
                    break;
            }
        }
    }
}
