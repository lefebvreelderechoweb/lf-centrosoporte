<?php
class Lf_Service_Clone
{
	/**
	 * Añade un botón para clonar el elemento actual
	 *
	 * @param  int $post_id
	 * @return void
	 */
	private static function _create_editor_clone_btn(int $post_id)
	{
		echo "<a href='#' id='clone_btn' class='button-secondary' data-post-id='$post_id'>Clonar</a>";
	}

	/**
	 * Función para clonar un elemento
	 *
	 * @param  WP_Post $post
	 * @return void
	 */
	static function clone(WP_Post $post)
	{
		global $wpdb;
		$posts_families_table = DB_POSTS_FAMILIES;

		$post_id = wp_insert_post([
			'post_type'		=> $post->post_type,
			'post_title'	=> $post->post_title . ' (Clon)',
			'post_status'	=> 'publish',
			'post_parent'	=> $post->post_parent,
			'post_author'	=> get_current_user_id(),
		]);

		if ($post_id) {
			$post_meta = get_post_meta($post->ID);
			foreach ($post_meta as $key => $value) {
				$parsed_value = unserialize($value[0]);

				if (!unserialize($value[0])) {
					$parsed_value = $value[0];
				}

				update_post_meta($post_id, $key, $parsed_value);
			}
		}

		$query			= "SELECT family_id, node_id FROM $posts_families_table WHERE post_id = $post->ID";
		$post_families	= $wpdb->get_results($query);

		foreach ($post_families as $family) {
			$arr = [
				'post_id'	=> $post_id,
				'family_id'	=> $family->family_id,
				'node_id'	=> $family->node_id
			];
			$wpdb->insert($posts_families_table, $arr);
		}

		return $post_id;
	}

	/**
	 * Crea el botón de clonación en el editor de post e implementa la lógica de clonación
	 *
	 * @param  WP_Post $post
	 * @return void
	 */
	static function clone_post(WP_Post $post)
	{
		$supported_post_types = [LF_CPT_QUESTION];
		$post_type = get_post_type($post);

		if (in_array($post_type, $supported_post_types)) {
			self::_create_editor_clone_btn($post->ID);
		}
	}
}
