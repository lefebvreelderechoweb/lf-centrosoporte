<?php
/**
 *
 * @link              lefebvre.es
 * @since             1.0.0
 * @package           lf_project
 *
 * @wordpress-plugin
 * Plugin Name:       LF Centro de Soporte
 * Plugin URI:        lefebvre.es
 * Description:       Funcionalidad específica del plugin.
 * Version:           1.0.0
 * Author:            Lefebvre
 * Author URI:        lefebvre.es
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lf-project
 * Domain Path:       /languages
 */
if (!defined('DS')) {
	define('DS', '/');
}

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die; 
}



// Dependencias de plugins
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('lf-lib/lf-lib.php') && false) {
	add_action('admin_notices', function() {
		$class = 'notice notice-error';
		$message = __('LF CENTRO SOPORTE está activado pero no en funcionamiento debido a que requiere del plugin LF LIB para su funcionamiento.', 'lf-project');

		printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
	});
} elseif (!is_plugin_active('lf-login/lf-login.php') && false) {
	add_action('admin_notices', function() {
		$class = 'notice notice-error';
		$message = __('LF CENTRO SOPORTE está activado pero no en funcionamiento debido a que requiere del plugin LF LOGIN para su funcionamiento.', 'lf-project');

		printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
	});
} else {
	// Coloca el plugin en la última posición de carga
	if ($plugins = get_option('active_plugins')) {
		$key = array_search('lf-centrosoporte/lf-project.php', $plugins);
		if ($key !== false) {
			array_splice($plugins, $key, 1);
			$plugins[] = 'lf-centrosoporte/lf-project.php';
			update_option('active_plugins', $plugins);
		}
	}
	/**
	 * Currently plugin version.
	 * Start at version 1.0.0 and use SemVer - https://semver.org
	 * Rename this for your plugin and update it as you release new versions.
	 */
	define('LF_PLUGIN_VERSION', '1.0.0');
	define('LF_PLUGIN_DIR', plugin_dir_path(__FILE__));
	define('LF_PLUGIN_URL', plugin_dir_url(__FILE__));
	define('LF_THEME_DIR', get_template_directory_uri());
 

	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path(__FILE__) . 'includes/class-lf-project.php';

	/**
	 * Begins execution of the plugin.
	 *
	 * Since everything within the plugin is registered via hooks,
	 * then kicking off the plugin from this point in the file does
	 * not affect the page life cycle.
	 *
	 * @since    1.0.0
	 */
	function run_lf_project() {		
		$plugin = new Lf_Project();
		$plugin->run();
	}

	run_lf_project();
}
?>