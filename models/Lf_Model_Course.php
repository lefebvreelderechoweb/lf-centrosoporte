<?php

class Lf_Model_Course extends Lf_Model_Base
{

	const POST_TYPE = LF_CPT_COURSE;

	public function __construct($course)
	{
		$this->ID				= $course->idCurso;
		$this->title			= $course->nombre ?? '';
		$this->url				= get_home_url(null, 'cursos/' . $this->ID);
		$this->families			= $course->lstFamilias ?? [];
		$this->resumen			= nl2br($course->resumenContenido ?? '');
		$this->recommend		= $course->recomendadoPara ?? '';
		$this->program			= nl2br($course->observaciones ?? '');
		$this->requirements		= nl2br($course->requisitosTecnicos ?? '');
		$this->image			= $course->urlImagen ?? '';
		$this->subscribe_modal	= nl2br($course->textoDescripcionInscripcion ?? '');
		$this->duration			= $course->duracion ?? '';
		$this->start_new		= $course->fechaInicioNovedad ? strtotime($course->fechaInicioNovedad) : '';
		$this->end_new			= $course->fechaFinNovedad ? strtotime($course->fechaFinNovedad) : '';
		$this->level			= $course->descNivel ?? '';
		$this->type				= $course->descTipoCurso ?? '';
		$this->sessions			= Lf_Model_Session::parse_sessions($course->lstSesiones ?? []);
		$this->video			= $course->video ?? [];
		$this->post_type		= self::POST_TYPE;

		// Set next_session the closest session by date
		$this->next_session		= '';
		$this->subscribed		= false;

		foreach ($this->sessions as $session) {
			if (!$this->next_session || $session->start_date < $this->next_session)
				$this->next_session = $session->start_date;
			if ($session->subscribed)
				$this->subscribed = true;
		}

		$this->next_session_formatted	= $this->next_session ? date('d/m/Y - H:i', $this->next_session) : '';
		$this->date						= $this->next_session_formatted;
	}

	public function get_families_ids()
	{
		$ids = [];
		foreach ($this->families as $family)
			$ids[] = $family->idFamilia;
		return $ids;
	}

	public function get_wp_families_ids()
	{
		return Lf_Utility_Base::get_wp_families_ids($this->get_families_ids());
	}

	public function get_families_names()
	{
		$names = [];
		foreach ($this->families as $family)
			$names[] = $family->descFamilia;
		return $names;
	}

	static function order_courses_by($courses, $field = '')
	{
		if ($field && $courses) {
			usort($courses, function ($a, $b) use ($field) {
				if ($a->$field && $b->$field) {
					return $a->$field < $b->$field ? -1 : 1;
				} else if ($a->$field) {
					return -1;
				} else if ($b->$field) {
					return 1;
				}
			});
		}

		return $courses;
	}

	static function filter_courses_by_date($courses, $field = '', $type = '>=')
	{
		if ($field && $courses) {
			$now = new DateTime('now');
			$now = $now->getTimestamp();

			$courses = array_filter($courses, function ($course) use ($now, $field, $type) {
				if ($type == '>=') {
					return $course->$field && $course->$field >= $now;
				} else if ($type == '<=') {
					return $course->$field && $course->$field <= $now;
				}
			});
		}

		return $courses;
	}

	static function order_courses_by_next_session($courses)
	{
		$courses = self::filter_courses_by_date($courses, 'next_session');

		return self::order_courses_by($courses, 'next_session');
	}
}
