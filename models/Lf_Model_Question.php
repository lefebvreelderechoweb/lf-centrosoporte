<?php

class Lf_Model_Question extends Lf_Model_Base
{

    const POST_TYPE = LF_CPT_QUESTION;

    public function __construct()
    {
    }

    static function get_instance()
    {
        return new self();
    }

    function get($id, $args = [])
    {
        parent::get($id, $args);

        $this->post_type = self::POST_TYPE;
        $this->family = json_decode((string)get_field('familias', $id));
        $this->main_family = self::get_faq_families($id);
        $this->url = get_permalink($id);
    }

    static function get_faq_families($id)
    {
        global $wpdb;

        $query = 'SELECT family_id FROM ' . DB_POSTS_FAMILIES . ' WHERE post_id = ' . $id;
        return $wpdb->get_col($query);
    }

    static function get_exclusive_family_questions(int $family)
    {
        global $wpdb;
        $found = [];

        $query = 'SELECT post_id as ID, post_title as title, family_id, COUNT(post_id) FROM '
            . DB_POSTS_FAMILIES . ' LEFT JOIN ' . $wpdb->prefix
            . 'posts p ON p.ID = post_id WHERE post_status = "publish" GROUP BY post_id HAVING COUNT(post_id) = 1';
        $result = $wpdb->get_results($query);

        foreach ($result as $item) {
            if ($item->family_id == $family)
                $found[] = $item;
        }

        return $found;
    }
}
