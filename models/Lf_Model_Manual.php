<?php

class Lf_Model_Manual extends Lf_Model_Base
{

	const POST_TYPE = LF_CPT_MANUAL;

	public function __construct()
	{
	}

	static function get_instance()
	{
		return new self();
	}

	function get($id, $args = [])
	{
		parent::get($id, $args);
		$this->post_type = self::POST_TYPE;
		$this->family = get_field('familias', $id);
		$this->visibile = get_field('visible', $id);
		$this->pdf = get_field('pdf', $id);
		$this->image = get_field('imagen', $id);
		$this->url = get_permalink($id);
		$this->expiration_date = get_field('fecha_caducidad', $id);
		$this->content = get_field('contenido', $id);
	}

	static function get_pdf_url($url)
	{
		$response = file_get_contents($url);
		$response = json_decode($response);

		return $response->pdfUrl ?? '';
	}

	static function get_versions($manual_id)
	{
		$versions = [];
		$n_versions = sizeof((array) get_field('versiones', $manual_id));
		$lf_user = Lf_Login::get_lf_user();
		if ($lf_user)
			$user_ei = $lf_user->get_entry_id();
		else {
			$user_id = get_current_user_id();
			if ($user_id) {
				$user = get_user_by('id', $user_id);
				$user_ei = $user->data->user_nicename . $user_id;
			} else {
				$user_ei = '';
			}
		}
		$link = VISOR_PDF . 'url?userId=' . $user_ei . '&siteId=centrodeayuda.lefebvre.es&pdfUrl=';
		$pdf = get_field('versiones_' . --$n_versions . '_pdf', $manual_id);
		$url = $pdf['url'];

		$versions['actual'] = self::get_pdf_url($link . $url);
		$versions['imagen'] = $pdf['ID'];
		if ($n_versions != 0) {
			$fecha = get_field('versiones_' . --$n_versions . '_fecha_caducidad', $manual_id);
			$today_dt = new DateTime();
			$expire_dt = DateTime::createFromFormat('d/m/Y', $fecha);

			if ($today_dt < $expire_dt) {
				$url = get_field('versiones_' . $n_versions . '_pdf', $manual_id)['url'];

				$versions['anterior'] = self::get_pdf_url($link . $url);
				$versions['limite'] = $fecha;
			}
		}

		return $versions;
	}
}
