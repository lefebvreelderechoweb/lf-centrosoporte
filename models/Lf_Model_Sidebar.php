<?php

class Lf_Model_Sidebar extends Lf_Model_Base
{

	const VIEWS = ['related', 'search', 'news', 'next_courses'];

	const PAGES = [
		'VÍDEOS'                => LF_CPT_VIDEO,
		'CURSOS'                => LF_CPT_COURSE,
		'PREGUNTAS FRECUENTES'  => LF_CPT_QUESTION,
		'MANUALES'              => LF_CPT_MANUAL,
		'CONTACTO'              => ''
	];

	const CLASSES = [
		LF_CPT_VIDEO            => 'lf-icon-video',
		LF_CPT_COURSE           => 'lf-icon-learn',
		LF_CPT_QUESTION         => 'lf-icon-question',
		LF_CPT_MANUAL           => 'lf-icon-book'
	];

	const NEWS_LIMIT = 4;

	public $post_name;
	public $post_type;
	public $layout;

	public static function get_post_name()
	{
		if (is_page())
			$post_name = get_the_title();
		else {
			$post_type = get_post_type();
			$post_type_object = get_post_type_object($post_type);
			$post_name = $post_type_object->label ?? '';
		}
		return mb_strtoupper($post_name);
	}

	public function get_layout()
	{
		$this->post_name = self::get_post_name();

		if (is_page()) {
			if ($this->post_name == 'CURSOS')
				return array_slice(self::VIEWS, 1, 3);
			else if ($this->post_name == 'CONTACTO')
				return ['contact'];
			else if ($this->post_name == 'MANUALES')
				return ['search'];
			else if ($this->post_name == 'BUSCADOR')
				return ['search'];
			else
				return array_slice(self::VIEWS, 1, 2);
		} else if ($this->post_name == 'CURSOS')
			return self::VIEWS;

		return array_slice(self::VIEWS, 0, 3);
	}

	public function get_post_type()
	{
		$this->post_name = self::get_post_name();

		return self::PAGES[$this->post_name] ?? 'BUSCADOR';
	}

	public function get_news($courses)
	{
		$this->post_type = self::PAGES[$this->post_name] ?? LF_CPT_COURSE;
		$post_id = get_the_ID();

		if ($this->post_type == LF_CPT_COURSE) {
			$news = Lf_Model_Course::filter_courses_by_date($courses, 'end_new', '>=');
			$news = Lf_Model_Course::order_courses_by($news, 'start_new');
			$news = array_slice($news, 0, self::NEWS_LIMIT);
		} else {
			$args = [
				'post_type' => $this->post_type,
				'limit'     => self::NEWS_LIMIT
			];
			if ($post_id)
				$args['post__not_in'] = [$post_id];

			$news = Lf_Utility_Base::get_list($args);
			if ($this->post_type == LF_CPT_QUESTION) {
				foreach ($news as $new)
					$new->main_family = array_unique($new->main_family);
			}
		}

		return $news;
	}

	public function get_related()
	{
		$post_id            = get_the_ID();
		$course_id			= 0;
		$items              = [];


		if (!$post_id) {
			$related = Lf_Utility_Course::get_related_elements();

			foreach ($related as $element) {
				$id			= $element->related_id;

				if ($element->post_type == LF_CPT_COURSE) {
					$item			= Lf_Utility_Course::get_course_by_id($id);
					if (!$item)
						continue;

					$type			= LF_CPT_COURSE;
					$item->class	= self::CLASSES[$type];

					$items[]		= $item;
				} else {
					$item			= wp_cache_get("$element->post_type\_$id");
					if ($item === false) {
						$instance		= Lf_Utility_Base::get_instance_by_post_type($element->post_type);
						$item			= new $instance;
						$item->get($id);
						$type			= $element->post_type;
						$item->class	= self::CLASSES[$type];
						wp_cache_set("$element->post_type\_$id", $item);
					}


					$items[]		= $item;
				}
			}
		} else {
			$related            = get_field('contenido_relacionado', $post_id);
			$related_courses    = (string)get_field('cursos_relacionados', $post_id);

			if (!empty($related)) {
				// Remove items if not published
				$related = array_filter((array)$related, function ($element) {
					if ($element->post_status == 'publish')
						return $element;
				});
				foreach ($related as $element) {
					$id		= $element->ID;

					$item	= wp_cache_get("$element->post_type\_$id");
					if ($item === false) {
						$instance = Lf_Utility_Base::get_instance_by_post_type($element->post_type);

						$item = new $instance;
						$item->get($id);

						$type = $element->post_type;
						$item->class = self::CLASSES[$type];
						wp_cache_set("$element->post_type\_$id", $item);
					}

					$items[] = $item;
				}
			}

			if (!empty($related_courses)) {
				$related_courses = explode(',', $related_courses);
				foreach ($related_courses as $element) {
					$item = Lf_Utility_Course::get_course_by_id($element);
					if (!$item)
						continue;

					$type = LF_CPT_COURSE;
					$item->class = self::CLASSES[$type];

					$items[] = $item;
				}
			}
		}

		return $items;
	}

	private function get_nodes($nodes, &$related)
	{
		global $wpdb;

		foreach ($nodes as $node) {
			$query = 'SELECT DISTINCT p.ID FROM ' . $wpdb->posts . ' p LEFT JOIN '
				. DB_POSTS_FAMILIES . ' ON p.ID = post_id WHERE node_id = ' . $node
				. ' AND post_status = "publish" AND post_type = "' . LF_CPT_QUESTION . '"';

			$questions = $wpdb->get_col($query);
			$related = array_merge($related, $questions);

			$query = 'SELECT DISTINCT p.ID FROM ' . $wpdb->posts . ' p LEFT JOIN '
				. DB_POSTS_FAMILIES . ' ON p.ID = post_id WHERE family_id = ' . $node
				. ' AND post_status = "publish"';

			$this->get_nodes($wpdb->get_col($query), $related);
		}
	}

	public function get_related_faq($limit = 5)
	{
		global $wpdb;
		$post_id = get_the_ID();
		$related = [];

		$query = 'SELECT DISTINCT p.ID FROM ' . $wpdb->posts . ' p LEFT JOIN '
			. DB_POSTS_FAMILIES . ' ON p.ID = node_id WHERE post_id = ' . $post_id
			. ' AND post_status = "publish"';

		$nodes = $wpdb->get_col($query);

		$this->get_nodes($nodes, $related);

		$related = array_unique($related);

		foreach ($related as $key => $value) {
			if ($value == $post_id)
				unset($related[$key]);
		}

		shuffle($related);
		$related = array_slice($related, 0, $limit);

		$items = [];
		foreach ($related as $id) {
			$item = new Lf_Model_Question;
			$item->get($id);
			$items[] = $item;
		}

		$related = array_map(function ($element) {
			$element->class = self::CLASSES[LF_CPT_QUESTION];
			return $element;
		}, $items);

		return $related;
	}
}
