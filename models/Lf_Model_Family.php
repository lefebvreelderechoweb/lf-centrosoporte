<?php

class Lf_Model_Family extends Lf_Model_Base {

	const POST_TYPE = LF_CPT_FAMILY;

	public function __construct() {
	}

	static function get_instance() {
		return new self();
	}

	public function get($id, $args = []) {
		parent::get($id, $args);
		$this->post_type = self::POST_TYPE;
		$this->code = (int)get_field('codigo', $id);
	}

	static function import() {
		$data = Lf_WS_Formation::get_families() ?? null;
		if ($data) {
			// Spanenglish de M.C.
			$items = $data->data->Familia ?? [];

			self::check_families($items);

			foreach ($items as $item) {
				$title = $item->Tittle ?? '';
				$code = $item->Code ?? '';

				if ($title && $code) {

					$args = [
						'post_type' => self::POST_TYPE,
						'post_title' => $title
					];

					$post_id = Lf_Model_Import::check_exist_by_post_meta_value(self::POST_TYPE, 'codigo', $code);

					if (!$post_id) {
						$args['post_status'] = 'draft';
						$post_id = wp_insert_post($args);
					} else {
						$args['ID'] = $post_id;
						wp_update_post($args);
					}
					$metas = [
						'codigo' => $code,
						'titulo' => $title,
						'estado' => 'publish'
					];
					Lf_Model_Import::update_post_metas($post_id, $metas);
				}
			}
		}
		return '1';
	}

	/**
	 * Get the array of user's families that exists on centrosoporte
	 *
	 * @return array Families that has user and that exists on the platform
	 */
	static function get_families(): array {
		$user = Lf_Login::get_lf_user();

		if (empty($user) && !is_user_logged_in())
			return [];

		$args = [
			'post_type'		=> LF_CPT_FAMILY,
			'post_status'	=> 'publish',
			'numberposts'	=> -1
		];

		$families = get_posts($args);
		foreach ($families as $key => $family) {
			$code = get_post_meta($family->ID, 'codigo', true);
			if (!empty($user) && !$user->has_area($code))
				unset($families[$key]);
		}
		return $families;
	}

	/**
	 * check_families
	 *
	 * @param  array $families
	 * @return void
	 */
	static function check_families(array $families) {
		$prev_families = self::get_families();
		$families = array_column($families, 'Code');

		$removed_families = [];

		foreach ($prev_families as $family) {
			$code = get_field('codigo', $family->ID);
			if (!in_array($code, $families))
				$removed_families[] = $family->ID;
		}
		if (sizeof($removed_families)) {
			$elements = Lf_Utility_Base::prepare_dependant_posts($removed_families);
			Lf_Utility_Base::send_removed_family_mail($elements);
		}
	}

	static function get_family_groups(array $families) {
		$group = [];

		foreach ($families as $family) {
			$family_group = get_field('familia_agrupada', $family);
			if ($family_group && $family_group->ID)
				$group[$family_group->ID][] = $family;
		}

		return $group;
	}
}
