<?php

class Lf_Model_Nps
{

    public function __construct()
    {
    }

    static function render_contents()
    {
        echo Lf_Shortcode_Rate_Content_List::call([]);
    }

    static function get_post_types()
    {
        $post_types = [
            LF_CPT_VIDEO    => "Vídeos",
            LF_CPT_QUESTION => "Preguntas Frecuentes",
            LF_CPT_COURSE   => "Cursos"
        ];

        return $post_types;
    }

    static function get_rating_levels()
    {
        $rating_levels = [
            "Nada útil",
            "Poco útil",
            "Algo útil",
            "Bastante útil",
            "Muy útil"
        ];

        return $rating_levels;
    }

    static function find_lf_user_by_ei($user_ei)
    {
        global $wpdb;

        $query = 'SELECT DISTINCT user_id FROM ' . DB_NPS_USERS;

        $users = $wpdb->get_col($query);
        foreach ($users as $user_id) {
            $lf_user = Lf_Login::get_lf_user($user_id);

            if ($lf_user) {
                $ei = $lf_user->get_entry_id();
                if ($ei == $user_ei)
                    return $user_id;
            }
        }
        return 0;
    }

    static function find_lf_user_by_navision($user_navision)
    {
        global $wpdb;
        $user_ids = [];

        $query = 'SELECT DISTINCT user_id FROM ' . DB_NPS_USERS;

        $users = $wpdb->get_col($query);
        foreach ($users as $user_id) {
            $lf_user = Lf_Login::get_lf_user($user_id);

            if ($lf_user) {
                $id_navision = $lf_user->getNavisionClientId();
                if ($id_navision == $user_navision)
                    $user_ids[] = $user_id;
            }
        }
        return $user_ids;
    }

    private static function _append_query($query)
    {
        $title          = $_POST['title'] ?? null;
        $id_navision    = $_POST['id_navision'] ?? null;
        $id_user        = $_POST['id_user'] ?? null;
        $post_type      = $_POST['post_type'] ?? null;
        $rating         = $_POST['rating'] ?? null;
        $from           = $_POST['from'] ?? null;
        $until          = $_POST['until'] ?? null;

        if ($title && $post_type != LF_CPT_COURSE)
            $query .= ' AND post_title LIKE "%' . $title . '%"';
        if ($id_user) {
            $id_user = self::find_lf_user_by_ei($id_user);
            if (!$id_user)
                $id_user = 0;
            $query .= ' AND user_id = "' . $id_user . '"';
        }
        if ($id_navision) {
            $id_user = self::find_lf_user_by_navision($id_navision);
            if (!$id_user)
                $id_user = [0];
            $query .= ' AND user_id IN (' . implode(', ', $id_user) . ')';
        }
        if ($post_type && $post_type != LF_CPT_COURSE)
            $query .= ' AND post_type = "' . $post_type . '"';
        if ($rating)
            $query .= ' AND value = ' . $rating;
        if ($from)
            $query .= ' AND date >= "' . $from . '"';
        if ($until)
            $query .= ' AND date <= "' . $until . ' 23:59:59' . '"';

        $query .= ' ORDER BY date DESC';

        return $query;
    }

    static function get_ratings()
    {
        $post_type      = $_POST['post_type'] ?? null;
        $title          = $_POST['title'] ?? null;
        $results        = [];
        global $wpdb;

        if ($post_type != LF_CPT_COURSE) {
            $query      = 'SELECT user_id, value, comment, date, post_title, post_type FROM '
                . DB_NPS_USERS . ' n LEFT JOIN ' . $wpdb->posts
                . ' p on p.ID = n.post_id WHERE post_status = "publish"';
            $query      = self::_append_query($query);
            $results    = $wpdb->get_results($query);
        }

        if (!$post_type || $post_type == LF_CPT_COURSE) {
            $_POST['post_type'] = LF_CPT_COURSE;
            $query              = 'SELECT user_id, value, comment, date, course_id FROM ' . DB_NPS_USERS . ' WHERE 1';
            $query              = self::_append_query($query);
            $results_c          = $wpdb->get_results($query);
            $courses            = Lf_Utility_Course::get_all_courses();
            $courses            = array_combine(array_column($courses, 'ID'), $courses);
            foreach ($results_c as $result) {
                $course = $courses[$result->course_id] ?? null;
                if ($course) {
                    if ($title && !strpos(strtolower($course->title), strtolower($title)))
                        continue;
                    $result->post_title = $course->title;
                    $result->post_type = LF_CPT_COURSE;
                    $results[] = $result;
                }
            }
        }

        return $results;
    }
}
