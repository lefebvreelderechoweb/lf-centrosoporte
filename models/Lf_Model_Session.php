<?php

class Lf_Model_Session extends Lf_Model_Base
{
	public function __construct($session)
	{
		$this->ID					= $session->idSesion;
		$this->webex				= $session->idSesionWebex ?? '';
		$this->start_date			= $session->fechaComienzo ? strtotime($session->fechaComienzo) : '';
		$this->start_date_formatted	= $this->start_date ? date('d/m/Y - H:i', $this->start_date) : '';
		$this->end_date				= $session->fechaFin ? strtotime($session->fechaFin) : '';
		$this->end_date_formatted	= $this->end_date ? date('d/m/Y - H:i', $this->end_date) : '';
		$visibles					= $session->numPlazasVisibles ?? 0;
		$real						= $session->numPlazasReales ?? 0;		
		$places 					= $real < 30 ? $real : $visibles;
		$this->visible_places		= $places;
		$this->subscribed			= $session->indInscrito ?? false;
	}

	static function parse_sessions($sessions): array
	{
		$parsed_sessions = [];
		foreach ($sessions as $session) {
			$parsed_sessions[] = new Lf_Model_Session($session);
		}
		return $parsed_sessions;
	}
}
