<?php

class Lf_Model_Import{

	public function __construct() {

	}

	static function render_contents() {
		echo Lf_Shortcode_Import_Menu::call([]);
	}

	static function check_exist_by_post_meta_value($post_type, $meta_key, $meta_value) {

		global $wpdb;

		$post_id = null;

		if ($meta_value) {
			$sql = 'SELECT ID FROM wp_posts p';
			$sql .= ' LEFT JOIN wp_postmeta pm ON pm.post_id = p.ID';
			$sql .= ' WHERE p.post_type="' . $post_type . '" AND pm.meta_key="'.$meta_key.'" AND pm.meta_value="' . $meta_value.'"';
			$result = $wpdb->get_row($sql);
			$post_id = $result->ID ?? null;
		}

		return $post_id;
	}

	static function update_post_metas($post_id, $metas){
		foreach($metas as $meta_key => $meta_value){
			if($meta_value){
				update_post_meta($post_id, $meta_key, $meta_value);
			}
		}
	}

}
