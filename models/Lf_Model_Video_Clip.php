<?php

class Lf_Model_Video_Clip extends Lf_Model_Base {

	const POST_TYPE = LF_CPT_VIDEO;

	public function __construct() {
	}

	static function get_instance() {
		return new self();
	}

	function get($id, $args = []) {
		parent::get($id, $args);

		$this->post_type = self::POST_TYPE;
		$this->platform = get_field('plataforma', $id);
		$this->video_id = get_field('video_id', $id);
		$this->image_preview = get_field('imagen_miniatura', $id) ?? null;
		$this->description = get_field('descripcion', $id, false);
		
		$this->iframe = Lf_Model_Video::get_video($this->video_id, $this->platform);
		
		
		$model = new Lf_Model_Video($this->platform);
		$this->url = get_permalink($id);
		$this->url_iframe = $model->get_url_iframe($this->video_id);
		$this->preview = $model->get_preview(['video_id' => $this->video_id, 'size' => 'thumbnail_large', 'image_preview' => $this->image_preview]);
	}

}
