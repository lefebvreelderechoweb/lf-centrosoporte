<?php

class Lf_Model_Base
{
	public $ID;
	public $title;
	public $family;
	public $date;

	public const POST_TYPE_NAME = [
		LF_CPT_VIDEO => 'Videos',
		LF_CPT_MANUAL => 'Manuales',
		LF_CPT_QUESTION => 'Preguntas Frecuentes',
		LF_CPT_COURSE => 'Cursos'
	];

	function __construct()
	{;
	}

	function get(int $id, $args = [])
	{
		$this->ID = $id;
		$this->title = get_the_title($id);
		$this->family = get_field('familias', $id) ?? null;
		$this->date = get_the_date('Y-m-d H:i:s', $id);
	}

	static function has_voted_user_nps()
	{
		global $wpdb;
		$post_id	= get_the_ID();
		$course_id	= 0;
		$user_id	= get_current_user_id();
		if (!$post_id) {
			$url		= $_SERVER['REQUEST_URI'] ?? '';
			$url		= esc_url($url);
			$url		= explode('/', $url);
			$course_id	= (int) $url[count($url) - 1] ?? 0;
		}

		$query = 'SELECT post_id FROM ' . DB_NPS_USERS . ' WHERE user_id = ' . $user_id;
		if ($post_id)
			$query .= ' AND post_id = ' . $post_id;
		else
			$query .= ' AND course_id = ' . $course_id;
		$results = $wpdb->get_col($query);

		return !empty($results);
	}

	static function get_posts_list($type = [LF_CPT_VIDEO, LF_CPT_MANUAL, LF_CPT_QUESTION])
	{
		$posts = get_posts(array(
			'post_type' => $type,
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby' => 'title',
			'order' => 'ASC',
		));

		return $posts;
	}

	static function get_posts_list_group_by_type($type = [LF_CPT_VIDEO, LF_CPT_MANUAL, LF_CPT_QUESTION])
	{
		$posts = self::get_posts_list($type);
		$posts_by_type = [];
		foreach ($posts as $post) {
			$post_type = self::POST_TYPE_NAME[$post->post_type];
			if (!isset($posts_by_type[$post_type])) {
				$posts_by_type[$post_type] = [];
			}
			$posts_by_type[$post_type][] = $post;
		}

		return $posts_by_type;
	}
}
