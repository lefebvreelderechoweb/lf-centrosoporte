<?php

class Lf_Shortcode_Search extends Lf_Shortcode_Base
{
    public function register_assets($atts = [])
    {
        wp_enqueue_script('posts-list-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
    }

    public function register_hooks()
    {
        add_action('wp_ajax_show_more_posts', array($this, 'ajax_show_more_posts'));
        add_action('wp_ajax_nopriv_show_more_posts', array($this, 'ajax_show_more_posts'));
        add_action('wp_ajax_family_filter_posts', array($this, 'ajax_family_filter_posts'));
        add_action('wp_ajax_nopriv_family_filter_posts', array($this, 'ajax_family_filter_posts'));
        add_action('wp_ajax_contact_form_send', array($this, 'ajax_contact_form_send'));
        add_action('wp_ajax_nopriv_contact_form_send', array($this, 'ajax_contact_form_send'));
    }

    public function ajax_show_more_posts()
    {
        $post_name = $_POST['post_name'] ?? '';
        switch ($post_name) {
            case 'VÍDEOS':
                $model = new Lf_Shortcode_Video_List;
                $model->ajax_show_more_video();
                break;
            case 'MANUALES':
                $model = new Lf_Shortcode_Manual_List;
                $model->ajax_show_more_manual();
                break;
        }
    }

    public function ajax_family_filter_posts()
    {
        $post_name = $_POST['post_name'] ?? '';
        switch ($post_name) {
            case 'CURSOS':
                $model = new Lf_Shortcode_Course_List;
                $model->ajax_family_filter_course();
                break;
            case 'VÍDEOS':
                $model = new Lf_Shortcode_Video_List;
                $model->ajax_family_filter_video();
                break;
            case 'MANUALES':
                $model = new Lf_Shortcode_Manual_List;
                $model->ajax_family_filter_manual();
                break;
            case 'PREGUNTAS FRECUENTES':
                $model = new Lf_Shortcode_Faq_List;
                $model->ajax_family_filter_faq();
                break;
            case 'RESULTADOS':
                $_POST['product'] = $_POST['family'];
                $_POST['term'] = $_POST['search'];
                $_POST['type'] = $_POST['type'];
                $model = new Lf_Shortcode_Search_List;
                $model->ajax_order_change();
                break;
        }
    }

    public function ajax_contact_form_send()
    {
        $name           = $_POST['name'] ?? null;
        $email_user     = $_POST['email'] ?? null;
        $cause          = $_POST['cause'] ?? null;
        $observation    = $_POST['observation'] ?? null;

        $email          = MAIL_CONTACT;
        $from           = MAIL_FROM;
        $subject        = 'Centro de Ayuda | Formulario de Contacto';
        $body           = 'Prueba';
        $route          = LF_PLUGIN_DIR . '/templates/contact_form_email.html.php';
        ob_start();
        require         $route;
        $body           = Html::minify(ob_get_clean());
        $id             = Lf_WS_Automatic_Mail::send_automatic_mail($email, $from, $subject, $body, ID_MAIL_GROUP);

        echo json_encode([
            'success' => true
        ]);
        exit();
    }
}
