function ajax_show_more() {
  const page = $("#showMoreData").data("page");
  const family = $("#showMoreData").data("family");
  const showMore = $("#showMoreData").data("show-more");
  const loaded = $("#showMoreData").data("loaded");
  const postName = $("#showMoreData").data("post-name");

  $.ajax({
    url: lf.ajaxurl,
    type: "POST",
    data: {
      action: "show_more_posts",
      family: family,
      show_more: showMore,
      loaded: loaded,
      search,
      page: parseInt(page) + 1,
      post_name: postName,
    },
    beforeSend: function () {
      $("html").modalLoading("show");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      $("#listContainer").append(data.html);
      $("#showMoreData").data("page", data.page);
      $("#showMoreData").data("show-more", data.show_more);
      $("#showMoreData").data("loaded", data.loaded);

      if (!data.show_more) $("#showMorePosts").hide();
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      console.log(errorThrown);
    },
  });
}

$(document).ready(function () {
  /**
   * Create Subfamily by AJAX then update the tree
   */
  $(document).on("click", "#showMorePosts", function () {
    ajax_show_more();
  });
});
