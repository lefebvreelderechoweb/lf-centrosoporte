<?php

class Lf_Shortcode_Carousel extends Lf_Shortcode_Base
{
	const ITEMS = [LF_CPT_QUESTION, LF_CPT_VIDEO, LF_CPT_MANUAL, LF_CPT_COURSE];
	const POST_TYPE = ['PREGUNTAS FRECUENTES', 'VÍDEOS DE AYUDA', 'MANUALES', 'CURSOS'];

	/**
	 * Returns the position of the element in ITEMS
	 *
	 * @param int $item item to search
	 * @return int position of the item, -1 otherwise
	 */
	public static function get_item_position(int $item)
	{
		$found = array_search($item, self::ITEMS);

		if ($found === false)
			return -1;
		return $found;
	}

	public function handle($atts = [])
	{
		$lf_user = Lf_Login::get_lf_user();

		if (empty($lf_user) && !is_user_logged_in())
			return '';

		$args = [
			'orderby'   => 'rand',
			'limit'     => 1
		];

		$posts = [];
		foreach (self::ITEMS as $item) {
				
			if($item === LF_CPT_COURSE && Lf_Utility_Base::limited_user_access())
				continue;
			
			if ($item == LF_CPT_COURSE) {
				$user_courses = Lf_Utility_Course::get_courses_for_user();
				shuffle($user_courses);
				$post = $user_courses[0] ?? null;
			} else {
				$args['post_type'] = $item;
				$post = Lf_Utility_Base::get_list($args);
			}

			if ($post)
				$posts[] = $post;
		}


		return $this->render_view([
			'posts' => $posts
		]);
	}
}
