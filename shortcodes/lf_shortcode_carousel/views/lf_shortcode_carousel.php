<div id="carouselCAC" class="carousel slide carousel-cac" data-ride="carousel">
	<!--<div class="col-12"><h2><a href="#">NOVEDADES</a></h2></div>-->
	<div class="carousel-inner">

		<?php foreach ($posts as $key => $post) :
			$post_id = $post->ID;
			$post_title = $post->title;
			$url = $post->post_type == LF_CPT_COURSE ? get_home_url(null, 'cursos/' . $post_id) : get_permalink($post_id);

			$type = array_search($post->post_type ?? null, Lf_Shortcode_Carousel::ITEMS);
			$type_title = Lf_Shortcode_Carousel::POST_TYPE[$type];
		?>

			<div class="carousel-item <?= $key ? '' : 'active' ?>">
				<div class="container-fluid carousel-bg bg-0<?= $type + 1 ?>">
					<div class="container">
						<div class="row">
							<div class="offset-md-1 col-md-6 txt-carousel">
								<p><?= $type_title ?></p>
								<p>
									<a href="<?= esc_url($url) ?>">
										<?= $post_title ?>
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endforeach; ?>

	</div>
	<a class="carousel-control-prev" href="#carouselCAC" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Anterior</span>
	</a>
	<a class="carousel-control-next" href="#carouselCAC" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Siguiente</span>
	</a>
</div>
<!-- TODO: Pintar un curso aleatorio tambien -->
