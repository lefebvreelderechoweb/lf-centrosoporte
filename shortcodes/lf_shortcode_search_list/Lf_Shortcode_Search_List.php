<?php

class Lf_Shortcode_Search_List extends Lf_Shortcode_Base
{

    public function register_assets($atts = [])
    {
        wp_enqueue_script('search-result-list-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
    }

    public function register_hooks()
    {
        add_action('wp_ajax_order_change', array($this, 'ajax_order_change'));
        add_action('wp_ajax_nopriv_order_change', array($this, 'ajax_order_change'));
    }

    public function ajax_order_change()
    {
        $order        = $_POST['order'] ?? null;
        $items        = $this->get_items($order);

        ob_start();
        $this->print_group($items);
        $html = ob_get_contents();
        ob_end_clean();

        echo json_encode([
            'success'    => true,
            'html'        => $html
        ]);
        exit();
    }

    public function get_items($order = null)
    {
        $family_id    = $_GET['product'] ?? $_POST['product'] ?? null;
        $q            = $_GET['term'] ?? $_POST['term'] ?? '';
        $post_type    = $_POST['type'] ?? null;
        $classes    = Lf_Model_Sidebar::CLASSES;
        $items        = [];
        $limit        = 4;

        if ($family_id)
            $family_id = json_decode($family_id) ?? explode(', ', $family_id);

        $args = [
            'q' => $q
        ];

        if (!empty($family_id))
            $args['family_id'] = $family_id;

        if (empty($q) && empty($family_id))
            return;

        if (!empty($post_type)) {
            $args['post_type'] = $post_type;
            if ($post_type == LF_CPT_COURSE) {
                if (is_array($family_id)) {
                    $families = [];
                    foreach ($family_id as $family) {
                        $families[] = get_field('code', $family);
                    }
                } else {
                    $families = get_field('code', $family_id);
                }
                $items = Lf_Utility_Course::get_courses_for_user(['search' => $q, 'families_ids' => $families]);
            } else
                $items = Lf_Utility_Base::get_list($args);
        } else {
            foreach ($classes as $key => $value) {
                if ($key == LF_CPT_COURSE)
                    continue;
                $args['post_type'] = $key;
                if ($order == 'group')
                    $args['limit'] = $limit;
                $items = array_merge($items, Lf_Utility_Base::get_list($args));
            }

            if (is_array($family_id)) {
                $families = [];
                foreach ($family_id as $family) {
                    $families[] = get_field('code', $family);
                }
            } else {
                $families = get_field('code', $family_id);
            }
            $items = array_merge($items, Lf_Utility_Course::get_courses_for_user(['search' => $q, 'families_ids' => $families]));
        }

        switch ($order) {
            case 'old':
                Lf_Utility_Search::order_recent_reverse($items);
                break;
            case 'alfa':
                Lf_Utility_Search::order_alfa($items);
                break;
            case 'group':
                break;
            default:
                Lf_Utility_Search::order_recent($items);
                break;
        }

        return $items;
    }

    public function print_group($items)
    {
        $type_name  = Lf_Model_Sidebar::PAGES;
        $sub_items  = [];
        $type       = $_POST['type'] ?? null;
        $limit      = $type ? 0 : 3;

        if (!$items) {
?>
            <div class="row">
                <p class="msg-danger">No se encuentran resultados</p>
            </div>
            <?php
            return;
        }

        foreach ($type_name as $value) {
            $sub_items[$value] = [];
        }

        foreach ($items as $key => $item) {
            $sub_items[$item->post_type][] = $item;
        }


        foreach ($sub_items as $key => $item) {

            if (!empty($item)) {
            ?>
                <section class="col-12 results mb-5">
                    <h2 class="heading-bar"><?= array_search($key, $type_name) ?></h2>
                    <?= $this->print_list($item, $limit) ?>
                </section>
            <?php
            }
        }
    }

    public function print_list($items, $limit = 0)
    {
        $classes        = Lf_Model_Sidebar::CLASSES;
        $user_families    = Lf_Utility_Base::get_families_id_by_user();

        foreach ($items as $key => $item) {
            if ($limit && $key == $limit) {
            ?>
                <div class="row mt-4">
                    <div class="col-12 text-center">
                        <button type="button" class="btn-borderless show-more-results" data-type="<?= $item->post_type ?>">VER MÁS <span class="lf-icon-arrow-round-down"></span></button>
                    </div>
                </div>
            <?php
                break;
            } else {
                $target = "";
                if ($item->post_type == LF_CPT_MANUAL) {
                    $versions = Lf_Model_Manual::get_versions($item->ID);
                    $link = $versions['actual'];
                    $old = $versions['anterior'] ?? null;
                    $date = $versions['limite'] ?? null;
                    $target = "_blank";
                } else
                    $link = $item->post_type == LF_CPT_COURSE ? get_home_url(null, 'cursos/' . $item->ID) : get_permalink($item->ID);

                if ($item->post_type == LF_CPT_QUESTION)
                    $item->main_family = array_unique($item->main_family);

                $families = $item->main_family ?? $item->family ?? $item->get_wp_families_ids();
                if (!current_user_can('edit_posts'))
                    $families = Lf_Utility_Base::check_valid_family_to_search($families, $user_families);
            ?>

                <article class="row results-item no-gutters" data-key="<?= $key ?>">
                    <div class="col-2 col-xl-1 d-flex align-items-center">
                        <span class="icon-bg-round left-flat"><span class="<?= $classes[$item->post_type] ?>"></span></span>
                    </div>
                    <div class="col-10 col-xl-11">

                        <?php if ($families) foreach ($families as $family) { ?>
                            <span class="area-label small"><?= strtoupper($family->post_title ?? get_the_title($family)) ?></span>
                        <?php } ?>

                        <h3><a href="<?= $link ?>" class="stretched-link" target="<?= $target ?>" title="<?= $item->title ?>"><?= $item->title ?></a></h3>
                    </div>

                    <?php if ($item->post_type == LF_CPT_MANUAL && $old) { ?>
                        <p class="offset-2 offset-xl-1 col-10 col-xl-11">
                            <button class="link-text manual-modal" data-toggle="modal" data-target="#fechaCaducidad" target="_blank" title="El manual se abrirá en ventana nueva" data-fecha="<?= $date ?>" data-url="<?= $old ?>">
                                <span class="lf-icon-arrow-round-left"></span> Versión anterior
                            </button>
                        </p>
                    <?php } else { ?>
                        <p class="offset-2 offset-xl-1 col-10 col-xl-11"><?= $item->description ?? '' ?></p>
                    <?php } ?>

                </article>

<?php
            }
        }
    }

    public function handle($atts = [])
    {
        $items        = $this->get_items();

        return $this->render_view([
            'items'        => $items
        ]);
    }
}
