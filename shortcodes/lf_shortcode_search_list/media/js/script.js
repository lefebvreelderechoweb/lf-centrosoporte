const limit = 15;
let elements;
let size;
let pages;
let currentPage = 1;

function ajax_order_change() {
  const order = $("#orderType").val();
  const type = $("#contentType").val();
  const params = new URLSearchParams(window.location.search);
  const term = params.get("term");
  const product = params.get("product");

  $.ajax({
    url: lf.ajaxurl,
    type: "POST",
    data: {
      action: "order_change",
      order,
      type,
      term,
      product,
    },
    beforeSend: function () {
      $("html").modalLoading("show");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) {
        $("#resultsSection").html(data.html);
        prepare_pagination();
        pagination();
      } else alert("Algo ha fallado");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      console.log(errorThrown);
    },
  });
}

const prepare_pagination = () => {
  currentPage = 1;
  const pag = $(".pagination");

  pag.html(
    `<li class="page-item">
        <a
          class="page-link d-flex align-items-center previous"
          href="#"
          tabindex="-1"
          title="Anterior"
        >
          <span class="lf-icon-angle-left"></span>
        </a>
      </li>
      <li class="page-item page-number-link active" data-page="1">
        <a class="page-link" href="#">
          1
        </a>
      </li>`
  );

  for (let index = 2; index <= pages; index++) {
    pag.append(
      `<li class="page-item page-number-link" data-page="${index}">
          <a class="page-link" href="#">
            ${index}
          </a>
        </li>`
    );
  }
  if (pages > 1) {
    pag.append(
      `<li class="page-item">
          <a
            class="page-link d-flex align-items-center next"
            href="#"
            title="Siguiente"
          >
            <span class="lf-icon-angle-right"></span>
          </a>
        </li>`
    );
  }
};

const pagination = () => {
  const order = $("#orderType").val();

  if (size <= limit || order === "group") $("#pagination").hide();
  else {
    change_content();
    $("#totalPages").html(pages);
    $("#totalElements").html(size);

    if (currentPage == 1)
      $(".previous").attr("style", "display:none !important");
    else if (currentPage != 1) $(".previous").show();
    if (currentPage == pages)
      $(".next").attr("style", "display:none !important");
    else if (currentPage != pages) $(".next").show();
  }
};

const change_page = () => {
  pagination();
  $(".page-number-link.active").removeClass("active");
  $(`.page-number-link[data-page="${currentPage}"]`).addClass("active");
  $("#currentPage").html(currentPage);
};

const change_content = () => {
  const lower = (currentPage - 1) * limit;
  const higher = currentPage * limit - 1;
  elements.map(function () {
    if ($(this).data("key") < lower || $(this).data("key") > higher)
      $(this).hide();
    else $(this).show();
  });
};

$(document).ready(function () {
  $("#sidebar-search").hide();

  // Prepare the list and the pagination
  elements = $(".results-item");
  size = elements.length;
  pages = Math.ceil(size / limit);

  prepare_pagination();
  pagination();

  // On click a page
  $(document).on("click", ".page-number-link", function () {
    currentPage = $(this).data("page");
    change_page();
  });

  // On click a page
  $(document).on("click", ".next", function () {
    currentPage++;
    change_page();
  });
  // On click a page
  $(document).on("click", ".previous", function () {
    currentPage--;
    change_page();
  });

  $(".results-select-type").change(function () {
    if ($(".results-select-order").val() === "group")
      $(".results-select-order").val("");
    ajax_order_change();
  });
  $(".results-select-order").change(function () {
    if ($(this).val() === "group" && $(".results-select-type").val() !== "")
      $(".results-select-type").val("");
    ajax_order_change();
  });
  $(document).on("click", ".show-more-results", function () {
    const type = $(this).data("type");
    $("#contentType").val(type);
    $("#orderType").val("");
    ajax_order_change();
  });

  $(".filter-family").click(function () {
    const params = new URLSearchParams(window.location.search);
    const term = params.get("term");
    search = term;
  });

  $(document).on("click", ".manual-modal", function () {
    const fecha = $(this).data("fecha");
    $("#manual-date").html(fecha);
    $("#anterior-url").data("url", $(this).data("url"));
  });

  $(document).on("click", "#modal-accept", function () {
    const url = $("#anterior-url").data("url");
    window.open(url, "_blank");
  });
});
