<?php

class Lf_Shortcode_Course_Detail extends Lf_Shortcode_List_Base
{
	public function register_assets($atts = [])
	{
		wp_enqueue_script('course-detail-js', plugin_dir_url(__FILE__) . 'media/js/course_detail.js', array('jquery'));
	}

	public function register_hooks()
	{
		add_action('wp_ajax_subscribe_to_course', array($this, 'ajax_subscribe_to_course'));
		add_action('wp_ajax_nopriv_subscribe_to_course', array($this, 'ajax_subscribe_to_course'));
	}

	/**
	 * {
	 *	"idSesionWebex": 0,
	 *	"idClienteNav": 0,
	 *	"idContactoCrm": 0,
	 *	"email": "string",
	 *	"nombre": "string",
	 *	"apellido": "string",
	 *	"telefono": "string",
	 *	"idOrigen": 0,
	 *	"idUsuarioApp": 0
	 *	}
	 */

	public function ajax_subscribe_to_course()
	{
		$session_webex		= $_POST['session_webex'] ?? null;
		$name				= $_POST['name'] ?? null;
		$surname			= $_POST['surname'] ?? null;
		$email				= $_POST['email'] ?? null;
		$phone				= $_POST['phone'] ?? null;

		$lf_user			= Lf_Login::get_lf_user();
		$id_nav				= $lf_user->getNavisionClientId();
		$id_user			= $lf_user->get_ClientId();
		$id_origen			= 13;

		$status				= Lf_Api_Rest_Formation_Product::subscribe_to_course([
			'idSesionWebex'	=> $session_webex,
			'idClienteNav'	=> $id_nav,
			'idContactoCrm'	=> $id_user,
			'email'			=> $email,
			'nombre'		=> $name,
			'apellido'		=> $surname,
			'telefono'		=> $phone,
			'idOrigen'		=> $id_origen,
			// 'idUsuarioApp'	=> $id_user_app
		]);

		echo json_encode([
			'success'		=> true,
			'status'		=> $status
		]);
		exit();
	}

	public function handle($atts = [])
	{
		$url		= $_SERVER['REQUEST_URI'] ?? '';
		$url		= esc_url($url);
		$url		= explode('/', $url);
		$course_id	= (int) $url[count($url) - 1] ?? 0;
		$course		= Lf_Utility_Course::get_course_by_id($course_id);
		$sessions = self::sortSessions($course);
		$lf_user	= Lf_Login::get_lf_user();
		$has_access = $course ? Lf_Utility_Base::has_access($course->get_families_ids()) : false;

		if ($course_id && !is_user_logged_in()) {
			$url		= $_SERVER['REQUEST_URI'] ?? '';
			$url		= esc_url($url);
			$data_token = base64_encode(get_site_url() . $url);
			$url_redirect = get_site_url() . '/login/?redirect_to=' . get_site_url() . '?' . LF_DATA_REDIRECT_TOKEN . '=' . $data_token;
			wp_redirect($url_redirect);
			die;
		}

		if (!$course || !$has_access) {
			wp_redirect(home_url('no-autorizado'));
			die;
		}

		if (isset($_GET['video'])) {
			$video		= $course->video;
			$video_type	= strtolower($video->descTipoVideo ?? '');
			$video_id	= $video->idVideoExterno ?? '';
			if ($video)
				$iframe	= Lf_Model_Video::get_video($video_id, $video_type);
			else
				$iframe	= null;

			if ($iframe) {
				$register_view = Lf_Api_Rest_Formation_Product::register_video_preview([
					'IdVideo'		=> $video->idVideo,
					'IdUsuarioPro'	=> $lf_user->get_entry_id(),
					'idUsuarioApp'	=> 1
				]);
			}
		} else
			$courses	= Lf_Utility_Course::get_courses_for_user();

		echo $this->render_view([
			'course'	=> $course,
			'sessions'	=> $sessions,
			'iframe'	=> $iframe ?? null,
			'courses'	=> $courses ?? null
		]);
	}

	// Ordenar convocatorias por fecha ASC
	public static function sortSessions($course)
	{
		if ($course->sessions) {
			usort($course->sessions, function ($a, $b) {
				return $a->start_date - $b->start_date;
			});
			return $course->sessions;
		}
		return null;
	}
}
