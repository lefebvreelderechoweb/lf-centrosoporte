$ = jQuery;

$(document).ready(function () {
  $('.nav-link:contains("CURSOS")').parent().addClass('active');
  $('#availablePlaces').hide();
  $('#unavailablePlaces').hide();

  const hasVideo = $('#courseVideoBtn').data('has-video');
  if (hasVideo == '1') {
    $('#courseVideoBtn').removeClass('disabled');
    $('#courseVideoBtn').addClass('modal-loading');
    $('#courseVideoBtn').prop('href', '?video=1');
  }

  $('#selectFecha').on('change', function (event) {
    const selected = $('#selectFecha option:selected');
    const webex = $(selected).val();
    const availablePlaces = $(selected).data('available-places');
    const subscribed = $(selected).data('subscribed');
    $('#subscribeBtn').text('Inscribirme');

    if (webex == 0) {
      $('#availablePlaces').hide();
      $('#unavailablePlaces').hide();
      $('#subscribeBtn').prop('disabled', true);
    } else {
      if (availablePlaces > 0 && !subscribed) {
        $('#availablePlaces').show();
        $('#availablePlaces').text(
          `Quedan ${availablePlaces} plazas disponibles`
        );
        $('#unavailablePlaces').hide();
        $('#subscribeBtn').prop('disabled', false);
      } else {
        $('#availablePlaces').hide();
        $('#unavailablePlaces').hide();
        if (subscribed) {
          $('#subscribeBtn').text('Ya estás inscrito');
        } else {
          $('#subscribeBtn').text('Inscribirme');
          $('#unavailablePlaces').show();
        }
        $('#subscribeBtn').prop('disabled', true);
      }
      $('#webexInput').val(webex);
    }

    event.preventDefault();
  });

  $('#tlfInput').on('change', function (event) {
    const telf = document.getElementById('tlfInput');
    const constraint = new RegExp('[0-9]{9}', '');
    if (constraint.test(telf.value)) {
      telf.setCustomValidity('');
    } else {
      telf.setCustomValidity('Por favor, escribe correctamente el teléfono');
    }
  });

  $('#inscriptionCourseForm').on('submit', function (event) {
    $('#inscrCurso').modal('hide');

    // Get all values
    const name = $('#nameInput').val();
    const surname = $('#apellidosInput').val();
    const phone = $('#tlfInput').val();
    const email = $('#mailInput').val();
    const session_webex = $('#webexInput').val();

    $.ajax({
      url: lf.ajaxurl,
      type: 'POST',
      data: {
        action: 'subscribe_to_course',
        name,
        surname,
        phone,
        email,
        session_webex,
      },
      beforeSend: function () {
        $('html').modalLoading('show');
      },
      success: function (response) {
        response = JSON.parse(response);
        if (response?.status?.iResultado >= 0) {
          $('#inscrOk').modal('show');
          const selected = $('#selectFecha option:selected');
          const availablePlaces = $(selected).data('available-places');
          $(selected).data('available-places', availablePlaces - 1);
          $(selected).data('subscribed', 1);
          $('#selectFecha').trigger('change');
        } else {
          $('#inscrErrorMsg').text(response?.status?.sDescripcion);
          $('#inscrError').modal('show');
        }
        $('html').modalLoading('hide');
      },
      error: function (error) {
        $('html').modalLoading('hide');
      },
    });

    event.preventDefault();
  });
});
