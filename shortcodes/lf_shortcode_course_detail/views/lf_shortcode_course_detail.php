<?php

get_header(null, ['title' => $course->title]);

?>

<div class="container">
	<div class="row">
		<div class="col-4 col-lg-3">
			<?php get_sidebar(null, ['courses' => $courses]); ?>
		</div>
		<?php 
			$mixpanel_title = $course->title ?? '';
			$mixpanel_family = ($course->families) ? $course->families[0]->descFamilia : '' ;
		?>

		<div class="col-8 col-lg-9">
			<div class="row pb-3 mb-4">
				<!-- Esto es el h1 en la home de cursos -->
				<header class="section-title col-12">CURSOS</header>
				<div class="col-12">
					<!-- En este caso el título de la página (h1) es el del vídeo -->
					<h1 class="section-claim">
						<?php if ($iframe) : ?>
							<a href="<?= get_home_url(null, 'cursos/' . $course->ID) ?>" class="modal-loading" title="Volver al detalle del curso">
							<?php endif ?>
							<?= $course->title ?>
							<?php if ($iframe) : ?>
							</a>
						<?php endif ?>
					</h1>
					<p class="section-subclaim"><?= $course->resumen ?></p>
				</div>
				<?php if ($iframe) : ?>
					<div class="embed-responsive embed-responsive-16by9 col-12 offset-lg-1 col-lg-10">
						<?= $iframe ?>
					</div>
				<?php else : ?>
					<div class="col-7 col-lg-8">
						<div class="row">
							<div class="col-12 mb-3">
								<img src="<?= esc_url($course->image ?? '') ?>" alt="" class="img-fluid">
							</div>
							<div class="col-12 pt-2">
								<ul class="nav nav-tabs tabs d-flex justify-content-between mixpanel-course-tab" id="courseTabs" role="tablist"
								data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family?>">
									<li class="nav-item"><a class="nav-link active" id="recommendedTab" data-toggle="tab" href="#recommended" role="tab" aria-controls="recommended" aria-selected="true">RECOMENDADO</a></li>
									<li class="nav-item"><a class="nav-link" id="programmeTab" data-toggle="tab" href="#programme" role="tab" aria-controls="programme" aria-selected="false">PROGRAMA</a></li>
									<li class="nav-item"><a class="nav-link" id="requirementsTab" data-toggle="tab" href="#requirements" role="tab" aria-controls="requirements" aria-selected="false">REQUISITOS</a></li>
								</ul>
							</div>
							<div class="tab-content row w-100 m-0" id="courseTabsContent">
								<div class="col-12 tab-pane fade show active pt-3" id="recommended" role="tabpanel" aria-labelledby="recommendedTab">
									<p><?= $course->recommend ?></p>
								</div>
								<div class="col-12 tab-pane fade pt-3" id="programme" role="tabpanel" aria-labelledby="programmeTab">
									<p><?= $course->program ?></p>
								</div>
								<div class="col-12 tab-pane fade pt-3" id="requirements" role="tabpanel" aria-labelledby="requirementsTab">
									<p><?= $course->requirements ?></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-5 col-lg-4">
						<ul class="course-features mb-3">
							<li><span class="lf-icon-on"></span>Online</li>
							<li><span class="lf-icon-clock"></span><?= $course->duration ?> min</li>
							<li><span class="lf-icon-coin"></span>Gratuito</li>
							<li><span class="lf-icon-legal-reference"></span>Nivel <?= $course->level ?></li>
							<li><span class="lf-icon-user"></span>Para usuarios de <?= implode(', ', $course->get_families_names()) ?></li>
							<?php if ($sessions) : ?>
								<li>
									<span class="lf-icon-calendar"></span>
									<label for="selectFecha" class="sr-only">Consulta fechas</label>
									<select class="custom-select features mb-3 mixpanel-course-date" id="selectFecha"
									data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family?>">
										<option value="0">Consulta fechas</option>
										<?php
										foreach ($sessions as $session) :
											$visible_places = $session->visible_places;
											/*if (in_array(getenv('ENVIRONMENT'), ['DEV', 'PRE']))
												$visible_places = $course->ID == 28 ? $session->visible_places : 0;
											else
												$visible_places = $session->visible_places;*/
										?>
											<option value="<?= $session->webex ?>" data-available-places="<?= $visible_places ?>" data-subscribed="<?= $session->subscribed ?>"><?= $session->start_date_formatted ?></option>
										<?php endforeach; ?>
									</select>
									<p id="availablePlaces" class="help-text ml-4 pl-3"></p>
									<p id="unavailablePlaces" class="help-text txt-danger ml-4 pl-3">No quedan plazas</p>
								</li>
							<?php endif; ?>
						</ul>
						<a href="<?= get_home_url(null, 'instrucciones-de-acceso') ?>" class="help-text mb-4" target="_blank"><span class="lf-icon-document"></span> Instrucciones de acceso</a>
						<div class="text-right">
							<?php if ($sessions) : ?>
								<button id="subscribeBtn" type="button" disabled="" class="btn btn-secondary btn-small mixpanel-course-suscribe" 
								data-toggle="modal" data-target="#inscrCurso" data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family?>">Inscribirme</button>
							<?php endif; ?>

							<?php if ($course->video) : ?>
								<a id="courseVideoBtn" class="link-text d-block mt-4 disabled" data-has-video="<?= $course->video ? true : false ?>" href="#0"><span class="lf-icon-arrow-round-left mr-1"></span><?= __('VER GRABACIÓN CURSO')?></a>
							<?php endif; ?>

						</div>
					</div>
				<?php endif ?>
			</div>

			<?php if ($iframe) : ?>
				<div class="row mt-5">
					<?= Lf_Shortcode_Rate_Content::call() ?>
				</div>
			<?php endif ?>
		</div>

	</div>
</div>

<div class="modal fade" id="inscrCurso" tabindex="-1" role="alertdialog" aria-labelledby="inscrCursoLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header d-flex align-items-center no-gutters">
				<div class="col-6 d-flex">
					<img src="https://assets.lefebvre.es/media/logos/web/comunes/lefebvre-100x20.png" alt="Lefebvre">
				</div>
				<div class="col d-flex align-items-center justify-content-end">
					<h2 id="inscrCursoLabel" class="title">Curso de Formación OnLine</h2>
					<button type="button" data-dismiss="modal" aria-label="Cerrar" class="close p-0 m-0 ml-2">
						<span aria-hidden="true" class="lf-icon-close-round"></span>
					</button>
				</div>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-12 bb3 pt-3 mb-3"></div>
						<div class="col-12 mb-3">
							<h3><?= $course->title ?></h3>
							<p><?= $course->subscribe_modal ?></p>
						</div>
					</div>
					<form id="inscriptionCourseForm">
						<div class="row inscripcion mb-3">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="nameInput" class="m-0">Nombre del asistente</label>
									<input type="text" class="form-control" id="nameInput" name="name" required>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="apellidosInput" class="m-0">Apellidos del asistente</label>
									<input type="text" class="form-control" id="apellidosInput" name="surname" required>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="tlfInput" class="m-0">Teléfono de contacto</label>
									<input type="tel" pattern="[0-9]{9}" class="form-control" id="tlfInput" name="phone" required>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="mailInput" class="m-0">Correo electrónico</label>
									<input type="email" class="form-control" id="mailInput" name="email" required>
								</div>
							</div>

							<!-- MENSAJE DE ERROR -->
							<p id="courseInscrValidate" class="msg-danger text-center my-3" style="display: none;"><span class="lf-icon-exclamation mr-2"></span>Debes rellenar todos los campos</p>
							<!-- FIN mensaje de error -->

						</div>
						<div class="row">
							<div class="col-12 mb-3">
								<div class="form-group form-check p-0">
									<input type="checkbox" class="form-check-input" id="inscrTerms" required>
									<label class="form-check-label" for="inscrTerms">He leído y acepto las condiciones de la <a href="https://lefebvre.es/politica-privacidad/" target="_blank" title="Enlace externo. Abre en ventana nueva">Política de privacidad</a> y el <a href="https://lefebvre.es/aviso-legal/" target="_blank" title="Enlace externo. Abre en ventana nueva">Aviso legal</a></label>
								</div>
                                <label for="webexInput" class="sr-only">Sesión Webex</label>
								<input id="webexInput" name="session_webex" hidden>
							</div>
						</div>
						<div class="row align-items-end">
							<div class="col text-right">
								<button type="submit" class="btn btn-primary mb-3">Confirmar inscripción</button>
								<p><a href="#" class="back" data-dismiss="modal" data-toggle="modal" data-target="#infoCurso">Quizá más tarde</a></p>

							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de la modal de inscripción al curso -->

<!-- Modal confirmación inscripción OK -->
<div class="modal modal-message fade" id="inscrOk" tabindex="-1" role="alertdialog" aria-modal="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" title="Cerrar"><span aria-hidden="true" class="lf-icon-close-round"></span></button>
				<span class="mx-auto lf-icon-check-round"></span>
			</div>
			<div class="modal-body">
				<p>Inscripción realizada con éxito</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">ACEPTAR</button>
			</div>
		</div>
	</div>
</div>
<!-- FIN modal confirmación inscripción OK -->
<!-- Modal confirmación inscripción ERROR -->
<div class="modal modal-message fade" id="inscrError" tabindex="-1" role="alertdialog" aria-modal="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" title="Cerrar"><span aria-hidden="true" class="lf-icon-close-round"></span></button>
				<span class="mx-auto lf-icon-information"></span>
			</div>
			<div class="modal-body">
				<p id="inscrErrorMsg"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">ACEPTAR</button>
			</div>
		</div>
	</div>
</div>
<!-- FIN modal confirmación inscripción ERROR -->

<?php
get_footer();
