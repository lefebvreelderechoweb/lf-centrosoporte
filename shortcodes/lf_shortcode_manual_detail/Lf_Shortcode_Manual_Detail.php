<?php

class Lf_Shortcode_Manual_Detail extends Lf_Shortcode_List_Base {
	public function handle( $atts = [] ) {
		$lf_user = Lf_Login::get_lf_user();
		$manual = new Lf_Model_Manual();
		$manual->get( get_the_ID() );
		$has_access = false;

		if ($manual) {
			$families_ids = array_column($manual->family, 'ID');
			$video_families = [];
			foreach ($families_ids as $family_id) {
				$video_families[] = get_field('codigo', $family_id);
			}
			$has_access = Lf_Utility_Base::has_access($video_families);
		}


		if ( !is_user_logged_in() && ( empty( $lf_user ) || !$has_access ) )
			return '';

		$versions = Lf_Model_Manual::get_versions( get_the_ID());
        $visor_url = $versions['actual'] ?? '';

		return $this->render_view([
			'manual' => $manual,
			'visorUrl' => $visor_url
		]);
	}
}
