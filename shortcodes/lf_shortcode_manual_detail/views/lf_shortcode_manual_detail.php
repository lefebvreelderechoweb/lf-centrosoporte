<div class="col-12">
    <!-- En este caso el título de la página (h1) es el del vídeo -->
    <h1 class="section-claim"><?= $manual->title ?? '' ?></h1>
</div>
<?php if('' != $visorUrl){ ?>
<div class="col-12 text-right">
    <a href="<?= $visorUrl; ?>" class="btn btn-secondary btn-small" target="_blank">VER PDF</a>
</div>
<?php } ?>
<div class="embed-responsive  col-12 offset-lg-1 col-lg-10 mt-4">
    <?= $manual->content ?? '' ?>
</div>
