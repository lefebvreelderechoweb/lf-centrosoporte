<div id="list-container" class="col-8 col-lg-9">
   <?= Lf_Shortcode_Manual_List::print_view($manuals, $show_more, $family) ?>
</div>

<!-- Modal caducidad versión anterior -->
<div class="modal modal-message fade" id="fechaCaducidad" tabindex="-1" role="alertdialog" aria-modal="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar" title="Cerrar"><span aria-hidden="true" class="lf-icon-close-round"></span></button>
            <span class="mx-auto lf-icon-information"></span>
         </div>
         <div class="modal-body">
            <p>La versión anterior caducará el <span id="manual-date"></span>.<br>Asegúrate de guardar las anotaciones que has realizado en la versión online.</p>
         </div>
          <label for="anterior-url" class="sr-only">Anterior URL</label>
         <input hidden id="anterior-url">
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="modal-accept" data-dismiss="modal">ACEPTAR</button>
         </div>
      </div>
   </div>
</div>
<!-- FIN modal caducidad versión anterior -->
