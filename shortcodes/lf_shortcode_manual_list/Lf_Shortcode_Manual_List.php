<?php

class Lf_Shortcode_Manual_List extends Lf_Shortcode_List_Base
{
	private $manuals = [];
	private $total;

	public function register_assets($atts = [])
	{
		wp_enqueue_script('manual-modal-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
	}

	public function ajax_show_more_manual()
	{
		$family		= $_POST['family'] ?? null;
		$page		= $_POST['page'] ?? 0;
		$show_more	= $_POST['show_more'] ?? false;
		$loaded		= $_POST['loaded'] ?? 0;
		$search		= $_POST['search'] ?? 0;
		$html		= '';

		// Prepare HTML of the Tree
		if ($show_more) {
			$this->get_manuals($family, $page, $search);
			$loaded += sizeof($this->manuals);

			if ($this->total <= $loaded)
				$show_more = false;

			ob_start();
			self::print_manuals_list($this->manuals);
			$html = ob_get_contents();
			ob_end_clean();
		}

		echo json_encode([
			'page'				=> $page,
			'show_more'			=> $show_more,
			'loaded'			=> $loaded,
			'html'				=> $html
		]);
		exit();
	}

	public function ajax_family_filter_manual()
	{
		$family	= $_POST['family'] ?? null;
		$search	= $_POST['search'] ?? null;
		$show_more	= $this->get($family, $search);

		ob_start();
		self::print_view($this->manuals, $show_more, $family);
		$html = ob_get_contents();
		ob_end_clean();

		echo json_encode([
			'html' => $html
		]);
		exit();
	}

	function get($family, $search)
	{
		$families	= Lf_Utility_Base::get_families_id_by_user();
		$page		= $atts['page'] ?? 0;
		$show_more	= false;

		if (count($families) || current_user_can('edit_posts')) {
			$this->get_manuals($family, $page, $search);

			if ($this->total > sizeof($this->manuals))
				$show_more = true;
		}

		return $show_more;
	}

	private function get_manuals($family, int $page, $search)
	{
		$args = [
			'post_type'	=> LF_CPT_MANUAL,
			'limit'		=> 8,
			'orderby'	=> 'post_title',
			'page'		=> $page
		];

		if ($family)
			$args['family_id'] = $family;

		if ($search)
			$args['q'] = $search;

		$this->manuals = Lf_Utility_Base::get_list($args);

		$this->total = Lf_Utility_Base::get_total($args);
	}

	static function print_manuals_list(array $manuals)
	{
		foreach ($manuals as $manual) {
			$versions = Lf_Model_Manual::get_versions($manual->ID);
?>
			<figure class="col-12 col-md-6 col-lg-3 mb-5">
				<a class="modal-loading item course mb-2 p-0" href="<?= get_permalink($manual->ID) ?>" title="El manual se abrirá en ventana nueva">
					<?= wp_get_attachment_image($versions['imagen'], array('840', '600'), "", array("class" => "img-fluid", "alt" => esc_attr($manual->title))); ?>
				</a>
				<?php if (isset($versions['anterior'])) { ?>
					<a class="link-text manual-modal" data-toggle="modal" href="#fechaCaducidad" target="_blank" title="El manual se abrirá en ventana nueva" data-fecha="<?= $versions['limite'] ?>" data-url="<?= esc_url($versions['anterior']) ?>">
						<span class="lf-icon-arrow-round-left"></span> Versión anterior
					</a>
				<?php } ?>
			</figure>
		<?php }
	}

	static function print_view(array $manuals, $show_more, $family)
	{
		?>
		<div class="row mb-4">
			<h1 class="section-title col-12">MANUALES</h1>
		</div>
		<?php if (!sizeof($manuals)) : ?>
			<div id="listContainer" class="row">
				<p class="msg-danger">No se encuentran resultados</p>
			</div>
		<?php else : ?>
			<div id="listContainer" class="row">
				<?= self::print_manuals_list($manuals) ?>
			</div>

			<input id="showMoreData" hidden data-page="0" data-show-more="<?= $show_more ?>" data-family="<?= $family ?>" data-loaded="<?= sizeof($manuals) ?>" data-post-name="MANUALES" />
			<?php if ($show_more) : ?>
				<div class="row mb-5">
					<div class="col-12 text-center">
						<button id="showMorePosts" class="btn-borderless mixpanel-show-more">VER MÁS <span class="lf-icon-arrow-round-down"></span></button>
					</div>
				</div>
<?php
			endif;
		endif;
	}

	public function handle($atts = [])
	{
		$family		= $atts['family'] ?? null;
		$search		= $atts['search'] ?? null;

		$show_more = $this->get($family, $search);

		return $this->render_view([
			'manuals'	=> $this->manuals,
			'family'	=> $family,
			'show_more'	=> $show_more
		]);
	}
}
