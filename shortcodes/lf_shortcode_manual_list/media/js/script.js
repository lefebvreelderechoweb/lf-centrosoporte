$(document).ready(function () {
  $(document).on("click", ".manual-modal", function () {
    const fecha = $(this).data("fecha");
    $("#manual-date").html(fecha);
    $("#anterior-url").data("url", $(this).data("url"));
  });

  $(document).on("click", "#modal-accept", function () {
    const url = $("#anterior-url").data("url");
    window.open(url, "_blank");
  });
});
