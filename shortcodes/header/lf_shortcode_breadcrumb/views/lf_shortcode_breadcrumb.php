<!-- Miga de pan (ocultar en home) -->
<?php if (!is_home()) { ?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/" class="modal-loading mixpanel-breadcrumb">CENTRO DE AYUDA</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <a href="<?= $url ?>" class="modal-loading mixpanel-breadcrumb">
                    <?= esc_html($post_name) ?>
                </a>
            </li>
        </ol>
    </nav>
<?php } ?>
<!-- Fin miga de pan -->
