<?php

class Lf_Shortcode_Breadcrumb extends Lf_Shortcode_Base
{

	public function handle($atts = [])
	{
		if (is_404())
			return;

		$post_type          = get_post_type();
		$post_type_object   = get_post_type_object($post_type);
		if (is_page()) {
			$url            = esc_url(get_post_permalink());
			$post_name      = get_the_title();
		} else {
			$url            = Lf_Utility_Base::get_post_type_url($post_type);
			$post_name      = $post_type_object->label ?? '';
		}

		if (!$post_name) {
			$post_name		= 'CURSOS';
			$url			= get_home_url(null, 'cursos');
		}

		$post_name = mb_strtoupper($post_name);
		if ($post_name == "BUSCADOR")
			$post_name = "RESULTADOS";

		return $this->render_view([
			'post_name' => $post_name,
			'url' => $url
		]);
	}
}
