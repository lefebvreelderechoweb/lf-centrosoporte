<?php

class Lf_Shortcode_User_Products extends Lf_Shortcode_Base
{
	const URL = API_SERVICE_COM_TOOLS;
	const TOOL_ID_LEXON = 9;
	const TOOL_ID_CENTINELA = 11;

	public function handle($atts = [])
	{
		$lf_user = Lf_Login::get_lf_user();
		$tools = $lf_user ? $lf_user->get_tool_mini_hub() : '';
		$json = null;

		if (empty($lf_user) || empty($tools) || Lf_Utility_Base::limited_user_access())
			return '';

		foreach ($tools as $tool) {
			if (!$tool->indAcceso)
				continue;
			if (self::TOOL_ID_LEXON == $tool->idHerramienta) {
				$response = wp_remote_get($tool->url);
				if (is_array($response) && isset($response['body']))
					$json = json_decode($response['body']);

				$tool->url = $json->url ?? '';
			} else if (self::TOOL_ID_CENTINELA == $tool->idHerramienta) {
				$tool->url = get_home_url(null, 'go-to-centinela');
			}
		}

		return $this->render_view([
			'entry' => $lf_user->_idEntradaEncriptada,
			'tools' => $tools
		]);
	}
}
