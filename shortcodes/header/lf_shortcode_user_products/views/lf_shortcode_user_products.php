<div class="nav-item dropdown position-relative">
    <a class="nav-link app-list-toggle" href="#" id="appsList" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"><span class="lf-icon-products"></span></a>
    <div class="dropdown-menu dropdown-menu-right app-list" aria-labelledby="appsList">
        <span class="dropdown-menu-arrow"></span>
        <span class="lf-icon-close text-right d-block pr-3 pt-3"></span>
        <div class="content">
            <a href="https://espaciolefebvre.lefebvre.es/login/<?=$entry?>" target="_blank" title="Acceder a Espacio Lefebvre. Abre en ventana nueva"><img src="https://assets.lefebvre.es/media/logos/web/comunes/lefebvre-ecosistema-177x29.png" class="logo-eco" alt="ECOSISTEMA LEFEBVRE"></a>
            <div role="heading" class="menu-title">Accede desde aquí a los productos contratados</div>
            <div class="product-list">
                <ul class="text-center p-0 m-0">
                    <?php foreach ($tools as $tool) : ?>
                        <?php if ($tool->indAcceso === 1) : ?>
                            <li>
                                <a href="<?= $tool->url ?>" target="_blank" rel="noopener noreferrer" title="Enlace externo. Abre en ventana nueva">
                                    <span class="<?= $tool->icono ?>"></span>
                                    <?= $tool->descHerramienta ?>
                                </a>
                            </li>
                        <?php endif ?>
                    <?php endforeach ?>
                </ul>
            </div>
            <!--<div class="product-description">
                <span>Utiliza este menú para iniciar directamente otro producto de Lefebvre</span>
            </div>-->
        </div>
    </div>
</div>
