<?php

class Lf_Shortcode_User_Menu extends Lf_Shortcode_Base {

    /**
     * Returns the URL to Personal Area if user is logged in
     *
     * @return String URL to Personal Area or empty string
     */
    public static function get_url_area_personal() {
        $current_user = Lf_Login::get_lf_user();

        if ( $current_user )
            return URL_AREA_PERSONAL . $current_user->get_entry_encripted_id();
        else
            return '';
    }

    /**
     * It gets the user's logo from the API, and if it doesn't find it, it gets the first letter of the
     * user's name
     * 
     * @param atts an array of attributes that are passed to the shortcode.
     * 
     * @return the result of the render_view function.
     */
    public function handle( $atts = [] ) {
        
        $no_image = true;

        // Get Logo
        $user = Lf_Login::get_lf_user();

        if ($user){ //avoid compilation issue in case we don't find user in lf-login
            $user_entry_id = $user ? $user->get_entry_id() : '';
            $logo = Lf_WS_Api_Area_Personal::get_logo_user( $user_entry_id );
            $logo = json_decode( $logo );
            $headers = @get_headers($logo->sResultado);
            if($headers && strpos( $headers[0], '200')){
                // image found so we don't need to default
                $no_image = false;
            }
        }        

        // Get Username
        $wp_user_data = wp_get_current_user()->data;
        $user_name = $wp_user_data->user_nicename ?? '';

        // Get URL
        $capability = current_user_can( 'edit_posts' );
        $url = $capability ? get_admin_url() : self::get_url_area_personal();

        // Prepare Logo
        if ( $user && $logo && !$no_image)
            $img = '<img src="' . $logo->sResultado . '" alt="' . $user_name . '" class="d-none d-md-inline-block" style="width:auto;height:40px;">';
        else if ( $user_name && $user_name!='' )
            $img = '<div class="ml-2 letra-user-registred">' . strtoupper( $user_name[0] ) . '</div>';
        else
            $img = '';

        return $this->render_view( [
            'img' => $img,
            'url' => $url
        ] );
    }
}
