<?php if ( $img ) { ?>
    <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="dropdownAccount" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
            <span class="lf-icon-user d-md-none"></span>
            <?= $img ?>
        </a>
        <div class="dropdown-menu dropdown-account" aria-labelledby="dropdownAccount">
            <a class="dropdown-item" href="<?= $url ?>" target="_blank" rel="noopener noreferrer"><span class="lf-icon-tag-user"></span>IR A MI CUENTA</a>
        </div>
    </div>
<?php } ?>
