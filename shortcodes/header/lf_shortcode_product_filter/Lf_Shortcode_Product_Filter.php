<?php

class Lf_Shortcode_Product_Filter extends Lf_Shortcode_Base
{

	public function register_assets($atts = [])
	{
		wp_enqueue_script('filter-family-header-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
	}

	public function handle($atts = [])
	{
		$families = Lf_Utility_Base::get_families_id_by_user();
		$group_families	= Lf_Model_Family::get_family_groups($families);
		Lf_Utility_Family_Tree::order_group_families_tree($group_families);

		return $this->render_view([
			'group_families'	=> $group_families
		]);
	}
}
