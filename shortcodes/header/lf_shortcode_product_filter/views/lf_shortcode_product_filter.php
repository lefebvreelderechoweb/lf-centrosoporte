<label for="selectMateria" class="sr-only"></label>
<select class="custom-select materia col-12 mixpanel-product" id="selectMateria" name="product">
    <option value="">Todos los productos</option>
    <?php foreach ($group_families as $key => $group) { ?>
        <option class="selected-family" value="<?= json_encode(array_column($group, 'ID')) ?>"><?= mb_strtoupper($key) ?></option>
    <?php } ?>
</select>
