<?php

class Lf_Shortcode_Course_List extends Lf_Shortcode_List_Base
{
	private $featured_course;
	private $courses = [];
	public const PAGE_SIZE = 6;

	public function register_assets($atts = [])
	{
		wp_enqueue_script('family-tree-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
	}

	public function ajax_family_filter_course()
	{
		$families		= $_POST['family'] ?? null;
		$search			= $_POST['search'] ?? '';
		$courses		= [];

		ob_start();
		echo $this->handle(['courses' => $courses, 'family' => $families, 'search' => $search]);
		$html = ob_get_contents();
		ob_end_clean();

		echo json_encode([
			'html' => $html
		]);
		exit();
	}

	private function _get_featured_course($courses, $family = '')
	{
		$now					= new DateTime('now');
		$now					= $now->getTimestamp();
		$featured_courses		= Lf_Utility_Base::get_featured_post($family, 'curso');

		foreach ($featured_courses as $featured_course) {
			$featured_course		= explode(',', $featured_course->featured_id ?? '');
			$featured_course		= $featured_course[0] ?? 0;

			// Find course in courses list by ID
			$featured_course = array_filter($courses, function ($course) use ($featured_course) {
				return $course->ID == $featured_course;
			});

			$featured_course = reset($featured_course);

			if (!$this->featured_course || ($featured_course && $featured_course->next_session > $now && $featured_course->next_session < $this->featured_course->next_session))
				$this->featured_course = $featured_course;
		}

		if ($this->featured_course) {
			// Get featured course from courses by ID
			foreach ($courses as $key => $course) {
				if ($course->ID == $this->featured_course->ID ?? 0) {
					unset($courses[$key]);
					break;
				}
			}
		}

		return $courses;
	}

	private function group_courses($courses = [], $families = '', $search = '')
	{
		if (!$courses && !$search && !$families) {
			$courses				= Lf_Utility_Course::get_courses_for_user();
		}
		$families		= $families ? explode(', ', $families) : [];
		$id_families	= [];
		foreach ($families as $family) {
			$family_code = get_field('codigo', $family);
			if ($family_code)
				$id_families[] = $family_code;
		}
		if ($search || $id_families)
			$courses = Lf_Utility_Course::get_courses_for_user(['families_ids' => implode(',', $id_families), 'search' => $search]);
		if (!$search) {
			$courses = $this->_get_featured_course($courses, $families);
		}

		if (!$this->featured_course) {
			$this->featured_course	= reset($courses);
			array_shift($courses);
		}

		$this->courses = Lf_Model_Course::order_courses_by($courses, 'next_session');
	}

	public function handle($atts = [])
	{
		$courses	    = Lf_Utility_Course::get_courses_for_user();
		
		$atts			= Lf_Utility_Base::get_post_params();
		$atts['courses'] = $courses;
		
		$family			= $atts['family'] ?? [];
		$courses		= $atts['courses'] ?? [];
		$search			= $atts['search'] ?? [];
		$user_families	= Lf_Utility_Base::get_families_id_by_user();
		$show_more		= false;

		if (count($user_families)) {
			$this->group_courses($courses, $family, $search);

			if (sizeof($this->courses) > self::PAGE_SIZE)
				$show_more = true;
		}

		return $this->render_view([
			'featured'		=> $this->featured_course,
			'courses'		=> $this->courses,
			'family'		=> $family,
			'user_families' => $user_families,
			'show_more'		=> $show_more
		]);
	}
}
