<?php if (!$featured && !$courses) { ?>
	<div class="row bbottom pb-4 mb-4">
		<h1 class="section-title col-12">CURSOS</h1>
		<p class="msg-danger">No se encuentran resultados</p>
	</div>
<?php
} else {
	$families_names = [];
	$families = Lf_Utility_Base::check_valid_family_to_search($featured->get_wp_families_ids(), $user_families);
	foreach ($families as $fam) {
		$families_names[] = $fam->post_title ?? get_the_title($fam);
	}

	$mixpanel_title = $featured->title ?? '';
	$mixpanel_family = ($featured->families) ? $featured->families[0]->descFamilia : '' ;
?>
	<div class="row bbottom pb-4 mb-4">
		<h1 class="section-title col-12">CURSOS</h1>
		<div class="col-12">
			<h2 class="section-claim"><?= $featured->title ?? '' ?></h2>
			<p class="section-subclaim"><?= $featured->resumen ?? '' ?></p>
		</div>
		<div class="col-7 col-lg-8">
			<div class="row">
				<div class="col-12 mb-3">
					<img src="<?= esc_url($featured->image ?? '') ?>" alt="" class="img-fluid">
				</div>
			</div>
		</div>
		<div class="col-5 col-lg-4">

			<ul class="course-features mb-3">
				<li><span class="lf-icon-on"></span>Online</li>
				<li><span class="lf-icon-clock"></span><?= $featured->duration ?? '' ?> min</li>
				<li><span class="lf-icon-coin"></span>Gratuito</li>
				<li><span class="lf-icon-legal-reference"></span>Nivel <?= strtolower($featured->level ?? 'medio') ?></li>
				<li><span class="lf-icon-user"></span>Para usuarios de <?= implode(', ', $families_names) ?></li>
			</ul>
			<a href="<?= get_site_url() . '/instrucciones-de-acceso' ?>" class="help-text mb-4" target="_blank"><span class="lf-icon-document"></span> Instrucciones de acceso</a>
		</div>
		<div class="col-12 text-right">
			<a href="<?= esc_url($featured->url) ?>" class="btn btn-secondary btn-small modal-loading mixpanel-course-primary" 
			data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family ?>">VER</a>
		</div>
	</div>

	<div id="courseListContainer" class="row bbottom mb-4">
		<?php $i = 0 ?>
		<?php
		foreach ($courses as $course) :
			$mixpanel_title = $course->title ?? '';
			$mixpanel_family = ($course->families) ? $course->families[0]->descFamilia : '' ;

			$families = Lf_Utility_Base::check_valid_family_to_search($course->get_wp_families_ids(), $user_families);
		?>
			<article class="col-12 col-md-6 col-xl-4 mb-5 <?= $i >= Lf_Shortcode_Course_List::PAGE_SIZE ? 'd-none' : '' ?> course-li" data-key='<?= $i ?>'>
				<a class="item course h-100 modal-loading mixpanel-course-all" href="<?= esc_url($course->url) ?>"
				data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family ?>">
					<!-- CATEGORIA -->
					<div class="course-areas col-12 pl-0">
						<?php foreach ($families as $fam) : ?>
							<span class="area-label"><?= strtoupper($fam->post_title ?? get_the_title($fam)) ?></span>
						<?php endforeach ?>
					</div>
					<!-- FIN CATEGORIA -->
					<img src="<?= esc_url($course->image ?? '') ?>" alt="" class="img-fluid">
					<div class="course-type">
						<span>Online</span>
					</div>
					<div class="row no-gutters">
						<p class="col-12 course-level text-right">Nivel <?= strtolower($course->level ?? 'medio') ?></p>
					</div>
					<div class="row">
						<div class="col-12">
							<h2 class="course-title"><?= $course->title ?? '' ?></h2>
							<p><?= $course->resumen ?? '' ?></p>
						</div>
					</div>
					<div class="row abs-btn">
						<div class="col-12 text-center fixed-button-btn">
							<button class="btn btn-secondary btn-small">VER</button>
						</div>
					</div>
				</a>
			</article>
			<?php $i++ ?>
		<?php endforeach; ?>
	</div>

	<?php if ($show_more) : ?>
		<input id="showMoreData" hidden data-page="0" data-show-more="<?= $show_more ?>" data-family="<?= json_encode($family) ?>" data-loaded="<?= sizeof($courses) ?>" />
		<div class="row">
			<div class="col-12 text-center">
				<button id="showMoreCourses" class="btn-borderless mixpanel-show-more">VER MÁS <span class="lf-icon-arrow-round-down"></span></button>
			</div>
		</div>
	<?php endif ?>

<?php } ?>
</div>
