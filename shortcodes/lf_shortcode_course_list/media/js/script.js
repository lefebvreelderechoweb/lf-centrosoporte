$(document).ready(function () {
  $(document).on('click', '#showMoreCourses', function () {
    if (!$('#showMoreData').data('show-more')) return

    const MAX_PAGE = 6
    let courses = $('.course-li.d-none')

    for (let i = 0; i < MAX_PAGE; i++) {
      $(courses[i]).removeClass('d-none')
    }

    courses = $('.course-li.d-none')
    if (!courses.length) {
      $('#showMoreData').data('show-more', 0)
      $('#showMoreCourses').hide()
    }
  })
})
