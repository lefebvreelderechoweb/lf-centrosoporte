<form class="offset-xl-1 col-12 col-xl-10 rate-content text-center mb-5" id="videoNPSForm">
    <span class="title">VALORA ESTE CONTENIDO</span>
    <ul class="d-flex justify-content-between pl-0 mt-3 mb-4 ">
        <li><button type="button" class="btn btn-rate d-flex align-items-center" id="rateBtn1" data-level="1"><span class="lf-icon-face-sad mr-2"></span> Nada útil</button></li>
        <li><button type="button" class="btn btn-rate d-flex align-items-center" id="rateBtn2" data-level="2"><span class="lf-icon-face-sad-half mr-2"></span> Poco útil</button></li>
        <li><button type="button" class="btn btn-rate d-flex align-items-center" id="rateBtn3" data-level="3"><span class="lf-icon-face-neutral mr-2"></span> Algo útil</button></li>
        <li><button type="button" class="btn btn-rate d-flex align-items-center" id="rateBtn4" data-level="4"><span class="lf-icon-face-happy-half mr-2"></span> Bastante útil</button></li>
        <li><button type="button" class="btn btn-rate d-flex align-items-center" id="rateBtn5" data-level="5"><span class="lf-icon-face-happy mr-2"></span> Muy útil</button></li>
    </ul>
    <!-- Mostrar comentarios si Nada o poco útil -->
    <div class="low-rated d-none">
        <input id="postId" hidden data-post-id="<?= $post_id ?>" data-course-id="<?= $course_id ?>" />
        <label for="commentArea">Añadir comentario</label>
        <textarea id="commentArea" class="mb-2 w-100" maxlength="300"></textarea>
        <div class="row align-items-center pb-2">
            <p class="col-4 text-left warning mb-0">Máximo 300 caracteres</p>
            <div class="col-8 text-right">
                <button type="submit" class="btn btn-secondary btn-small mixpanel-rating">ENVIAR</button>
            </div>
        </div>
    </div>
    <!-- FIN comentarios -->
</form>

<!-- CONFIRMACIÓN (jugar con d-none) -->
<div class="col-12 offset-xl-1 col-xl-10 rate-content text-center mb-5 d-none" id="videoNPSConfirm">
    <div class="rated my-3">
        <span class="lf-icon-check-round"></span>
        <span class="state d-block">Tu valoración ha sido registrada correctamente</span>
        <span class="thanks d-block">¡Muchas gracias!</span>
    </div>
</div>
