<?php

class Lf_Shortcode_Rate_Content extends Lf_Shortcode_Base
{
	public function register_assets($atts = [])
	{
		wp_enqueue_script('rate-content-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
	}

	public function register_hooks()
	{
		add_action('wp_ajax_rate_content', array($this, 'ajax_rate_content'));
		add_action('wp_ajax_nopriv_rate_content', array($this, 'ajax_rate_content'));
	}

	public function ajax_rate_content()
	{
		global $wpdb;
		$value          = $_POST['value'] ?? null;
		$comment        = $_POST['comment'] ?? null;
		$post_id        = $_POST['post_id'] ?? null;
		$course_id      = $_POST['course_id'] ?? null;
		$user_id        = get_current_user_id();
		$table          = DB_NPS_USERS;

		$arr = [
			'post_id'   => esc_sql($post_id),
			'course_id' => esc_sql($course_id),
			'user_id'   => esc_sql($user_id),
			'value'     => esc_sql($value),
			'comment'   => esc_sql($comment)
		];
		$success = $wpdb->insert($table, $arr);
		if ($value < 3)
			Lf_Utility_Base::send_bad_rating_mail($post_id);

		echo json_encode([
			'success' => $success ? true : false
		]);
		exit();
	}

	public function handle($atts = [])
	{
		$has_voted = Lf_Model_Base::has_voted_user_nps();
		$post_id   = get_the_ID();
		$course_id = 0;
		if (!$post_id) {
			$url		= $_SERVER['REQUEST_URI'] ?? '';
			$url		= esc_url($url);
			$url		= explode('/', $url);
			$course_id	= (int) $url[count($url) - 1] ?? 0;
		}

		if ($has_voted)
			return;

		return $this->render_view([
			'post_id'	=> $post_id,
			'course_id'	=> $course_id
		]);
	}
}
