let selected = "";

/**
 *
 * @param {String} parent
 * @param {String} name
 */
function ajax_rate_content(value, comment = null) {
  const postId      = $("#postId").data("post-id");
  const courseId    = $("#postId").data("course-id");

  $.ajax({
    url: lf.ajaxurl,
    type: "POST",
    data: {
      action: "rate_content",
      post_id: postId,
      course_id: courseId,
      value,
      comment,
    },
    beforeSend: function () {
      $("html").modalLoading("show");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) $("#videoNPSConfirm").removeClass("d-none");
      else alert("Algo ha fallado");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      console.log(errorThrown);
    },
  });
}

$(document).ready(function () {
  $(".btn-rate").click(function () {
    $(".btn-rate").removeClass("selected");
    $(this).addClass("selected");
    selected = parseInt($(this).data("level"));

    $(".low-rated").removeClass("d-none");
  });

  $("#videoNPSForm").submit(function (event) {
    const comment = $("#commentArea").val();

    $(this).hide();
    ajax_rate_content(selected, comment);
    event.preventDefault();
  });
});
