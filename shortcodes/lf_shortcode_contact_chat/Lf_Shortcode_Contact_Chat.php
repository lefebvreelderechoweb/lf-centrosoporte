<?php

class Lf_Shortcode_Contact_Chat extends Lf_Shortcode_Base
{

	public function register_assets($atts = [])
	{
		$is_online_url = 'https://islpronto.islonline.net/live/islpronto/public/chat_info.js?d=sistemas';

		wp_enqueue_script('chat-js', plugin_dir_url(__FILE__) . 'media/js/chat.js', array('jquery'));
		wp_enqueue_script('isonline-chat-js', $is_online_url, array('jquery'));
	}

	public function handle($atts = [])
	{
		$floating = $atts['floating'] ?? null;
		$user = Lf_Login::get_lf_user();
		$navisionClientId = '';

		if (!empty($user))
			$navisionClientId = $user->get_entry_id() ?? '';

		if ($floating)
			return $navisionClientId;

		return $this->render_view([
			'navisionClientId' => $navisionClientId
		]);
	}
}
