<div class="col-4 mb-3 px-2">
    <div class="item support text-center py-4 px-2 h-100">
        <span class="lf-icon-chat mb-2"></span>
        <h2>Chat Online</h2>
        <p>Solicita asistencia online de uno de nuestros agentes para ayudarte a lo largo de tu sesión</p>
        <p class="horario mb-5">Lunes a jueves: 10:00-14:00 y 16:00-18:00 / Viernes: 9:00-14:00</p>
        <p class="horario"><button class="btn-primary-outline openChat">Activar chat</button></p>
    </div>
</div>
