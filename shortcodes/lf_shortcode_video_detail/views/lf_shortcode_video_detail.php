<div class="col-12">
    <!-- En este caso el título de la página (h1) es el del vídeo -->
    <h1 class="section-claim"><?= $video->title ?? '' ?></h1>
    <p class="section-subclaim"><?= $video->description ?? '' ?></p>
</div>
<div class="embed-responsive embed-responsive-16by9 col-12 offset-lg-1 col-lg-10">
    <?= $video->iframe ?? '' ?>
</div>
