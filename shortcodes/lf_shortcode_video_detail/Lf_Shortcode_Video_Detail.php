<?php

class Lf_Shortcode_Video_Detail extends Lf_Shortcode_List_Base {
	public function handle( $atts = [] ) {
		$lf_user = Lf_Login::get_lf_user();
		$video = new Lf_Model_Video_Clip();
		$video->get( get_the_ID() );
		$has_access = false;

		if ($video) {
			$families_ids = array_column($video->family, 'ID');
			$video_families = [];
			foreach ($families_ids as $family_id) {
				$video_families[] = get_field('codigo', $family_id);
			}
			$has_access = Lf_Utility_Base::has_access($video_families);
		}


		if ( !is_user_logged_in() && ( empty( $lf_user ) || !$has_access ) )
			return '';

		return $this->render_view([
			'video' => $video
		]);
	}
}
