<?php

class Lf_Shortcode_Faq_List extends Lf_Shortcode_List_Base
{
	public function ajax_family_filter_faq()
	{
		$family	= $_POST['family'] ?? null;
		$search	= $_POST['search'] ?? null;
		$tree = Lf_Utility_Family_Tree::create_tree();
		if (!$family && !current_user_can('edit_posts')) {
			$user_families_ids = Lf_Utility_Base::get_families_id_by_user();
			$family = implode(', ', $user_families_ids);
		}

		$tree = self::filter_tree($tree, $family, $search);

		ob_start();
		self::print_view($tree, $search, $family);
		$html = ob_get_contents();
		ob_end_clean();

		echo json_encode([
			'html' => $html
		]);
		exit();
	}

	static function filter_tree(array $tree, $family, $search)
	{
		if ($family) {
			$family = explode(', ', $family);
			foreach ($tree as $key => $value) {
				if (!in_array($value->ID, $family))
					unset($tree[$key]);
			}
		}

		if ($search)
			$tree = self::filter_nodes_tree($tree, $search);

		return $tree;
	}

	static function filter_nodes_tree(array $tree, $search)
	{
		foreach ($tree as $key => $value) {
			$questions = self::get_questions($value->ID, $search);

			$value->childs = self::filter_nodes_tree($value->childs, $search);

			if (empty($questions) && empty($value->childs))
				unset($tree[$key]);
		}

		return $tree;
	}

	static function print_faqs_list(array $tree, $search, $family)
	{
		$group_families = Lf_Model_Family::get_family_groups($tree);
		if ($family) {
			$family = explode(', ', $family);
			$is_family_group = is_array($family) && sizeof($family) > 1;
		}

?>

		<div class="sticky-top" style="top: 5em">
			<button class="btn py-0 float-right" id="collapseFaqs">Replegar todo</button>
		</div>

		<?php

		Lf_Utility_Family_Tree::order_group_families_tree($group_families);

		$i = 0;
		foreach ($group_families as $key => $node) {

			// Lf_Utility_Family_Tree::order_tree_subfamilies($node);

			if (sizeof($node) != 1 || !empty($node[0]->childs) || self::get_questions($node[0]->ID, $search)) {
		?>
				<div class="tree-list mb-5" id="group_<?= $i ?>_0">
					<h2 class="heading-bar">
						<?= mb_strtoupper($family && sizeof($group_families) == 1 && !$is_family_group ? $node[0]->post_title : $key) ?>
					</h2>

					<div class="list-group">

						<?php
						foreach ($node as $element) {
							Lf_Utility_Family_Tree::order_tree_subfamilies($element->childs);

							$questions = self::get_questions($element->ID, $search);
							$html_id = 'group_' . $i . '_' . $element->ID;
							$class = '';
							$level = 2;
							$exists_questions = false;

							if (!sizeof($questions)) {
								$childs = $element->childs;

								while (sizeof($childs) && !$exists_questions) {
									Lf_Utility_Family_Tree::order_tree_subfamilies($childs);

									foreach ($childs as $child) {
										$hasQuestions = self::get_questions($child->ID, $search);
										if (sizeof($hasQuestions)) {
											$exists_questions = true;
											break;
										}
									}
									$childs = $childs->$childs ?? [];
								}
							}

							if (sizeof($questions) || $exists_questions) {
								$product =  mb_strtoupper($family && sizeof($group_families) == 1 && !$is_family_group ? $node[0]->post_title : $key) ;
								$subcategory =  mb_strtoupper($element->post_title);
								if (sizeof($node) > 1) {
									$class = 'collapse';
						?>
									<a href="#<?= $html_id ?>" 
									class="list-group-item mixpanel-faq-group" 
									data-toggle="collapse" 
									data-parent="#group_<?= $i ?>_0"
									data-product="<?= $product ?>"
									data-subcategory="<?= $subcategory ?>"
									><?= mb_strtoupper($element->post_title) ?></a>
								<?php } ?>

								<div class="<?= $class ?>" id="<?= $html_id ?>">

									<?php
									self::print_faq_nodes($element->childs, $html_id, $level, $search, $product, $subcategory);
									self::print_faq_items($questions, $html_id, $level, $product);
									?>

								</div>
						<?php
							}
						}
						?>

					</div>

				</div>
			<?php
			}
			$i++;
		}
	}

	static function print_faq_nodes(array $node, string $html_id, int $level, $search, $product = null, $subcategory = null)
	{
		foreach ($node as $subfamily) {
			$questions = self::get_questions($subfamily->ID, $search);
			$html_id_sub = $html_id . '_' . $subfamily->ID;
			$exists_questions = false;

			if (!sizeof($questions)) {
				$childs = $subfamily->childs;

				while (sizeof($childs) && !$exists_questions) {

					foreach ($childs as $child) {
						$hasQuestions = self::get_questions($child->ID, $search);
						if (sizeof($hasQuestions)) {
							$exists_questions = true;
							break;
						}
					}
					$childs = $childs->$childs ?? [];
				}
			}
			if (sizeof($questions) || $exists_questions) {
			?>
				<a href="#<?= $html_id_sub ?>" 
				class="list-group-item level-<?= $level ?> mixpanel-faq-group" 
				data-toggle="collapse" 
				data-parent="#<?= $html_id ?>"
				data-product="<?= $product ?>"
				data-subcategory="<?= $subcategory ?>"
				data-faq="<?= $subfamily->post_title ?>"
				><?= $subfamily->post_title ?></a>
				<div class="collapse list-group-submenu" id="<?= $html_id_sub ?>">

					<?php
					self::print_faq_nodes($subfamily->childs, $html_id_sub, $level + 1, $search, $product, $subcategory);
					self::print_faq_items($questions, $html_id_sub, $level + 1, $product);
					?>

				</div>

		<?php }
		}
	}

	static function print_faq_items($questions, $html_id, $level, $product = null)
	{
		// <ul>
		// 	<li><a href="#0" class="list-group-item level-1" data-parent="#centinela_0">Level 1. Logra el cumplimiento legal y controla el riesgo con Centinela Penal<br><span>Se explicará la pantalla principal o dashboard, organizaciones, estructura de la implantación, identificación y análisis de riesgos, informes y canal de denuncias.</span></a></li>
		// 	<li><a href="#0" class="list-group-item level-1" data-parent="#centinela_0">Level 1. Consigue acuerdos con Centinela Mediación<br><span>Se explicará el dashboard, gestor de usuarios, dar de alta una organización y una implantación, estructura del proceso de mediación.</span></a></li>
		// 	<li><a href="#0" class="list-group-item level-1" data-parent="#centinela_0">Level 1. Identifica y evalúa los riesgos con Centinela Protección de Datos<br><span>Se explicará la página principal o dashboard, organizaciones, estructura de la implantación, evaluación de impacto, análisis básico e informes.</span></a></li>
		// </ul>
		?>
		<ul>
			<?php
			foreach ($questions as $question) {
				$resumen = get_field('resumen', $question->ID);
			?>
				<li>
					<a href="<?= esc_url(get_permalink($question->ID)) ?>" 
					class="list-group-item level-<?= $level ?> modal-loading mixpanel-faq-item" 
					data-parent="#<?= $html_id ?>"
					data-title="<?= $question->post_title ?>"
					data-product="<?= $product ?>"
					>
						<?= $question->post_title ?>
						<?php if (strlen((string)$resumen)) : ?>
							<br>
							<span><?= $resumen ?></span>
						<?php endif ?>
					</a>
				</li>
			<?php
			}
			?>
		</ul>
		<?php
	}

	static function get_questions(int $family_id, $search = '')
	{
		global $wpdb;

		$query = 'SELECT DISTINCT p.ID, post_title FROM ' . $wpdb->posts . ' p LEFT JOIN '
			. DB_POSTS_FAMILIES . ' d ON p.ID = d.post_id LEFT JOIN ' . $wpdb->postmeta
			. ' pm ON p.ID = pm.post_id  WHERE node_id = ' . $family_id
			. ' AND post_status = "publish" AND post_type = "' . LF_CPT_QUESTION . '"';
		if ($search) {
			$query .= 'AND (post_title LIKE "%' . $search . '%"'
				. 'OR pm.meta_key = "resumen" AND pm.meta_value LIKE "%' . $search . '%"'
				. 'OR pm.meta_key = "respuesta" AND pm.meta_value LIKE "%' . $search . '%")';
		}

		return $wpdb->get_results($query);
	}

	static function print_view(array $tree, $search, $family)
	{
		if (!sizeof($tree)) { ?>
			<div class="row">
				<p class="msg-danger">No se encuentran resultados</p>
			</div>
<?php
		} else
			self::print_faqs_list($tree, $search, $family);
	}

	public function handle($atts = [])
	{
		$family = $atts['family'] ?? null;
		$search = $atts['search'] ?? null;
		$tree = Lf_Utility_Family_Tree::create_tree();
		if (!$family && !current_user_can('edit_posts')) {
			$user_families_ids = Lf_Utility_Base::get_families_id_by_user();
			$family = implode(', ', $user_families_ids);
		}
		$tree = self::filter_tree($tree, $family, $search);

		return $this->render_view([
			'tree' => $tree,
			'search' => $search,
			'family' => $family
		]);
	}
}
