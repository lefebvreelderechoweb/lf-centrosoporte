<?php

class Lf_Shortcode_Import_Menu extends Lf_Shortcode_Base {
    public function register_assets($atts = []) {
        wp_enqueue_script('import-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
    }

    public function handle($atts = array()) {
        $types_import   = [LF_CPT_FAMILY];
        $types_slug     = ['families'];

        return $this->render_view([
            'types_import'  => $types_import,
            'types_slug'    => $types_slug
        ]);
    }
}
