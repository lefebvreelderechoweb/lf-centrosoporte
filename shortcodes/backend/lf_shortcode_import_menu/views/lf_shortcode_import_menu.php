<div id="import" class="container my-5">
    <h3>Importación de contenido:</h3>
    <div class="row">
        <?php foreach ($types_import as $key => $post_type_name) {
            $post_type = get_post_type_object($post_type_name);
            // var_dump($post_type);
        ?>
            <div class="col-2">
                <button class="btn btn-primary btn-import" value="import-<?= $types_slug[$key] ?>">
                    Importar <?= $post_type->label ?>
                </button>
            </div>
        <?php } ?>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-import-images">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Importación finalizada correctamente.</p>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<?php
