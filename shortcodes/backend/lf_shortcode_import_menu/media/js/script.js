/**
 *
 * @param {String} url
 */
function ajax_import(url) {
  $.ajax({
    url: url,
    type: 'GET',
    beforeSend: function () {
      $('html').modalLoading('show');
    },
    success: function () {
      $('html').modalLoading('hide');
      $('#modal-import-images').modal('show');
    },
    error: function (errorThrown) {
      $('html').modalLoading('hide');
      alert(errorThrown);
    },
  });
}

$(document).ready(function () {
  $('.btn-import').on('click', function () {
    const url = $(this).val();
    ajax_import(url);
  });
});
