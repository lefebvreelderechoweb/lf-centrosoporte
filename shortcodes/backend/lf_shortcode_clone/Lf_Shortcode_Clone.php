<?php

class Lf_Shortcode_Clone extends Lf_Shortcode_Base
{
	public function register_hooks()
	{
		add_action('wp_ajax_clone', array($this, 'ajax_clone'));
	}

	/**
	 * Ajax para clonar un post
	 *
	 * @return void
	 */
	public function ajax_clone()
	{
		$orig_post_id	= $_POST['post_id'] ?? '';
		$post			= get_post($orig_post_id);
		$post_id		= $post ? Lf_Service_Clone::clone($post) : 0;

		$response		= [
			'post_id'			=> $post_id,
			'post_editor_url'	=> get_edit_post_link($post_id, 'raw'),
		];

		if ($post_id)
			wp_send_json_success($response);
		else
			wp_send_json_error($response);

		exit();
	}
}
