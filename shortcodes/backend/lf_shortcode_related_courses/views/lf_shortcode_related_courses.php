<input type="hidden" name="<?= $field_name ?>" value="<?= $field_value ?>" id="hidden-input-related-courses" />
<select class="js-source-states" name="states[]" multiple="multiple">
    <?php foreach ($courses as $key => $list) { ?>

        <optgroup label="<?= $key ?>">
            <?php foreach ($list as $course) { ?>
                <option value="<?= $course->ID ?>"><?= $course->title ?></option>
            <?php } ?>
        </optgroup>
    <?php } ?>
</select>
