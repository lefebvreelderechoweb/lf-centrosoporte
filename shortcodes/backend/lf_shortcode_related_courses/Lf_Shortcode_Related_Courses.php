<?php

class Lf_Shortcode_Related_Courses extends Lf_Shortcode_Base
{
	public function register_assets($atts = [])
	{
		wp_enqueue_script('related-courses-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), '4.1.0-rc.0', true);
		wp_enqueue_script('related-courses-script-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
		wp_enqueue_style('related-courses-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), '4.1.0-rc.0');
		wp_enqueue_style('related-courses-style-css', plugin_dir_url(__FILE__) . 'media/css/style.css', array(), _S_VERSION);
	}

	public function handle($atts = [])
	{
		$group_courses = Lf_Utility_Course::get_courses_group_by_families();

		return $this->render_view([
			'courses'		=> $group_courses,
			'field_name'	=> urldecode($atts['field_name']),
			'field_value'	=> urldecode($atts['field_value'])
		]);
	}
}
