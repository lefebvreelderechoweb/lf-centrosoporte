jQuery(document).ready(function ($) {
  const courses = $("#hidden-input-related-courses").val();
  $(".js-source-states").val(courses.split(","));

  $(".js-source-states").change(function () {
    const selected = $(this).val();

    $("#hidden-input-related-courses").val(selected);
  });

  $(".js-source-states").select2();
});
