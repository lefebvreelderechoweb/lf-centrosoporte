$ = jQuery;

var tooltipTriggerList = [].slice.call(
  document.querySelectorAll('[data-bs-toggle="tooltip"]')
);
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});

/**
 * Open Family's Tree and set element as checked
 *
 * @param {Object} element
 * @param {string} value
 */
const setCheck = (element, value) => {
  $(`#collapse-${value}`).addClass("show");
  element.prop("checked", true);
};

/**
 * Order the tree
 *
 * @param {Object} a
 * @param {Object} b
 * @returns if a < b: -1
 * @returns if b > a: 1
 * @returns if a = b: 0
 */
const orderTree = (a, b) => {
  // Cache inner content from the first element (a) and the next sibling (b)
  var aText = a.innerText;
  var bText = b.innerText;

  // Returning -1 will place element `a` before element `b`
  if (aText < bText) {
    return -1;
  }

  // Returning 1 will do the opposite
  if (aText > bText) {
    return 1;
  }

  // Returning 0 leaves them as-is
  return 0;
};

const checkFamilies = () => {
  let families = {};

  $(".family-check:checked").map(function () {
    const id = parseInt($(this).data("node"));
    const inputs = $(`input[data-parent="${id}"]:checked`).map(function () {
      return $(this).data("node");
    });

    families[id] = inputs.toArray();
  });

  $("#hidden-input-families-selected").val(JSON.stringify(families));
};

const prepareTree = () => {
  /**
   * Draw the Tree depending on selected Families/Subfamilies
   */
  // Get array of selected IDs
  let selected = $("#hidden-input-tree").val();
  if (selected) selected = JSON.parse(selected);

  // Check Subfamilies
  $(".subfamily-check").map(function () {
    let value = $(this).data("node");
    let parent = $(this).data("parent");

    if ($.inArray(parseInt(value), selected) > -1) {
      $(`#flexCheckDefault-${parent}`).prop("checked", true);
      $(`#flexCheckDefault-${parent}`).data("storedValue", true);
      $(`input[data-node="${parent}"]`).data("storedValue", true);
      setCheck($(this), parent);
    }
  });

  // Check Families
  $(".family-check").map(function () {
    let value = $(this).data("node");

    if ($.inArray(parseInt(value), selected) > -1) {
      setCheck($(this), value);
      $(this).data("storedValue", true);
    }
  });

  checkFamilies();
};

const setSubfamiliesValues = () => {
  // Get Checked Subfamilies
  let subfamily = $(".subfamily-subfamily-check:checked");

  /**
   * Get Checked Families, then filter by checked subfamilies
   * and return ONLY checked Families with no subfamily selected
   */
  let family = $(".subfamily-family-check:checked");

  // Get Subfamilies' ID
  subfamily = subfamily.map(function () {
    return parseInt($(this).data("node"));
  });
  family = family.map(function () {
    if ($(this).data("node") !== undefined)
      return parseInt($(this).data("node"));
  });
  subfamily = [...new Set(subfamily)];

  let result = $.merge(family, subfamily).toArray();
  $("#selectedSubfamiliesModal").val(JSON.stringify(result));
};

jQuery(document).ready(function ($) {
  let selectedNode = "0";

  $(document).on("click", ".createSubfamily", function () {
    selectedNode = $(this).data("node");
  });

  $(document).on("click", "#createSubfamilyFormButton", function () {
    const subfamilyName = $("#floatingInputSubfamily").val();

    if (subfamilyName.length < 3) {
      $("#floatingInputSubfamily").addClass("is-invalid");
      $("#floatingInputSubfamilyInvalid").show();
    } else ajax_create_subfamily(selectedNode, subfamilyName);
  });

  $(document).on("input", "#floatingInputSubfamily", function () {
    const subfamilyName = $(this).val();

    if (subfamilyName.length >= 3) {
      $("#floatingInputSubfamily").removeClass("is-invalid");
      $("#floatingInputSubfamilyInvalid").hide();
    }
  });

  $("#editTreeBtn").click(function () {
    const editTxt = "Crear Subfamilia Individual";
    const removeTxt = "Terminar la edición";

    if ($(this).html() === editTxt) $(this).html(removeTxt);
    else $(this).html(editTxt);
    $(".createSubBtn").toggle();
  });

  /**
   * Draw the Tree depending on selected Families/Subfamilies
   */
  prepareTree();

  /**
   * On uncheck the Family, resets the Subfamily selection
   */
  $(document).on("click", ".select-family .form-check-input", function () {
    let groupId = $(this).data("node");

    if (!$(this).is(":checked")) {
      const inputs = $(`input[data-parent="${groupId}"]`);

      inputs.map(function () {
        const node = $(this).data("node");

        $(`.subfamily-check[data-node="${node}"]`).prop("checked", false);
      });
    }
  });

  /**
   * Get array of IDs of Families and Subfamilies checked
   */
  $(document).on("click", ".form-check", function () {
    // Get Checked Subfamilies
    let subfamily = $(".subfamily-check:checked");
    /**
     * Get Checked Families, then filter by checked subfamilies
     * and return ONLY checked Families with no subfamily selected
     */
    let family = $(".family-check:checked").map(function () {
      // Get Family's ID
      let value = $(this).data("node");
      // Get array of Subfamilies' parent ID (Family ID)
      let subfamilyParent = subfamily.map(function () {
        return $(this).data("parent");
      });

      // Return Family's ID if no subfamily selected
      return $.inArray(value, subfamilyParent) === -1 ? parseInt(value) : null;
    });

    // Get Subfamilies' ID
    subfamily = subfamily.map(function () {
      return parseInt($(this).data("node"));
    });
    subfamily = [...new Set(subfamily)];

    let result = $.merge(family, subfamily).toArray();
    $("#hidden-input-tree").val(JSON.stringify(result));

    checkFamilies();
  });

  $(document).on("click", ".subfamily-check", function () {
    const node = $(this).data("node");
    const isChecked = $(this).is(":checked");

    const subfamilies = $(`.subfamily-check[data-node="${node}"]`);

    subfamilies.prop("checked", isChecked);

    subfamilies.map(function () {
      if (isChecked) {
        let parent = $(this).data("parent");

        $(`.family-check[data-node="${parent}"]`).prop("checked", true);
        $(`#collapse-${parent}`).addClass("show");
      }
    });
  });

  // Change the order of the Tree
  $("#order-button").click(function () {
    let order = $(this).attr("name");
    let elements = $(".accordion");
    let sortList = Array.prototype.sort.bind(elements);

    if (order === "ASC") {
      sortList((a, b) => orderTree(a, b));
      $(this).attr("name", "DESC");
      $(this).val("Ordenar [A-Z]");
    } else {
      sortList((a, b) => orderTree(b, a));
      $(this).attr("name", "ASC");
      $(this).val("Ordenar [Z-A]");
    }

    $(".main-tree").html(elements);
  });

  /**
   * Subfamilies Tree Modal
   */
  $("#createSubfamiliesBtn").click(function () {
    $("#subfamiliesModalLabel").text("Crear Subfamilias");
    $("#floatingInput").val("");
    $("#selectedSubfamiliesModal").val("");
    $(".subfamily-family-check").prop("checked", false);
    $(".subfamily-subfamily-check").prop("checked", false);
    $(".subfamily-subfamily-check").prop("disabled", false);
    $("#createSubfamiliesFormButton").show();
    $("#updateSubfamiliesFormButton").hide();
    $("#deleteSubfamiliesFormButton").hide();
  });

  $(document).on("click", "#subfamilyCheck", function () {
    $(".subfamily-family-check").prop("checked", $(this).is(":checked"));
    setSubfamiliesValues();
  });

  $(document).on("click", "#allFamilyCheck", function () {
    const isCheck = $(this).is(":checked");
    const families = $(".family-check").prop("checked", isCheck);
    families.map(function () {
      const id = $(this).data("node");

      if (isCheck) $(`#collapse-${id}`).addClass("show");
      else $(`#collapse-${id}`).removeClass("show");
      $(this).prop("checked", isCheck);
      if (!isCheck) {
        const inputs = $(`input[data-parent="${id}"]`);

        inputs.map(function () {
          const node = $(this).data("node");

          $(`.subfamily-check[data-node="${node}"]`).prop("checked", false);
        });
      }
    });
  });

  $(document).on("click", ".subfamily-family-check", function () {
    setSubfamiliesValues();
  });

  $(document).on("click", ".subfamily-subfamily-check", function () {
    const id = $(this).data("node");
    $(`.subfamily-subfamily-check[data-node="${id}"]`).prop(
      "checked",
      $(this).is(":checked")
    );
    setSubfamiliesValues();
  });

  $(document).on("click", "#createSubfamiliesFormButton", function () {
    const subfamilyName = $("#floatingInput").val();

    if (subfamilyName.length < 3) {
      $("#floatingInput").addClass("is-invalid");
      $("#floatingInputInvalid").show();
    } else ajax_create_subfamilies(subfamilyName);
  });

  $(document).on("input", "#floatingInput", function () {
    const subfamilyName = $(this).val();

    if (subfamilyName.length >= 3) {
      $("#floatingInput").removeClass("is-invalid");
      $("#floatingInputInvalid").hide();
    }
  });

  /**
   * Edit Subfamilies
   */
  $(document).on("click", ".editSubfamily", function () {
    const id = $(this).data("node");
    const title = $(this).data("title");
    const list = $(`.editSubfamily[data-node="${id}"]`);
    $(".subfamily-family-check").prop("checked", false);
    $(".subfamily-subfamily-check").prop("checked", false);
    $(".subfamily-subfamily-check").prop("disabled", false);

    list.map(function () {
      const parent = $(this).data("parent");

      $(`.subfamily-family-check[data-node="${parent}"]`).prop("checked", true);
      $(`.subfamily-subfamily-check[data-node="${parent}"]`).prop(
        "checked",
        true
      );
    });
    $(`.subfamily-subfamily-check[data-node="${id}"]`).prop("disabled", true);
    const nested = $(`.subfamily-nodes[data-parent="${id}"]`).find(
      ".subfamily-subfamily-check"
    );

    nested.map(function () {
      const node_id = $(this).data("node");

      $(`.subfamily-subfamily-check[data-node="${node_id}"]`).prop(
        "disabled",
        true
      );
    });

    $("#subfamiliesModalLabel").text("Editar Subfamilia");
    $("#floatingInput").val(title);
    setSubfamiliesValues();
    $("#floatingInput").data("id", id);

    $("#createSubfamiliesFormButton").hide();
    $("#updateSubfamiliesFormButton").show();
    $("#deleteSubfamiliesFormButton").show();
    $("#subfamiliesModal").modal("show");
  });

  $(document).on("click", "#updateSubfamiliesFormButton", function () {
    const subfamilyName = $("#floatingInput").val();
    const subfamilyId = $("#floatingInput").data("id");

    if (subfamilyName.length < 3) {
      $("#floatingInput").addClass("is-invalid");
      $("#floatingInputInvalid").show();
    } else {
      const families = $("#selectedSubfamiliesModal").val();

      if (families === "[]") $("#deleteSubfamiliesFormButton").click();
      else ajax_update_subfamily(subfamilyId, subfamilyName);
    }
  });

  /**
   * Delete Subfamilies
   */
  $(document).on("click", "#deleteSubfamiliesFormButton", function () {
    const subfamilyName = $("#floatingInput").val();
    const subfamilyId = $("#floatingInput").data("id");

    $("#nameSpan").html(subfamilyName);
    $("#deleteConfirmFormButton").data("value", subfamilyId);
    $("#subfamiliesModal").modal("hide");
    $("#confirmDeleteModal").modal("show");
  });

  $("#deleteConfirmFormButton").click(function () {
    const subfamilyId = $(this).data("value");

    ajax_delete_subfamily(subfamilyId);
  });
});
