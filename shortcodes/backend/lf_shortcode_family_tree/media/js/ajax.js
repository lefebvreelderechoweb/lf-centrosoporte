/**
 *
 * @param {String} parent
 * @param {String} name
 */
function ajax_create_subfamily(parent, name) {
  const onlyRadio = $("#only_tree").val();
  const completeTree = $("#complete_tree").val();
  const family = $(".accordion").data("node");

  $.ajax({
    url: ajaxurl,
    type: "POST",
    data: {
      action: "create_subfamily",
      parent,
      name,
      only_radio: onlyRadio,
      complete_tree: completeTree,
      family,
    },
    beforeSend: function () {
      $("#floatingInputSubfamily").val("");
      $("html").modalLoading("show");
      $("#subfamilyModal").modal("hide");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) {
        $("#tree").html(data.html);
        $("#subfamiliesModal .container").html(data.htmlModal);
        prepareTree();
        $(".createSubBtn").toggle();
      }

      $("#resultMessage").text("La subfamilia se ha creado correctamente");
      $("#messageModal").modal("show");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      alert(errorThrown);
    },
  });
}
/**
 *
 * @param {String} name
 */
function ajax_create_subfamilies(name) {
  const families = $("#selectedSubfamiliesModal").val();
  const isIndividual = $("#individualSubfamilyCheck").is(":checked");

  $.ajax({
    url: ajaxurl,
    type: "POST",
    data: {
      action: "create_subfamilies",
      name,
      families,
      individual: isIndividual ? 1 : 0,
    },
    beforeSend: function () {
      $("#floatingInput").val("");
      $("html").modalLoading("show");
      $("#subfamiliesModal").modal("hide");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) {
        const isEditable = $(".createSubBtn").is(":visible");
        $("#tree").html(data.html);
        $("#subfamiliesModal .container").html(data.htmlModal);
        prepareTree();
        if (isEditable) $(".createSubBtn").toggle();
      }

      $("#resultMessage").text("La subfamilia se ha creado correctamente");
      $("#messageModal").modal("show");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      alert(errorThrown);
    },
  });
}

/**
 *
 * @param {int} id
 * @param {String} name
 */
function ajax_update_subfamily(id, name) {
  const families = $("#selectedSubfamiliesModal").val();

  $.ajax({
    url: ajaxurl,
    type: "POST",
    data: {
      action: "update_subfamily",
      id: id,
      name: name,
      families: families,
    },
    beforeSend: function () {
      $("#floatingInput").val("");
      $("html").modalLoading("show");
      $("#subfamiliesModal").modal("hide");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) {
        const isEditable = $(".createSubBtn").is(":visible");
        $("#tree").html(data.html);
        $("#subfamiliesModal .container").html(data.htmlModal);
        prepareTree();
        if (isEditable) $(".createSubBtn").toggle();
      }

      $("#resultMessage").text("La subfamilia se ha modificado correctamente");
      $("#messageModal").modal("show");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      alert(errorThrown);
    },
  });
}

/**
 *
 * @param {int} id
 */
function ajax_delete_subfamily(id) {
  $.ajax({
    url: ajaxurl,
    type: "POST",
    data: {
      action: "delete_subfamily",
      id: id,
    },
    beforeSend: function () {
      $("html").modalLoading("show");
      $("#confirmDeleteModal").modal("hide");
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $("html").modalLoading("hide");

      if (data.success) {
        const isEditable = $(".createSubBtn").is(":visible");
        $("#tree").html(data.html);
        $("#subfamiliesModal .container").html(data.htmlModal);
        prepareTree();
        if (isEditable) $(".createSubBtn").toggle();
      }

      $("#resultMessage").text("La subfamilia se ha eliminado correctamente");
      $("#messageModal").modal("show");
    },
    error: function (errorThrown) {
      $("html").modalLoading("hide");
      alert(errorThrown);
    },
  });
}
