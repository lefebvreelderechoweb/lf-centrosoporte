<?php

class Lf_Shortcode_Family_Tree extends Lf_Shortcode_Base
{

	private $complete_tree = true;
	private $only_radio;
	private $tree = [];

	public function register_assets($atts = [])
	{
		wp_enqueue_script('backend-bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), '5.0.1', true);
		wp_enqueue_script('family-tree-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
		wp_enqueue_script('family-tree-ajax-js', plugin_dir_url(__FILE__) . 'media/js/ajax.js', array('jquery'), _S_VERSION, true);
		wp_enqueue_style('backend-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css', array(), '5.0.1');
		wp_enqueue_style('backend-bootstrap-icons-css', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css', array(), '1.5.0');
		wp_enqueue_style('family-tree-css', plugin_dir_url(__FILE__) . 'media/css/style.css', array(), _S_VERSION);
	}

	public function register_hooks()
	{
		add_action('wp_ajax_create_subfamily', array($this, 'ajax_create_subfamily'));
		add_action('wp_ajax_create_subfamilies', array($this, 'ajax_create_subfamilies'));
		add_action('wp_ajax_update_subfamily', array($this, 'ajax_update_subfamily'));
		add_action('wp_ajax_delete_subfamily', array($this, 'ajax_delete_subfamily'));
	}

	/**
	 * Crear una Subfamilia con los datos recibidos desde Backend
	 */
	public function ajax_create_subfamily()
	{
		$parent					= $_POST['parent'];
		$this->only_radio		= $_POST['only_radio'];
		$this->complete_tree	= $_POST['complete_tree'];

		$this->create_subfamilies([$parent]);
	}
	/**
	 * Crear una Subfamilia con los datos recibidos desde Backend
	 */
	public function ajax_create_subfamilies()
	{
		$families				= $_POST['families'];
		$families				= json_decode($families, true);

		$this->create_subfamilies($families);
	}
	/**
	 * Actualizar una Subfamilia con los datos recibidos desde Backend
	 */
	public function ajax_update_subfamily()
	{
		$families				= $_POST['families'];
		$families				= json_decode($families, true);

		$this->create_subfamilies($families);
	}

	/**
	 * Eliminar una Subfamilia con los datos recibidos desde Backend
	 */
	public function ajax_delete_subfamily()
	{
		$id						= $_POST['id'];
		$this->complete_tree	= true;

		$post_data = wp_delete_post($id);

		$this->send_ajax_response($post_data);
	}

	private function create_subfamilies($families)
	{
		$id						= $_POST['id'] ?? null;
		$name					= $_POST['name'];
		$individual				= (int)$_POST['individual'] ?? 0;

		$args					= [
			'post_title'		=> $name,
			'post_name'			=> str_replace(" ", "-", strtolower($name)),
			'post_type'			=> LF_CPT_SUB_FAMILY,
			'post_status'		=> 'publish'
		];
		if ($id) {
			$args['ID']			= $id;
			$post_id			= wp_update_post($args);
		} else if ($individual) {
			foreach ($families as $family) {
				global $wpdb;

				$post_id		= wp_insert_post($args);
				$wpdb->insert(DB_POSTS_FAMILIES, ['post_id' => (int)$post_id, 'family_id' => (int)$family]);
			}
			$this->send_ajax_response($post_id);
		} else {
			$post_id			= wp_insert_post($args);
			$this->send_ajax_response($post_id, $families);
		}
	}

	private function send_ajax_response($result, $families = null)
	{
		$family					= $_POST['family'] ?? null;

		$success				= $result != 0;
		$html					= '';
		$htmlModal				= '';

		// Prepare HTML of the Tree
		if ($success) {

			if ($families)
				$this->create_parent_subfamily($result, $families);

			ob_start();
			$this->tree = Lf_Utility_Family_Tree::create_tree($this->complete_tree, $family);
			$this->print_tree();
			$html = ob_get_contents();
			ob_end_clean();
			ob_start();
			$this->print_subfamily_tree();
			$htmlModal = ob_get_contents();
			ob_end_clean();
		}

		echo json_encode([
			'success'			=> $success,
			'html'				=> $html,
			'htmlModal'			=> $htmlModal
		]);
		exit();
	}

	private function create_parent_subfamily(int $post_id, $families)
	{
		global $wpdb;

		$previous = $wpdb->get_col('SELECT family_id FROM ' . DB_POSTS_FAMILIES . ' WHERE post_id = ' . $post_id);

		foreach ($previous as $family) {
			if (!in_array($family, $families))
				$wpdb->delete(DB_POSTS_FAMILIES, ['post_id' => $post_id, 'family_id' => $family]);
			else
				$families = array_diff($families, [$family]);
		}

		foreach ($families as $family) {
			$wpdb->insert(DB_POSTS_FAMILIES, ['post_id' => $post_id, 'family_id' => $family]);
		}
	}

	private function add_new_button($id)
	{
?>
		<li class="parent_li createSubBtn hidden">
			<span>
				<button type="button" class="btn btn-success btn-sm lh-1 createSubfamily" data-bs-toggle="modal" data-bs-target="#subfamilyModal" data-node="<?= $id ?>">+</button>
			</span>
		</li>
		<?php
	}

	public function print_subfamily_tree()
	{
		foreach ($this->tree as $node) {
		?>
			<div class="tree m-0 p-0">
				<ul>
					<li class="parent_li">
						<div class="form-check select-family">
							<span>
								<input class="form-check-input subfamily-family-check mt-1" type="checkbox" value="" id="subfamilyCheck-<?= $node->ID ?>" data-node="<?= $node->ID ?>">
								<label class="form-check-label" for="subfamilyCheck-<?= $node->ID ?>">
									<?= $node->post_title ?>
								</label>
							</span>
						</div>

						<ul>
							<?= $this->print_tree_nodes_subfamily($node->childs, $node->ID, $node->ID) ?>
						</ul>

					</li>
				</ul>
			</div>

		<?php }
	}

	private function print_tree_nodes_subfamily(array $tree, int $group_id = 0, $prev)
	{

		foreach ($tree as $node) {
			// Print HTML
		?>

			<li class="parent_li subfamily-nodes" data-parent="<?= $prev ?>">
				<span>
					<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input subfamily-subfamily-check mt-1" type="checkbox" data-node="<?= $node->ID ?>">
							<strong>#<?= $node->ID ?></strong> <?= $node->post_title ?>
						</label>
					</div>
				</span>
				<ul>
					<?= $this->print_tree_nodes_subfamily($node->childs, $group_id, $node->ID) ?>
				</ul>
			</li>

		<?php
		}
	}

	public function print_tree()
	{
		?>
		<input type="hidden" value="<?= $this->only_radio ?>" id="only_tree" />
		<input type="hidden" value="<?= $this->complete_tree ?>" id="complete_tree" />
		<?php
		foreach ($this->tree as $node) { ?>

			<div class="accordion" id="accordion-<?= $node->ID ?>" data-node="<?= $node->ID ?>">
				<div class="accordion-item border-0 tree p-0 m-0">
					<ul class="m-0">
						<li class="parent_li">
							<div class="form-check select-family">
								<span>
									<?php if (!$this->complete_tree) : ?>
										<?= $node->post_title ?>
									<?php elseif ($this->only_radio) : ?>
										<h2 class="accordion-header" id="headingOne">
											<input id="flexRadio-<?= $node->ID ?>" class="form-check-input family-check mt-1" type="radio" data-node="<?= $node->ID ?>" data-bs-toggle="collapse" data-bs-target="#collapse-<?= $node->ID ?>" aria-expanded="true" aria-controls="collapseOne">
											<label class="form-check-label" for="flexRadio-<?= $node->ID ?>">
												<?= $node->post_title ?>
											</label>
										</h2>
									<?php else : ?>
										<h2 class="accordion-header" id="headingOne">
											<input id="flexCheckDefault-<?= $node->ID ?>" class="form-check-input family-check mt-1" type="checkbox" name="<?= $node->ID ?>" data-node="<?= $node->ID ?>" data-bs-toggle="collapse" data-bs-target="#collapse-<?= $node->ID ?>" aria-expanded="true" aria-controls="collapseOne">
											<label class="form-check-label" for="flexCheckDefault-<?= $node->ID ?>">
												<?= $node->post_title ?>
											</label>
										</h2>
									<?php endif ?>
								</span>
							</div>

							<?php if ($this->complete_tree) : ?>
								<div id="collapse-<?= $node->ID ?>" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordion-<?= $node->ID ?>">
								<?php endif ?>

								<div class="accordion-body py-0">
									<ul>
										<?=
										$this->add_new_button($node->ID);
										$this->print_tree_nodes($node->childs, $node->ID, $this->complete_tree, $node->ID);
										?>
									</ul>
								</div>

								<?php if ($this->complete_tree) : ?>
								</div>
							<?php endif ?>

						</li>
					</ul>
				</div>
			</div>

		<?php }
	}

	/**
	 * Print HTML for tree's childrens recursively
	 *
	 * @param array $tree the tree of Families
	 * @param int $group_id ID of the Parent node (Family)
	 * @param bool $print_radio choose between radio and checkbox for Families
	 */
	private function print_tree_nodes(array $tree, int $group_id = 0, bool $print_radio = true, $prev)
	{
		foreach ($tree as $node) {
			// Print HTML
		?>

			<li class="parent_li">
				<span>
					<?php if ($print_radio) : ?>
						<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input subfamily-check mt-1" type="checkbox" name="flexRadio-<?= $group_id ?>" data-parent="<?= $group_id ?>" data-node="<?= $node->ID ?>">
								<strong>#<?= $node->ID ?></strong> <?= $node->post_title ?>
							</label>
							<button type="button" class="btn btn-sm py-0 border-0 editSubfamily" data-node="<?= $node->ID ?>" data-title="<?= $node->post_title ?>" data-parent="<?= $prev ?>">
								<i class="bi bi-pencil-square" title="Editar"></i> Editar
							</button>
						</div>
					<?php else : ?>
						<strong>#<?= $node->ID ?></strong> <?= $node->post_title; ?>
					<?php endif ?>
				</span>
				<ul>
					<?=
					$this->add_new_button($node->ID);
					$this->print_tree_nodes($node->childs, $group_id, $print_radio, $node->ID);
					?>
				</ul>
			</li>

<?php
		}
	}

	public function handle($atts = [])
	{
		$this->complete_tree	= $atts['complete_tree']	?? false;
		$this->only_radio		= $atts['only_radio']		?? false;
		$this->tree				= Lf_Utility_Family_Tree::create_tree($this->complete_tree);

		return $this->render_view([
			'field_name'	=> urldecode($atts['field_name']),
			'field_value'	=> urldecode($atts['field_value']),
			'complete_tree'	=> $this->complete_tree
		]);
	}
}
