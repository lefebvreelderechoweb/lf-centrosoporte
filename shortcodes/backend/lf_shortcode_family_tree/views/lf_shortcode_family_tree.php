<div class="d-flex justify-content-center">
    <?php if ($complete_tree) : ?>
        <input id="order-button" class="btn btn-primary" type="button" value="Ordenar [A-Z]" name="DESC">
        <button type="button" id="createSubfamiliesBtn" class="btn btn-success ml-2" data-bs-toggle="modal" data-bs-target="#subfamiliesModal">Crear Subfamilias</button>
    <?php endif ?>
    <button type="button" id="editTreeBtn" class="btn btn-outline-warning ml-2" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Dentro de cada familia aparecerán botones [+] para añadir Subfamilias a cualquier nodo">Crear Subfamilia Individual</button>
</div>
<input type="hidden" name="<?= $field_name ?>" value="<?= $field_value ?>" id="hidden-input-tree" />
<input type="hidden" name="familiesSelected" id="hidden-input-families-selected" />
<div class="form-check mx-3">
    <input class="form-check-input subfamily-family-check mt-1" type="checkbox" value="" id="allFamilyCheck">
    <label class="form-check-label" for="allFamilyCheck">
        Seleccionar todas las familias
    </label>
</div>
<div class="container main-tree" id="tree">
    <?php $this->print_tree() ?>
</div>

<!-- MODAL MESSAGE -->
<div class="modal fade" id="messageModal" tabindex="-1" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">

                <div id="resultMessage" class="alert alert-success" role="alert">
                    La subfamilia se ha creado correctamente
                </div>
                <div class="d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- MODAL CONFIRM DELETE -->
<div class="modal fade" id="confirmDeleteModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div id="resultMessage">
                    ¿Está seguro de que desea eliminar la subfamilia <span id="nameSpan"></span>?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Descartar</button>
                <button id="deleteConfirmFormButton" type="button" class="btn btn-danger">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL CREATE SINGULAR SUBFAMILY -->
<div class="modal fade" id="subfamilyModal" tabindex="-1" aria-labelledby="subfamilyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="subfamilyModalLabel">Crear Subfamilia Individual</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInputSubfamily" placeholder="Nombre Subfamilia">
                    <label for="floatingInputSubfamily">Nombre</label>
                    <div id="floatingInputSubfamilyInvalid" class="invalid-feedback hidden">
                        Mínimo 3 carácteres
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Descartar</button>
                <button type="button" id="createSubfamilyFormButton" class="btn btn-primary">Crear</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL CREATE MULTIPLE SUBFAMILIES -->
<div class="modal fade" id="subfamiliesModal" tabindex="-1" aria-labelledby="subfamiliesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="subfamiliesModalLabel">Crear Subfamilias</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="form-floating mx-3 mt-1">
                <input type="text" class="form-control" id="floatingInput" placeholder="Nombre Subfamilia">
                <label for="floatingInput">Nombre</label>
                <div id="floatingInputInvalid" class="invalid-feedback hidden">
                    Mínimo 3 carácteres
                </div>
            </div>
            <div class="d-flex mx-3 mt-4">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                    <label class="form-check-label" for="inlineRadio1">Subfamilia Compartida</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="individualSubfamilyCheck" value="option2">
                    <label class="form-check-label" for="individualSubfamilyCheck">Subfamilia Individual</label>
                </div>
            </div>
            <hr />
            <div class="form-check mx-3">
                <input class="form-check-input subfamily-family-check mt-1" type="checkbox" value="" id="subfamilyCheck">
                <label class="form-check-label" for="subfamilyCheck">
                    Seleccionar todas las familias
                </label>
            </div>
            <div class="modal-body">

                <input type="hidden" id="selectedSubfamiliesModal" />
                <div class="container" id="tree">
                    <?php $this->print_subfamily_tree() ?>
                </div>

            </div>
            <div class="modal-footer">
                <button id="deleteSubfamiliesFormButton" type="button" class="btn btn-danger mr-auto">Eliminar</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Descartar</button>
                <button id="createSubfamiliesFormButton" type="button" class="btn btn-primary">Crear</button>
                <button id="updateSubfamiliesFormButton" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
