<h1>Gestor de Valoraciones</h1>
<div class="content mx-5">
    <h3>Filtro avanzado</h3>
    <form class="row border p-1" id="filterNpsForm">
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <span class="input-group-text" id="npsTitle">Título</span>
                <input type="text" name="title" class="form-control" aria-label="Título" aria-describedby="npsTitle">
            </div>
        </div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <span class="input-group-text" id="npsIdNavision">Id Navision</span>
                <input type="text" name="id_navision" class="form-control" aria-label="Id Navision" aria-describedby="npsIdNavision">
            </div>
        </div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <span class="input-group-text" id="npsIdUsuario">Id Usuario</span>
                <input type="text" name="id_user" class="form-control" aria-label="Id Usuario" aria-describedby="npsIdUsuario">
            </div>
        </div>
        <div></div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <label class="input-group-text" for="npsPostType">Tipo Contenido</label>
                <select class="form-select" name="post_type" id="npsPostType">
                    <option value="" selected>Sin selección</option>
                    <?php foreach ($post_types as $key => $value) { ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <label class="input-group-text" for="npsRating">Valoración</label>
                <select class="form-select" name="rating" id="npsRating">
                    <option value="" selected>Sin selección</option>
                    <?php foreach ($rating_levels as $key => $value) { ?>
                        <option value="<?= $key + 1 ?>"><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div></div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <span class="input-group-text" id="inputGroup-sizing-sm">Desde</span>
                <input type="date" name="from" id="npsFrom" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                <button type="button" class="input-group-text" id="npsFromReset" title="Resetear"><i class="bi bi-x"></i></button>
            </div>
        </div>
        <div></div>
        <div class="col-3">
            <div class="input-group input-group-sm mb-3">
                <span class="input-group-text" id="inputGroup-sizing-sm">Hasta</span>
                <input type="date" name="until" id="npsUntil" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                <button type="button" class="input-group-text" id="npsUntilReset" title="Resetear"><i class="bi bi-x"></i></button>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Filtrar</button>
            <button type="button" class="btn btn-secondary" id="npsReset">Resetear</button>
        </div>
    </form>
    <table id="myTable" class="display mt-3">
        <thead>
            <tr>
                <th>Tipo de Contenido</th>
                <th>Título</th>
                <th>Id Navision</th>
                <th>Valoración</th>
                <th>Id Usuario</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
            <?= $this->print_content($rating_levels, $ratings) ?>
        </tbody>
    </table>
</div>
