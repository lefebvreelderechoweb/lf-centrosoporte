function ajax_rate_content_list(params) {
  params.action = 'rate_content_list';
  $.ajax({
    url: ajaxurl,
    type: 'POST',
    data: params,
    beforeSend: function () {
      $('html').modalLoading('show');
    },
    success: function (data) {
      data = jQuery.parseJSON(data);

      $('html').modalLoading('hide');
      $('#myTable').DataTable().destroy();

      if (data.success) $('#myTable tbody').html(data.html);
      else alert('Algo ha fallado');

      table();
    },
    error: function (errorThrown) {
      $('html').modalLoading('hide');
      console.log(errorThrown);
    },
  });
}

function table() {
  var popoverTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="popover"]')
  );
  var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl);
  });

  $('#myTable').DataTable({
    language: {
      url: 'https://cdn.datatables.net/plug-ins/1.11.5/i18n/es-ES.json',
    },
    lengthChange: false,
    pageLength: 10,
    order: [],
  });
}

jQuery(document).ready(function () {
  $ = jQuery;
  table();

  $('#filterNpsForm').submit(function (event) {
    const data = $(this)
      .serializeArray()
      .reduce((obj, item) => {
        obj[item.name] = item.value;
        return obj;
      }, {});

    ajax_rate_content_list(data);
    event.preventDefault();
  });

  $('#npsFromReset').click(function () {
    $('#npsFrom').val('');
  });
  $('#npsUntilReset').click(function () {
    $('#npsUntil').val('');
  });

  $('#npsReset').click(function () {
    // Reset the form
    $('#filterNpsForm').trigger('reset');
    $('#filterNpsForm').submit();
  });
});
