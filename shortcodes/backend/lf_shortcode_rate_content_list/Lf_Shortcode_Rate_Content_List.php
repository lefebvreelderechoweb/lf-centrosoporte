<?php

class Lf_Shortcode_Rate_Content_List extends Lf_Shortcode_Base
{
    public function register_assets($atts = [])
    {
        // Bootstrap
        wp_enqueue_script('backend-bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), '5.0.1', true);
        wp_enqueue_style('backend-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css', array(), '5.0.1');
        wp_enqueue_style('backend-bootstrap-icons-css', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css', array(), '1.5.0');
        // Datatable
        wp_enqueue_script('datatable-js', 'https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js', array(), '1.11.5', true);
        wp_enqueue_style('datatable-css', 'https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css', array(), '1.11.5');

        wp_enqueue_script('rate-content-list-js', plugin_dir_url(__FILE__) . 'media/js/script.js', array('jquery'), _S_VERSION, true);
    }

    public function register_hooks()
    {
        add_action('wp_ajax_rate_content_list', array($this, 'ajax_rate_content_list'));
    }

    public function ajax_rate_content_list()
    {
        $rating_levels  = Lf_Model_Nps::get_rating_levels();
        $ratings        = Lf_Model_Nps::get_ratings();
        $success        = 1;
        $html           = '';

        ob_start();
        $this->print_content($rating_levels, $ratings);
        $html = ob_get_contents();
        ob_end_clean();

        echo json_encode([
            'success'   => $success,
            'html'      => $html
        ]);
        exit();
    }

    public function print_content($rating_levels, $ratings)
    {
        foreach ($ratings as $rating) {
            $post_type  = $rating->post_type;
            $title      = $rating->post_title;
            $obj        = get_post_type_object($post_type);
            $value      = $rating->value - 1;
            $user_id    = $rating->user_id;
            $lf_user    = Lf_Login::get_lf_user($user_id);
            $date       = new DateTime($rating->date);
            $date       = $date->format('d/m/Y H:i:s');
            $comment    = $rating->comment;
?>
            <tr>
                <td><?= $obj->description ?? 'Curso' ?></td>
                <td><?= $title ?></td>
                <td><?= $lf_user ? $lf_user->getNavisionClientId() : "Usuario Wordpress" ?></td>
                <td class="d-flex justify-content-between">
                    <?= $rating_levels[$value] ?>
                    <?php if ($comment) { ?>
                        <span class="btn btn-secondary btn-sm" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="bottom" data-bs-content="<?= esc_attr(str_replace('\n', "\n", $comment)) ?>">
                            <i class="bi bi-chat-left-text"></i>
                        </span>
                    <?php } ?>
                </td>
                <td><?= $lf_user ? $lf_user->get_entry_id() : "Usuario Wordpress" ?></td>
                <td><?= $date ?></td>
            </tr>
<?php
        }
    }

    public function handle($atts = [])
    {
        $post_types     = Lf_Model_Nps::get_post_types();
        $rating_levels  = Lf_Model_Nps::get_rating_levels();
        $ratings        = Lf_Model_Nps::get_ratings();

        return $this->render_view([
            'post_types'    => $post_types,
            'rating_levels' => $rating_levels,
            'ratings'       => $ratings
        ]);
    }
}
