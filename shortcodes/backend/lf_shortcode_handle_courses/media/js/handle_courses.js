$ = jQuery;

$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
  const hasNextSession = data[3];
  const hideNoSession = $('#noSessionCheck').is(':checked');

  return (hasNextSession && hideNoSession) || !hideNoSession;
});

$(document).ready(function () {
  $('.js-source-states').change(function () {
    const selected = $(this).val();
    const selectedId = $(this).data('id');

    $(`.related-content[data-id="${selectedId}"]`).val(selected);
  });
  $('.js-source-courses').change(function () {
    const selected = $(this).val();
    const selectedId = $(this).data('id');

    $(`.related-courses[data-id="${selectedId}"]`).val(selected);
  });

  $('.js-source-states').select2();
  $('.js-source-courses').select2();
  const dt = $('#courseTable').DataTable({
    order: [],
    language: {
      url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/es_es.json',
    },
  });

  $('#noSessionCheck').click(function () {
    dt.draw();
  });

  $('#courseTable').on('draw.dt', function () {
    $('#courseTable_length label select').css('width', '50px');
  });

  $('.form-check .form-check-label').css('margin-left', '20px');
  $('.form-check .form-check-label').css('margin-top', '-20px');

  $('#btnSave').click(function (e) {
    e.preventDefault();
    const related = dt.rows().nodes().$('input[name="related-content"]');
    const relatedCourses = dt.rows().nodes().$('input[name="related-courses"]');

    let relatedElements = [];
    let relatedElementsCourses = [];

    related.map((index, element) => {
      const id = $(element).data('id');
      const values = $(element).val().split(',');

      console.log('values.length :>> ', values.length);
      if (values.length && values[0] !== '') {
        relatedElements.push({ id: id, values: values });
      }
    });

    relatedCourses.map((index, element) => {
      const id = $(element).data('id');
      const values = $(element).val().split(',');

      console.log('values.length :>> ', values.length);
      if (values.length && values[0] !== '') {
        relatedElementsCourses.push({ id: id, values: values });
      }
    });

    $('html').modalLoading('show');

    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      type: 'POST',
      data: {
        action: 'save_relationships_courses',
        relatedElements: relatedElements,
        relatedElementsCourses: relatedElementsCourses,
      },
      success: function (response) {
        $('html').modalLoading('hide');
      },
      error: function (error) {
        $('html').modalLoading('hide');
      },
    });
  });
});
