<div class="m-5">
	<div class="d-flex justify-content-between mb-1">
		<div class="form-check">
			<input class="form-check-input" type="checkbox" id="noSessionCheck">
			<label class="form-check-label" for="noSessionCheck">Esconder los cursos que no tienen sesiones</label>
		</div>
		<button class="btn btn-success" id="btnSave">Guardar</button>
	</div>
	<table id="courseTable" class="table table-striped">
		<thead>
			<tr>
				<th>Familia</th>
				<th>ID</th>
				<th>Curso</th>
				<th>Próxima sesión</th>
				<th>Contenido Relacionado</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($courses as $family => $group) : ?>
				<?php
				foreach ($group as $course) :
					$field_value = implode(',', $course->related_elements);
					$courses_field_value = implode(',', $course->related_courses);
				?>
					<tr class="<?= $next_session ? '' : 'no-session' ?>">
						<td><?= $family ?></td>
						<td><?= $course->ID ?></td>
						<td><?= $course->title ?></td>
						<td><?= $course->next_session_formatted ?></td>
						<td>
							Videos, Manuales y Preguntas Frecuentes
							<input type="hidden" class="related-content" name="related-content" data-id="<?= $course->ID ?>" value="<?= $field_value ?>" />
							<select class="js-source-states" name="states[]" data-id="<?= $course->ID ?>" multiple="multiple" style="width: 100%">
								<?php foreach ($posts as $key => $list) : ?>
									<optgroup label="<?= $key ?>">
										<?php foreach ($list as $post) : ?>
											<?php if ($post->ID != $course->ID) : ?>
												<option value="<?= $post->ID ?>" <?= in_array($post->ID, $course->related_elements) ? 'selected' : '' ?>><?= $post->post_title ?></option>
											<?php endif ?>
										<?php endforeach ?>
									</optgroup>
								<?php endforeach ?>
							</select>
							Cursos
							<input type="hidden" class="related-courses" name="related-courses" data-id="<?= $course->ID ?>" value="<?= $courses_field_value ?>" />
							<select class="js-source-courses" name="states[]" data-id="<?= $course->ID ?>" multiple="multiple" style="width: 100%">
								<?php foreach ($courses as $key => $list) : ?>
									<optgroup label="<?= $key ?>">
										<?php foreach ($list as $related_course) : ?>
											<?php if ($related_course->ID != $course->ID) : ?>
												<option value="<?= $related_course->ID ?>" <?= in_array($related_course->ID, $course->related_courses) ? 'selected' : '' ?>><?= $related_course->title ?></option>
											<?php endif ?>
										<?php endforeach ?>
									</optgroup>
								<?php endforeach ?>
							</select>
						</td>
					</tr>
				<?php endforeach ?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
