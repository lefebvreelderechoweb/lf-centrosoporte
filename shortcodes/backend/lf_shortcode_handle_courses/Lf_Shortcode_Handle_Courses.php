<?php

class Lf_Shortcode_Handle_Courses extends Lf_Shortcode_Base
{
	public function register_assets($atts = [])
	{
		wp_enqueue_script('datatable-js', 'https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js', array('jquery'), '1.11.3', true);
		wp_enqueue_script('handle-courses-js', plugin_dir_url(__FILE__) . 'media/js/handle_courses.js', array('jquery', 'datatable-js'), _S_VERSION, true);
		wp_enqueue_script('related-courses-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), '4.1.0-rc.0', true);
		wp_enqueue_style('datatable-css', 'https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css', array(), '1.11.3');
		wp_enqueue_style('related-courses-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), '4.1.0-rc.0');
	}

	public function register_hooks()
	{
		add_action('wp_ajax_save_relationships_courses', array($this, 'ajax_save_relationships_courses'));
		add_action('wp_ajax_nopriv_save_relationships_courses', array($this, 'ajax_save_relationships_courses'));
	}

	private function _delete_related_elements($element, &$related_list)
	{
		global $wpdb;

		// Find $relation->post_id in $related_list
		$key = array_search($element->post_id, array_column($related_list, 'id'));

		// If found, check if there is $element->related_id in $related_list[$key]['values']
		if ($key !== false || $key === null) {
			$values = $related_list[$key]['values'];
			$key_related = array_search($element->related_id, $values);

			// If not found, remove from DB
			if ($key_related === false || $key === null) {
				$wpdb->delete(DB_COURSES_RELATIONS, [
					'post_id'		=> $element->post_id,
					'related_id'	=> $element->related_id,
					'post_type'		=> $element->post_type
				]);
			} else {
				unset($related_list[$key]['values'][$key_related]);
			}
		} else if ($key === false) {
			$wpdb->delete(DB_COURSES_RELATIONS, [
				'post_id'		=> $element->post_id,
				'related_id'	=> $element->related_id,
				'post_type'		=> $element->post_type
			]);
		}
	}

	private function _insert_related_elements($related_list, $type = '')
	{
		global $wpdb;

		foreach ($related_list as $related) {
			foreach ($related['values'] as $related_id) {
				$post_type = $type != '' ? $type : get_post_type($related_id);
				$wpdb->insert(DB_COURSES_RELATIONS, [
					'post_id'		=> $related['id'],
					'related_id'	=> $related_id,
					'post_type'		=> $post_type
				]);
			}
		}
	}

	public function ajax_save_relationships_courses()
	{
		global $wpdb;

		$related_elements	= $_POST['relatedElements'] ?? [];
		$related_courses	= $_POST['relatedElementsCourses'] ?? [];

		$relations = $wpdb->get_results('SELECT * FROM ' . DB_COURSES_RELATIONS);

		foreach ($relations as $relation) {
			if ($relation->post_type == LF_CPT_COURSE)
				$this->_delete_related_elements($relation, $related_courses);
			else
				$this->_delete_related_elements($relation, $related_elements);
		}

		$this->_insert_related_elements($related_elements);
		$this->_insert_related_elements($related_courses, LF_CPT_COURSE);

		echo json_encode([
			'success'	=> true
		]);
		exit();
	}

	public static function render()
	{
		echo self::call();
	}

	public function handle($args = [])
	{
		$group_courses = Lf_Utility_Course::get_courses_group_by_families();
		$posts_by_type = Lf_Model_Base::get_posts_list_group_by_type();
		$related_elements = Lf_Utility_Course::get_related_elements(true);

		$group_courses = array_map(function ($group) use ($related_elements) {
			return array_map(function ($course) use ($related_elements) {
				// Assign all related elements that are not courses
				$course->related_elements = array_column(array_filter($related_elements, function ($related_element) use ($course) {
					return $related_element->post_type != LF_CPT_COURSE && $related_element->post_id == $course->ID;
				}), 'related_id');

				$course->related_courses = array_column(array_filter($related_elements, function ($related_element) use ($course) {
					return $related_element->post_type == LF_CPT_COURSE && $related_element->post_id == $course->ID;
				}), 'related_id');

				return $course;
			}, $group);
		}, $group_courses);

		return $this->render_view([
			'courses' => $group_courses,
			'posts' => $posts_by_type,
			'related_elements' => $related_elements
		]);
	}
}
