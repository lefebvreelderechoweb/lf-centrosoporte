<?php

class Lf_Shortcode_Video_List extends Lf_Shortcode_List_Base
{
	private $featured_video;
	private $videos = [];
	private $total;
	
	public function ajax_show_more_video()
	{
		$family		= $_POST['family'] ?? null;
		$page		= $_POST['page'] ?? 0;
		$show_more	= $_POST['show_more'] ?? false;
		$loaded		= $_POST['loaded'] ?? 0;
		$search		= $_POST['search'] ?? 0;
		$html		= '';
		
		// Prepare HTML of the Tree
		if ($show_more) {
			$this->get_videos($family, $page, $search);
			$loaded += sizeof($this->videos);

			if ($this->total <= $loaded)
				$show_more = false;

			ob_start();
			self::print_videos_list($this->videos);
			$html = ob_get_contents();
			ob_end_clean();
		}

		echo json_encode([
			'page'				=> $page,
			'show_more'			=> $show_more,
			'loaded'			=> $loaded,
			'html'				=> $html
		]);
		exit();
	}

	public function ajax_family_filter_video()
	{
		$family	= $_POST['family'] ?? null;
		$search	= $_POST['search'] ?? null;
		$values	= $this->get($family, $search);

		ob_start();
		self::print_view($values['featured'], $this->videos, $values['show_more'], $family);
		$html = ob_get_contents();
		ob_end_clean();

		echo json_encode([
			'html' => $html
		]);
		exit();
	}

	function get($family, $search)
	{
		$families	= Lf_Utility_Base::get_families_id_by_user();
		$page		= $atts['page'] ?? 0;
		$featured	= [];
		$show_more	= false;

		if (count($families) || current_user_can('edit_posts')) {
			$this->get_videos($family, $page, $search);

			if ($this->total > sizeof($this->videos))
				$show_more = true;

			// Get Featured video with large thumbnail
			if ($this->featured_video) {
				if (is_array($this->featured_video))
					$this->featured_video = $this->featured_video[0];
				$featured = new Lf_Model_Video_Clip();
				$featured->get($this->featured_video->featured_id);
			} else if (sizeof($this->videos)) {
				$featured = reset($this->videos);
				array_shift($this->videos);
			}

			if ($featured) {
				$model = new Lf_Model_Video($featured->platform ?? '');
				$featured->preview = $model->get_preview(['video_id' => $featured->video_id, 'size' => 'thumbnail_large', 'image_preview' => $featured->image_preview]);
			}
		}

		return [
			'featured' => $featured,
			'show_more' => $show_more
		];
	}

	private function get_videos($family, int $page, $search)
	{
		if (!$search)
			$this->featured_video = Lf_Utility_Base::get_featured_post($family);

		$args = [
			'post_type'		=> LF_CPT_VIDEO,
			'post__not_in'	=> [$this->featured_video->featured_id ?? 0],
			'limit'			=> !$this->featured_video && !$page ? 7 : 6,
			'page'			=> $page			
		];

		if ($family)
			$args['family_id'] = $family;

		if ($search)
			$args['q'] = $search;

		$this->videos = Lf_Utility_Base::get_list($args);

		$this->total = Lf_Utility_Base::get_total($args);
	}

	static function print_videos_list(array $videos)
	{
		foreach ($videos as $video) { 
			/*$hasCourses = false;
			$isRelated = get_field('cursos_relacionados', $video->ID);
			if ($isRelated)
				$hasCourses=true;*/
			?>
			<article class="col-12 col-md-6 col-xl-4 mb-5">
				<a class="item course h-100 modal-loading" href="<?= esc_url(get_permalink($video->ID)) ?>">
						<img src="<?= esc_url($video->preview) ?>" alt="" class="img-fluid mb-3">
					<div class="row">
						<div class="col-12">
							<h2 class="course-title mb-0"><?= $video->title ?></h2>
							<p><?= $video->description ?></p>
						</div>
					</div>
					<div class="row abs-btn">
						<div class="col-12 text-center fixed-button-btn">
							<div class="btn btn-secondary btn-small">
								<?php
									/*if ($hasCourses)
										echo 'VER GRABACIÓN CURSO';
									else*/
										echo 'VER VIDEO';
								?>
							</div>
						</div>
					</div>
				</a>
			</article>
		<?php }
	}

	static function print_view($featured, array $videos, $show_more, $family)
	{
		?>
		<div class="row bbottom mb-5 pb-4">
			<h1 class="section-title col-12">VÍDEOS</h1>
			<?php if (!sizeof($videos) && !$featured) : ?>
				<p class="msg-danger">No se encuentran resultados</p>
			<?php
			endif;
			if ($featured) :
				/*$videobyfeatured= get_posts(array(
					'numberofposts'	=> 1,
					'post_type'	=> 'cpt-video',
					'meta_key'	=> 'video_id',
					'meta_value'	=> $featured->video_id
				));
				$hasCourses=false;
				$isRelated = get_field('cursos_relacionados', $videobyfeatured[0]->ID);
				if ($isRelated)
					$hasCourses=true;*/
					$mixpanel_title = $featured->title ?? '';
					$mixpanel_family = ($featured->family) ? $featured->family[0]->post_title : '' ;
			?>
				<div class="col-12">
					<h2 class="section-claim"><?= $featured->title ?? '' ?></h2>
					<p class="section-subclaim"><?= $featured->description ?? '' ?></p>
				</div>
				<a href="<?= esc_url(get_permalink($featured->ID ?? '')) ?>" class="col-12 offset-lg-1 col-lg-10 modal-loading mixpanel-video" style="display:grid"
				data-title="<?= $mixpanel_title ?>" data-family="<?= $mixpanel_family?>" >
				
					<?php if($featured->platform !== Lf_Model_Video::TYPE_ARCADE) { ?><img class="img-fluid mb-4" src="<?= esc_url($featured->preview ?? '') ?>" alt=""> <?php } ?>
					<?php if($featured->platform === Lf_Model_Video::TYPE_ARCADE) { ?>
						<div class="embed-responsive embed-responsive-16by9 mb-4">
							<iframe class="mb-3" style="width: 100%; pointer-events:none" src="<?= $featured->url_iframe ?>"></iframe>  
						</div>
					<?php } ?>

					<div class="text-right">
						<div class="btn btn-secondary btn-small">
							<?php
								/*if ($hasCourses)
									echo 'VER GRABACIÓN CURSO';
								else*/
									echo 'VER VIDEO';
							?>
						</div>
					</div>
				</a>
			<?php endif ?>
		</div>

		<?php if (sizeof($videos)) : ?>
			<div id="listContainer" class="row bbottom mb-4">
				<?= self::print_videos_list($videos) ?>
			</div>

			<input id="showMoreData" hidden data-page="0" data-show-more="<?= $show_more ?>" data-family="<?= $family ?>" data-loaded="<?= sizeof($videos) + 1 ?>" data-post-name="VÍDEOS" />
			<?php if ($show_more) : ?>
				<div class="row mb-5">
					<div class="col-12 text-center">
						<button id="showMorePosts" class="btn-borderless mixpanel-show-more">VER MÁS <span class="lf-icon-arrow-round-down"></span></button>
					</div>
				</div>
<?php
			endif;
		endif;
	}

	public function handle($atts = [])
	{
		$family		= $atts['family'] ?? null;
		$search		= $atts['search'] ?? null;

		$values = $this->get($family, $search);
		
		return $this->render_view([
			'featured'	=> $values['featured'],
			'videos'	=> $this->videos,
			'family'	=> $family,
			'show_more'	=> $values['show_more']
		]);
	}
}
