<?php
class Lf_Project_Loader {

	protected $remove_actions;
	protected $remove_filters;
	protected $actions;
	protected $filters;
	protected $includeDirs;
	
	public function __construct($includeDirs) {

		$this->remove_actions = [];
		$this->remove_filters = [];
		$this->actions = [];
		$this->filters = [];

		$this->includeDirs = $includeDirs;
		$this->load_components();
	}

	public function remove_action($hook) {
		$this->remove_actions[] = $hook;
	}

	public function remove_filter($hook) {
		$this->remove_filters[] = $hook;
	}

	public function add_action($hook, $component, $callback, $priority = 10, $accepted_args = 1) {
		$this->actions = $this->add($this->actions, $hook, $component, $callback, $priority, $accepted_args);
	}
	
	public function add_filter($hook, $component, $callback, $priority = 10, $accepted_args = 1) {
		$this->filters = $this->add($this->filters, $hook, $component, $callback, $priority, $accepted_args);
	}

	private function add($hooks, $hook, $component, $callback, $priority, $accepted_args) {
		$hooks[] = array(
			'hook' => $hook,
			'component' => $component,
			'callback' => $callback,
			'priority' => $priority,
			'accepted_args' => $accepted_args
		);
		return $hooks;
	}

	public function run() {

		foreach ($this->remove_filters as $hook) {
			remove_all_filters($hook);
		}

		foreach ($this->remove_actions as $hook) {
			remove_all_actions($hook);
		}

		foreach ($this->filters as $hook) {
			add_filter($hook['hook'], array($hook['component'], $hook['callback']), $hook['priority'], $hook['accepted_args']);
		}
		foreach ($this->actions as $hook) {
			add_action($hook['hook'], array($hook['component'], $hook['callback']), $hook['priority'], $hook['accepted_args']);
		}
	}

	public function load_components() {		
		foreach ($this->includeDirs as $key => $incDir) {						
			$this->load_files($incDir);			
		}
	}
	
	public function load_files($incDir, $ds = '') {
		$files = scandir($incDir);

		if ($files) {

			foreach ($files as $file) {
				if ($file !== '.' && $file !== '..') {
					$pathFile = $incDir . $ds . $file;

					if (is_file($pathFile)) {
						require $pathFile;
					} else {
						if (!$ds && $file !== 'views') {
							$this->load_files($pathFile, DS);
						}
					}
				}
			}
		}
	}

}

?>