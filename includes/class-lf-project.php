<?php

class Lf_Project {

	protected $loader;
	protected $plugin_name;
	protected $version;

	public function __construct() {

		$this->version = LF_PLUGIN_VERSION;
		$this->plugin_name = 'lf_project';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_hooks();
		$this->define_utilities();
		$this->define_image_sizes();
		$this->handle_capabilities_roles();
	}

	public function handle_capabilities_roles() {
		
	}

	public function define_image_sizes() {
		//add_image_size('1065x513', 1065, 513, true);
	}

	public function register() {
		Lf_Utility_Post_Types::init();
		$this->register_roles();
		$this->register_taxonomies();
		$this->register_shortcodes();
	}

	public function register_shortcodes() {
		// Header
		new Lf_Shortcode_Product_Filter();
		new Lf_Shortcode_User_Menu();
		new Lf_Shortcode_User_Products();
		new Lf_Shortcode_Breadcrumb();

		// Sidebar
		new Lf_Shortcode_Search();

		// Login
		new Lf_Shortcode_Corporative_Login_Form();

		// Backend
		new Lf_Shortcode_Family_Tree();
		new Lf_Shortcode_Import_Menu();
		new Lf_Shortcode_Rate_Content_List();
		new Lf_Shortcode_Related_Courses();
		new Lf_Shortcode_Handle_Courses();
		new Lf_Shortcode_Clone();

		// Pages components
		new Lf_Shortcode_Rate_Content();
		new Lf_Shortcode_Carousel();
		new Lf_Shortcode_Contact_Chat();
		new Lf_Shortcode_Course_List();
		new Lf_Shortcode_Faq_List();
		new Lf_Shortcode_Manual_List();
		new Lf_Shortcode_Search_List();
		new Lf_Shortcode_Video_Detail();
		new Lf_Shortcode_Video_List();
		new Lf_Shortcode_Course_Detail();
		new Lf_Shortcode_Manual_Detail();
	}

	public function define_utilities() {
		//Lf_Utility_Post_Types::init();
		//Lf_Utility_Link_Nref::init();
		//Lf_Utility_Clone_Post::init();
		//Lf_Utility_Management_Banner_Content::init();
		Lf_Utility_Management_Banner::init([
			'after_title' => 'Después del título'
		]);
		Lf_Utility_Base::init();
		Lf_Utility_Family_Tree::init();
	}

	public function register_taxonomies() {
		
	}

	private function load_dependencies() {
		require_once plugin_dir_path(dirname(__FILE__)) . 'config/define.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-lf-project-loader.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-lf-project-i18n.php';

		$includeDirs = [
			LF_PLUGIN_DIR_ACF_FIELDS,
			LF_PLUGIN_DIR_UTILITIES,
			LF_PLUGIN_DIR_SHORTCODES,
			LF_PLUGIN_DIR_SHORTCODES_HEADER,
			LF_PLUGIN_DIR_SHORTCODES_SIDEBAR,
			LF_PLUGIN_DIR_SHORTCODES_BACKEND,
			LF_PLUGIN_DIR_MODELS,
			LF_PLUGIN_DIR_SERVICES
		];
		$this->loader = new Lf_Project_Loader($includeDirs);
	}

	private function set_locale() {
		$plugin_i18n = new Lf_Project_i18n();
		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
	}

	public function run() {
		$this->loader->run();
	}

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_version() {
		return $this->version;
	}

	public function register_roles() {
		if (is_admin()) {
			// Eliminar roles por defecto de wp
			remove_role('contributor');
			remove_role('subscriber');
		}
	}

	private function define_hooks() {
		$this->loader->add_filter('init', $this, 'check_redirect');
		$this->loader->add_filter('init', $this, 'register');
		$this->loader->add_filter('init', 'Lf_Service_Url', 'init');
		$this->loader->add_filter('save_post_' . LF_CPT_QUESTION, 'Lf_Utility_Post', 'save_question', 10, 3);
		$this->loader->add_filter('save_post_' . LF_CPT_MANUAL, 'Lf_Utility_Post', 'save_manual', 10, 3);
		$this->loader->add_filter('save_post_' . LF_CPT_FAMILY, 'Lf_Utility_Post', 'save_family', 10, 3);
		$this->loader->add_filter('save_post_' . LF_CPT_VIDEO, 'Lf_Utility_Post', 'save_video_course', 10, 3);
		$this->loader->add_filter('save_post_' . LF_CPT_COURSE, 'Lf_Utility_Post', 'save_video_course', 10, 3);
		$this->loader->add_filter('save_post_' . LF_CPT_GROUP_FAMILY, 'Lf_Utility_Post', 'save_family_group', 10, 3);
		$this->loader->add_filter('file_post_types_conf', $this, 'lf_file_post_types_conf');
		$this->loader->add_filter('wp_logout', $this, 'redirect_logout', 1, 1);
		$this->loader->add_action('admin_menu', $this, 'alter_menu_items');
		$this->loader->add_filter('acf/validate_value/name=familias', $this, 'my_acf_validate_value', 10, 4);
		$this->loader->add_action('admin_menu', $this, 'disable_new_families');

		//EVENTOS PARA MIXPANEL 
		$this->loader->add_filter('init', $this, 'check_cookie_mixpanel');
		$this->loader->add_filter('lf_login_success_mixpanel', $this, 'success_mixpanel', 10, 2);

		$this->loader->remove_action('do_feed_rss2');
		$this->loader->add_action('do_feed_rss2', 'Lf_Service_RSS', 'init', 10, 1);
		$this->loader->add_action('edit_form_after_title', 'Lf_Service_Clone', 'clone_post', 10, 1);
		//WEB-42226: mostrar la fecha de la última modificación
		$this->loader->add_action('submitpost_box', $this, 'set_last_edited', 10, 1);

		//WEB-42226: mostrar la fecha de la última modificación en los listados
		$this->loader->add_filter('manage_posts_columns', $this, 'add_last_edit_as_column', 10, 3);
		$this->loader->add_filter('manage_posts_custom_column', $this, 'add_last_edit_column_data', 10, 2);

		$this->loader->add_action('manage_' . LF_CPT_QUESTION . '_posts_columns', $this, 'add_columns_all', 10, 1);
		$this->loader->add_action('manage_' . LF_CPT_QUESTION . '_posts_custom_column', $this, 'handle_column', 10, 2);
		$this->loader->add_action('manage_' . LF_CPT_MANUAL . '_posts_columns', $this, 'add_columns', 10, 1);
		$this->loader->add_action('manage_' . LF_CPT_MANUAL . '_posts_custom_column', $this, 'handle_column', 10, 2);
		$this->loader->add_action('manage_' . LF_CPT_VIDEO . '_posts_columns', $this, 'add_columns', 10, 1);
		$this->loader->add_action('manage_' . LF_CPT_VIDEO . '_posts_custom_column', $this, 'handle_column', 10, 2);
		$this->loader->add_action('restrict_manage_posts', $this, 'restrict_manage_posts', 10, 1);
		$this->loader->add_action('pre_get_posts', $this, 'pre_get_posts', 10, 1);


		//WEB-42226: ordenar por fecha de modificación en los listados
		$this->loader->add_filter('manage_edit-' . LF_CPT_QUESTION . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);
		$this->loader->add_filter('manage_edit-' . LF_CPT_MANUAL . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);
		$this->loader->add_filter('manage_edit-' . LF_CPT_FAMILY . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);
		$this->loader->add_filter('manage_edit-' . LF_CPT_VIDEO . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);
		$this->loader->add_filter('manage_edit-' . LF_CPT_COURSE . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);
		$this->loader->add_filter('manage_edit-' . LF_CPT_GROUP_FAMILY . '_sortable_columns', $this, 'short_last_edit_column', 10, 1);

		//WEB-42225: pdf siempre en fondo blanco
		$this->loader->add_action('add_attachment', $this, 'lf_generate_pdf_thumbnails', 11, 1);
		$this->loader->add_filter('wp_update_attachment_metadata', $this, 'lf_handle_metadata_for_pdf', 10, 2);
	}


	public static function success_mixpanel($lf_user) {
		$CCNavision = $lf_user->_idClienteNav ?? null;
		$IDUsuario = $lf_user->_idCliente ?? null;
		$IDEntrada = $lf_user->_idEntrada ?? null;
		$Email = $lf_user->_email ?? null;
		
		
		$userProfile = array(
			'CCNavision'       => $CCNavision,
			'IDUsuario'        => $IDUsuario,
			'IDEntrada'        => $IDEntrada,
			'Email'			   => $Email
		);

		setcookie( 'UserProfile', json_encode($userProfile) ,[
			'expires' => time() + 21600,
			'path' => '/',
			'secure' => false,
			'httponly' => false,
			'samesite' => 'Lax',
		]);
	}

	public static function check_cookie_mixpanel() {
		$cookie = isset($_COOKIE['UserProfile']) ? $_COOKIE['UserProfile'] : null;

		if (!$cookie) {
			if (is_user_logged_in()) {
				$lf_user = Lf_Login::get_lf_user();
				self::success_mixpanel($lf_user);
			}
		}
		
	}

	public function check_redirect() {

		$is_page_login = Lf_Login::is_current_page_login();
		$redirect_to = $_GET['redirect_to'] ?? null;
		if (!$is_page_login && !$redirect_to) {
			$redirect_token = $_GET[LF_DATA_REDIRECT_TOKEN] ?? null;
			if ($redirect_token) {
				$redirect_token = base64_decode($redirect_token) ?? null;
				if ($redirect_token) {
					wp_redirect($redirect_token);
					die;
				}
			}
		}
		return;
	}

	function disable_new_families() {
		$post_id = $_GET['post'] ?? 0;
		$action = $_GET['action'] ?? 0;
		$post_type = get_post_type($post_id);

		// Hide sidebar link
		global $submenu;
		unset($submenu['edit.php?post_type=' . LF_CPT_FAMILY][10]);

		// Hide link on listing page
		if ((isset($_GET['post_type']) && $_GET['post_type'] == LF_CPT_FAMILY) || ($post_id && $action == 'edit' && $post_type == LF_CPT_FAMILY)
		) {
			echo '<style type="text/css">
			.page-title-action { display:none; }
			input#title, .disabled-family-code {
				pointer-events:none;
				color:#AAA;
				background:#F5F5F5;
			}
			</style>';
		}
	}

	public function lf_file_post_types_conf() {
		return plugin_dir_path(dirname(__FILE__)) . 'config/data.json';
	}

	public function redirect_logout($user_id) {
		$lf_user = get_user_meta($user_id, 'lf_user');

		if (empty($lf_user))
			return;

		$entryEncripted = $lf_user[0]->get_entry_encripted_id();
		$url = str_replace('{entryEncripted}', $entryEncripted, URL_ESPACIO_LEFEBVRE);
		wp_redirect($url);
		exit;
	}

	function my_acf_validate_value($valid, $value, $field, $input_name) {
		if (!$value)
			return false;
		return $valid;
	}

	public function alter_menu_items() {
		$post_types = ['page'];

		if (!current_user_can('administrator')) {
			foreach ($post_types as $post_type) {
				remove_menu_page('edit.php?post_type=' . $post_type);
			}
			remove_menu_page('edit.php');
			remove_menu_page('edit-comments.php');
			remove_menu_page('tools.php');
		}

		if (current_user_can('edit_posts')) {
			// Menu de Importación
			add_menu_page(
				__('Importación', 'lf-centrosoporte'),
				__('Importación', 'lf-centrosoporte'),
				'edit_posts',
				'import-contents',
				'Lf_Model_Import::render_contents',
				'dashicons-admin-site-alt3',
				2
			);
		}

		add_menu_page(
			__('Gestor de valoraciones', 'lf-centrosoporte'),
			__('Gestor de valoraciones', 'lf-centrosoporte'),
			'edit_posts',
			'rate-content',
			'Lf_Model_Nps::render_contents',
			'dashicons-thumbs-up',
			2
		);

		add_menu_page(
			__('Cursos', 'lf-centrosoporte'),
			__('Cursos', 'lf-centrosoporte'),
			'edit_posts',
			'courses',
			'Lf_Shortcode_Handle_Courses::render',
			'dashicons-groups',
			3
		);
	}

	public function pre_get_posts($query) {
		global $pagenow;

		if (!(is_admin() && $query->is_main_query())) {
			return $query;
		}
		$post_type = $query->query['post_type'];


		$valid_post_type_filter_family = [LF_CPT_MANUAL, LF_CPT_QUESTION, LF_CPT_VIDEO];
		if (!in_array($post_type, $valid_post_type_filter_family)) {
			return $query;
		}


		$selected_family = $_GET['filter_family'] ?? [];
		$selected_family = $selected_family ? [$selected_family] : [];		
		

		$selected_subfamily = $_GET['filter_subfamily'] ?? null;

		$selected_group_family = $_GET['filter_group_family'] ?? null;
		if ($selected_group_family) {
			$families = get_field('familias', $selected_group_family);
			if ($families) {
				foreach ($families as $family_id) {
					$selected_family[] = $family_id;
				}
			}
		}

		if (is_admin() && $pagenow == 'edit.php' && ($selected_family || $selected_subfamily)) {

			global $wpdb;

			$post_ids = [];

			$q = 'SELECT p.ID';
			$q .= ' FROM wp_posts p';
			$q .= ' LEFT JOIN lf_posts_families pf on pf.post_id = p.ID';
			$q .= ' WHERE p.post_type = "' . $post_type . '"';

			if ($selected_family) {
				$q .= ' AND pf.family_id IN (' . implode(',', $selected_family) . ')';
			}

			if ($selected_subfamily) {
				$q .= ' AND pf.node_id = ' . $selected_subfamily;
			}

			//$q .= ' AND pf.family_id IN ( ' . implode(',', $selected_ids) . ') OR pf.node_id IN (' . implode(',', $selected_ids) . ')';

			$results = $wpdb->get_results($q);
			if ($results) {
				foreach ($results as $r) {
					$post_ids[] = $r->ID;
				}
			}
			
			if(!$post_ids){
				$post_ids[] = -1;
			}

			$query->set('post__in', $post_ids);
		}

		return $query;
	}

	public function restrict_manage_posts($query) {

		$post_type = $_GET['post_type'] ?? null;
		$selected_group_family = $_GET['filter_group_family'] ?? null;
		$selected_family = $_GET['filter_family'] ?? null;
		$selected_subfamily = $_GET['filter_subfamily'] ?? null;

		if (in_array($post_type, [LF_CPT_QUESTION, LF_CPT_MANUAL, LF_CPT_VIDEO])) {

			$html = '';

			if (LF_CPT_QUESTION === $post_type) {

				// Selector de FAMILIAS AGRUPADAS
				$items = get_posts(['post_type' => LF_CPT_GROUP_FAMILY, 'post_status' => 'any', 'posts_per_page' => -1]);
				$html .= '<select class="select_family" name="filter_group_family">';
				$html .= '<option value=""> Todas los grupos de familias</option>';

				if ($items) {
					usort($items, function($obj1, $obj2) {
						return strcmp($obj1->post_title, $obj2->post_title);
					});

					foreach ($items as $item) {
						$selected = $item->ID == $selected_group_family ? 'selected' : '';
						$html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->ID. ' - '.$item->post_title . '</option>';
					}
				}
				$html .= '</select>';
			}

			// Selector de FAMILIAS
			$items = get_posts(['post_type' => LF_CPT_FAMILY, 'post_status' => 'any', 'posts_per_page' => -1]);
			$html .= '<select class="select_family" name="filter_family">';
			$html .= '<option value=""> Todas las familias</option>';

			if ($items) {
				usort($items, function($obj1, $obj2) {
					return strcmp($obj1->post_title, $obj2->post_title);
				});

				foreach ($items as $item) {
					$selected = $item->ID == $selected_family ? 'selected' : '';
					$html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->ID. ' - '.$item->post_title . '</option>';
				}
			}
			$html .= '</select>';

			if (LF_CPT_QUESTION === $post_type) {
				// Selector de SUB-FAMILIAS
				$items = get_posts(['post_type' => LF_CPT_SUB_FAMILY, 'post_status' => 'any', 'posts_per_page' => -1]);
				$html .= '<select class="select_family" name="filter_subfamily">';
				$html .= '<option value=""> Todas las subfamilias</option>';

				if ($items) {
					usort($items, function($obj1, $obj2) {
						return strcmp($obj1->post_title, $obj2->post_title);
					});

					foreach ($items as $item) {
						$selected = $item->ID == $selected_subfamily ? 'selected' : '';
						$html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->ID. ' - '.$item->post_title . '</option>';
					}
				}
				$html .= '</select>';
			}

			$html .= '<style> #post-query-submit{margin-left:10px;} .select2-container{margin-left:10px;} </style>';

			echo $html;
		}
	}

	public function add_columns_all($columns) {
		$columns_ = [];

		foreach ($columns as $key => $column) {
			$columns_[$key] = $column;
			if ('title' === $key) {
				$columns_['group_family'] = 'Grupo de Familia';
				$columns_['family'] = 'Familias';
				$columns_['subfamily'] = 'Subfamilias';
			}
		}
		return $columns_;
	}

	public function add_columns($columns) {
		$columns_ = [];

		foreach ($columns as $key => $column) {
			$columns_[$key] = $column;
			if ('title' === $key) {
				$columns_['family'] = 'Familias';
			}
		}
		return $columns_;
	}

	function handle_column($column, $post_id) {

		global $wpdb;

		$items = get_posts(['post_type' => LF_CPT_GROUP_FAMILY, 'post_status' => 'any', 'posts_per_page' => -1]);
		$group_families = [];
		if ($items) {
			foreach ($items as $item) {
				$item_id = $item->ID;
				$group_families[$item_id] = get_field('familias', $item_id);
			}
		}

		$q = 'SELECT family_id, node_id';
		$q .= ' FROM lf_posts_families';
		$q .= ' WHERE post_id = ' . $post_id;

		$result = $wpdb->get_results($q);
		$items = [];
		if ($result) {
			foreach ($result as $result) {
				if (!in_array($result->family_id, $items)) {
					$items[] = $result->family_id;
				}
				if (!in_array($result->node_id, $items)) {
					$items[] = $result->node_id;
				}
			}
		}
		$items_ = [];
		foreach ($items as $post_id) {
			$items_[] = get_post($post_id);
		}

		self::handle_column_family($group_families, $items_, $column, $post_id);
	}

	public static function handle_column_family($group_families, $items, $column, $post_id) {
		$families = [];
		$subfamilies = [];
		if ($items) {
			foreach ($items as $item) {
				$post_type = $item->post_type;
				if (LF_CPT_FAMILY === $post_type) {
					$families[] = $item;
				} elseif (LF_CPT_SUB_FAMILY === $post_type) {
					$subfamilies[] = $item;
				}
			}
		}

		switch ($column) {
			case 'group_family':
				if ($families && $group_families) {
					$print_families_ids = [];
					foreach ($families as $key => $item) {

						foreach ($group_families as $group_family_id => $families_gf) {
							if (in_array($item->ID, $families_gf) && !in_array($group_family_id, $print_families_ids)) {
								$print_families_ids[] = $group_family_id;
								//echo !IS_PROD ? $group_family_id.' ' : '';
								echo $group_family_id.' - ';
								echo '<a href="'.get_edit_post_link($item->ID).'">'.get_the_title($group_family_id).'</a>';
								echo '<br>';
							}
						}
					}
				}
				break;

			case 'family':
				if ($families) {
					foreach ($families as $key => $item) {
						echo $key ? '<br>' : '';
						//echo !IS_PROD ? $item->ID.' ' : '';
						echo $item->ID.' - ';
						echo '<a href="'.get_edit_post_link($item->ID).'">'.get_the_title($item->ID).'</a>';
					}
				}
				break;
			case 'subfamily':
				if ($subfamilies) {
					foreach ($subfamilies as $key => $item) {
						echo $key ? '<br>' : '';
						//echo !IS_PROD ? $item->ID.' ' : '';
						echo $item->ID.' - ';
						echo '<a href="'.get_edit_post_link($item->ID).'">'.get_the_title($item->ID).'</a>';
					}
				}
				break;
		}
	}

	function set_last_edited($post) {
		$date = get_the_modified_date('d/m/Y', $post);
		$date_time = get_the_modified_date('H:i', $post);
		?>
		<div class="postbox"><p style="font-size: 18px;padding: 8px;margin: 0;">Editado por última vez:<br><b><?= $date ?></b> a las <b><?= $date_time ?></b></p></div>
		<?php
	}

	function add_last_edit_as_column($columns) {
		?>
		<style>#last_edited{
				width:14%;
			}</style>
		<?php
		return array_merge($columns, ['last_edited' => __('Fecha edición', 'textdomain')]);
	}

	function add_last_edit_column_data($col_key, $post_id) {
		$date = get_the_modified_date('d/m/Y', $post_id);
		$date_time = get_the_modified_date('H:i', $post_id);
		if ($col_key == 'last_edited') {
			echo '<p>Editado por última vez<br>' . $date . ' a las ' . $date_time . '</p>';
		}
	}

	function short_last_edit_column($columns) {
		$columns['last_edited'] = 'modified';
		return $columns;
	}

	protected $generated_pdf_metadata;

	function lf_generate_pdf_thumbnails($pdf_id, $regenerate = false) {

		if (get_post_mime_type($pdf_id) === 'application/pdf') {

			$filepath = get_attached_file($pdf_id);
			$originalparts = pathinfo($filepath);
			$uploads_dir = wp_upload_dir();
			$ds = '/';
			$width = 644;
			$height = 912;
			$filetype = 'image/jpeg';
			$new_file = $uploads_dir['path'] . $ds . $originalparts['filename'] . '.jpg';

			$pdf_convert_full = 'convert -colorspace sRGB ' . $filepath . '[0] -size ' . $width . 'x' . $height . ' -flatten ' . $uploads_dir['path'] . $ds . $originalparts['filename'] . '.jpg';
			shell_exec($pdf_convert_full);

			$pdf_meta = self::lf_create_thumbnails_metadata($new_file, $pdf_id, $filetype);
			/* echo '<pre>';
			  var_dump($pdf_meta);
			  echo '</pre>';
			  die; */
			$this->generated_pdf_metadata = $pdf_meta;
			wp_update_attachment_metadata($pdf_id, $this->generated_pdf_metadata);

			return true;
		}
	}

	function lf_handle_metadata_for_pdf($data, $attachment_id) {
		if (get_post_mime_type($attachment_id) === 'application/pdf') {
			return $this->generated_pdf_metadata; //this is required so it substitutes the file autogenerated by wordpress
		} else
			return $data;
	}

	function lf_create_thumbnails_metadata($file, $attachment_id, $mime) {

		$pdf_meta = array(
			'sizes' => array(),
			'filesize' => wp_filesize($file)
		);

		$sizes = wp_get_registered_image_subsizes();

		$priority = array(
			'medium' => null,
			'large' => null,
			'thumbnail' => null,
			'medium_large' => null,
		);

		$sizes = array_filter(array_merge($priority, $sizes));

		$mainfileparts = pathinfo($file);
		$uploads_dir = wp_upload_dir();
		$ds = '/';
		$pdf_meta['sizes'] = array(
			'full' => array(
				'file' => $mainfileparts['basename'],
				'width' => 644,
				'height' => 912,
				'mime-type' => $mime,
				'filesize' => filesize($file),
				'file_exists' => file_exists($file)
			)
		);

		foreach ($sizes as $key => $size) {
			$new_file = $uploads_dir['path'] . $ds . $mainfileparts['filename'] . '-' . $key . '.jpg';
			$resize_cmd = 'convert -resize x' . $size['height'] . ' ' . $file . ' -quality 90 ' . $new_file;
			shell_exec($resize_cmd);

			$thumb_fileparts = pathinfo($new_file);
			$metasize = array(
				$key => array(
					'file' => $thumb_fileparts['basename'],
					'width' => $size['width'],
					'height' => $size['height'],
					'mime-type' => $mime,
					'filesize' => wp_filesize($new_file),
					'file_exists' => file_exists($new_file)
				)
			);
			$pdf_meta['sizes'] = array_merge($pdf_meta['sizes'], $metasize);
		}

		return $pdf_meta;
	}

}
