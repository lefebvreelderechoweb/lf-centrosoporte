<?php
class Lf_Project_i18n {

	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			LF_PROJECT,
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}
}
?>