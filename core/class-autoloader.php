<?php
class Class_Autoloader {

private $execptionsNames = array();

public function __construct( $includeDirs, $exceptionsNames = array() ) {

    foreach( $includeDirs as $incDir ) {
        $files  = scandir($incDir);
        if( $files ){
            foreach($files as $file ){
                if( $file !== '.' && $file !== '..' ){
                    $pathFile = $incDir.$file;
                    require $pathFile;
                }
            }
        }
    }
}
private function loader( $className ) {

    $found = false;

    if ( !empty( $this->execptionsNames )) {
        echo( $className);
        foreach ( $this->execptionsNames as $name ) {
            $found = ( $name === $className );
            if ( $found ) break;
        }
    }

    if ( $found ) {
        require_once $className . '.php';
    }
}

function add_include_path ($path)
{
    foreach (func_get_args() AS $path)
    {
        if (!file_exists($path) OR (file_exists($path) && filetype($path) !== 'dir'))
        {
            trigger_error("Include path '{$path}' not exists", E_USER_WARNING);
            continue;
        }

        $paths = explode(PATH_SEPARATOR, get_include_path());

        if (array_search($path, $paths) === false) {
            array_push($paths, $path);
        }

        set_include_path(implode(PATH_SEPARATOR, $paths));
    }
}
}
?>