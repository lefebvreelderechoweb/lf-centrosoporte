<?php

define('LF_PROJECT', 'lf-project');
/**
 * Redireccionamiento
 */
define('LF_DATA_REDIRECT_TOKEN', 'redirect_token');

/**
 * Definción de CPT's
 */
define('LF_CPT_VIDEO', 'cpt-video');
define('LF_CPT_TOPIC', 'cpt-topic');
define('LF_CPT_FAMILY', 'cpt-family');
define('LF_CPT_SUB_FAMILY', 'cpt-sub-family');
define('LF_CPT_GROUP_FAMILY', 'cpt-group-family');
define('LF_CPT_MANUAL', 'cpt-manual');
define('LF_CPT_COURSE', 'cpt-course');
define('LF_CPT_COURSE_INSTRUCTIONS', 'cpt-course-instr');
define('LF_CPT_QUESTION', 'cpt-question');

/**
 * Definición de CPT's URL
 */
define('LF_CPT_VIDEO_URL', 'videos');
define('LF_CPT_MANUAL_URL', 'manuales');
define('LF_CPT_COURSE_URL', 'cursos');
define('LF_CPT_QUESTION_URL', 'preguntas-frecuentes');

/**
 * Url's/Dir's
 */
define('LF_PLUGIN_DIR_ACF_FIELDS', LF_PLUGIN_DIR . 'acf-fields' . DS);
define('LF_PLUGIN_DIR_POST_TYPES', LF_PLUGIN_DIR . 'post-types' . DS);
define('LF_PLUGIN_DIR_SHORTCODES', LF_PLUGIN_DIR . 'shortcodes' . DS);
define('LF_PLUGIN_DIR_SHORTCODES_HEADER', LF_PLUGIN_DIR_SHORTCODES . 'header' . DS);
define('LF_PLUGIN_DIR_SHORTCODES_SIDEBAR', LF_PLUGIN_DIR_SHORTCODES . 'sidebar' . DS);
define('LF_PLUGIN_DIR_SHORTCODES_BACKEND', LF_PLUGIN_DIR_SHORTCODES . 'backend' . DS);
define('LF_PLUGIN_DIR_TEMPLATES', LF_PLUGIN_DIR . 'templates' . DS);
define('LF_PLUGIN_DIR_LIB', LF_PLUGIN_DIR . 'lib' . DS);
define('LF_PLUGIN_DIR_MODELS', LF_PLUGIN_DIR . 'models' . DS);
define('LF_PLUGIN_DIR_SERVICES', LF_PLUGIN_DIR . 'services' . DS);
define('LF_PLUGIN_DIR_UTILITIES', LF_PLUGIN_DIR . 'utilities' . DS);

define('LF_THEME_DIR_SRC', LF_THEME_DIR . '/src/' . DS);
define('LF_THEME_DIR_SRC_IMAGES', LF_THEME_DIR_SRC . '/images/' . DS);
define('LF_THEME_DIR_SRC_IMAGES_BACKEND', LF_THEME_DIR_SRC_IMAGES . '/backend/' . DS);
define('LF_THEME_DIR_SRC_IMAGES_ICONS', LF_THEME_DIR_SRC_IMAGES . '/icons/' . DS);

/**
 * Definición general
 */
switch (getenv('ENVIRONMENT')) {
	case 'DEV':
	case 'PRE':
		define('IS_PROD', false);
		define('ID_MAIL_GROUP', 5136);
		define('MAIL_TO', 'e.stina-ext@lefebvre.es');
		define('RATING_MAIL_TO', 'e.stina-ext@lefebvre.es');
		define('MAIL_CONTACT', 'e.stina-ext@lefebvre.es');
		define('URL_AREA_PERSONAL', 'http://led-dev-areapersonal-dev/login/dashboard/');
		define('URL_CENTINELA', 'https://compliance.affin.es/dashboard?sso={token}');
		define('VISOR_PDF', 'https://led-dev-plataforma-pdf-dev.eu.els.local/');		
		define( 'CHAT_DATA_S', 'Chat_SAC_GENERAL' );
		define( 'CHAT_DATA_URL', 'https://led-dev-atenea-dev.eu.els.local/api/chat/script' );
		define('MIXPANEL_TOKEN', 'd0bc86aac019a716ab4bb435b352f202');
		break;
	case 'PROD':
		define('IS_PROD', true);
		define('ID_MAIL_GROUP', 121);
		define('MAIL_TO', 'cc.sanchez@lefebvre.es');
		define('RATING_MAIL_TO', 'formacion@lefebvre.es');
		define('MAIL_CONTACT', 'clientes@lefebvre.es');
		define('URL_AREA_PERSONAL', 'https://areapersonal.lefebvre.es/login/dashboard/');
		define('URL_CENTINELA', 'https://centinela.lefebvre.es/dashboard?sso={token}');
		define('VISOR_PDF', 'https://vpdf.lefebvre.es/');
		define( 'CHAT_DATA_S', 'Chat_SAC_GENERAL' );
		define( 'CHAT_DATA_URL', 'https://atenea.lefebvre.es/api/chat/script' );
		define('MIXPANEL_TOKEN', 'af26bb89ac5f16bcabb4e537d53b96a9');		
		break;
}
define('MAIL_FROM', 'centrodeayuda@emailing.lefebvre.es');


define('URL_CENTROSOPORTE_RECOVERY_PASS_ONLINE', 'https://espaciolefebvre.lefebvre.es/recuperar-password?redirectTo=https://centrosoporte.lefebvre.es/');
define('URL_ONLINE_ELDERECHO', 'https://online.elderecho.com/');

// Tablas custom de la BBDD
define('DB_POSTS_FAMILIES', 'lf_posts_families');
define('DB_COURSES_RELATIONS', 'lf_courses_relations');
define('DB_NPS_USERS', 'lf_nps_users');

// define('RECAPTCHA_V3_PUBLIC_KEY', '6Lc10IIaAAAAAH6MeOghs8WMpWsBwidY9H0HuOVt');
// define('RECAPTCHA_V3_PRIVATE_KEY', '6Lc10IIaAAAAAL6D0Y-hVhLWtZKdDIVs3c_kT4_K');
